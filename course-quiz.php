<?php
// Initialize the session
session_start();

// Check if the user is already logged in, if yes then redirect him to welcome page
if (!isset($_SESSION["loggedin"])) {
    header("location: authentication.php");
    exit;
}

// Processing form data when form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST['quiz_submit']) && $_POST['quiz_submit']) {

        $section_names = array("Gain", "Absolute", "2D LUT", "Bus creator", "Bus selector", "Data Dictionary", "Multiport Switch", "Manual Switch", "Bit Clear", "Bitwise Operator", "Ramp", "Scope", "Intro Simulink", "MBD Intro");
        $focus_areas = array();


        if (isset($_POST['question-1-answers']) && $_POST['question-1-answers']) {
            $answer1 = $_POST['question-1-answers'];
        } else {
            $answer1 = "";
        }
        if (isset($_POST['question-2-answers']) && $_POST['question-2-answers']) {
            $answer2 = $_POST['question-2-answers'];
        } else {
            $answer2 = "";
        }
        if (isset($_POST['question-3-answers']) && $_POST['question-3-answers']) {
            $answer3 = $_POST['question-3-answers'];
        } else {
            $answer3 = "";
        }
        if (isset($_POST['question-4-answers']) && $_POST['question-4-answers']) {
            $answer4 = $_POST['question-4-answers'];
        } else {
            $answer4 = "";
        }
        if (isset($_POST['question-5-answers']) && $_POST['question-5-answers']) {
            $answer5 = $_POST['question-5-answers'];
        } else {
            $answer5 = "";
        }
        if (isset($_POST['question-6-answers']) && $_POST['question-6-answers']) {
            $answer6 = $_POST['question-6-answers'];
        } else {
            $answer6 = "";
        }
        if (isset($_POST['question-7-answers']) && $_POST['question-7-answers']) {
            $answer7 = $_POST['question-7-answers'];
        } else {
            $answer7 = "";
        }
        if (isset($_POST['question-8-answers']) && $_POST['question-8-answers']) {
            $answer8 = $_POST['question-8-answers'];
        } else {
            $answer8 = "";
        }
        if (isset($_POST['question-9-answers']) && $_POST['question-9-answers']) {
            $answer9 = $_POST['question-9-answers'];
        } else {
            $answer9 = "";
        }
        if (isset($_POST['question-10-answers']) && $_POST['question-10-answers']) {
            $answer10 = $_POST['question-10-answers'];
        } else {
            $answer10 = "";
        }
   

        $totalCorrect = 0;

        if ($answer1 == "A") {
            $totalCorrect++;
        }
        if ($answer2 == "C") {
            $totalCorrect++;
        }

        if ($answer1 != "A" || $answer2 == "C") {
            array_push($focus_areas, $section_names[0]);
        }

        if ($answer3 == "B") {
            $totalCorrect++;
        }
        if ($answer4 == "D") {
            $totalCorrect++;
        }

        if ($answer3 != "B" || $answer4 != "D") {
            array_push($focus_areas, $section_names[1]);
        }

        if ($answer5 == "D") {
            $totalCorrect++;
        }
        if ($answer6 == "C") {
            $totalCorrect++;
        }

        if ($answer5 != "D" || $answer6 != "C") {
            array_push($focus_areas, $section_names[2]);
        }

        if ($answer7 == "D") {
            $totalCorrect++;
        }
        if ($answer8 == "C") {
            $totalCorrect++;
        }
        if ($answer9 == "A") {
            $totalCorrect++;
        }
        if ($answer10 == "D") {
            $totalCorrect++;
        }

        if ($answer7 != "D" || $answer8 != "C" || $answer9 != "A" || $answer10 != "D") {
            array_push($focus_areas, $section_names[3]);
        }

        
        
      

        // Store data in session variables
        $_SESSION["total_correct"] = $totalCorrect;
        $_SESSION["total_questions"] = "10";
        $_SESSION["focus_areas"] = $focus_areas;

        // Redirect user to welcome page
        header("location: course-result jp.php");
    }
}
?>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="assets/images/favicon.png">
    <meta name="description" content="">
    <title> Course Quiz </title>
    <!-- Favicon -->
    <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Your stylesheet-->
    <link rel="stylesheet" href="assets/css/uikit.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <!-- font awesome -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <!--  javascript -->
    <script src="assets/js/simplebar.js"></script>
    <script src="assets/js/uikit.js"></script>
</head>

<body>
    <!-- for mobile  -->
    <header class="tm-course-content-header uk-background-grey uk-hidden@s" uk-sticky="show-on-up: true; animation: uk-animation-slide-top">
        <!-- mobile-sidebar  -->
        <i class="fas fa-video icon-large tm-side-right-mobile-icon uk-padding-small uk-text-white uk-float-right" uk-tooltip="title:  open side course    ; delay: 200 ; pos: bottom-left ;animation:uk-animation-scale-up ; offset:20 ; cls : uk-active" uk-toggle="target: #open-side-course"> </i>
        <a href="course-lesson%20jp.php" class="back-to-dhashboard" uk-tooltip="title: Back to Course Dashboard  ; delay: 200 ; pos: bottom-left ;animation:uk-animation-scale-up ; offset:20"> Back to Course Lessons</a>
    </header>
    <div class="uk-section-small uk-section-default uk-position-relative uk-visible@s">
        <div uk-grid>
            <div class="uk-width-1-4@m">
                <a href="course-dashboard jp.php" class="uk-logo uk-logo-dark uk-margin-medium-left"> <i class="fas fa-graduation-cap"> </i> </a>
            </div>
            <div class="uk-width-expand@m">
                <div uk-grid>
                    <div class="uk-width-2-3@m">
                        <a href="course-lesson%20jp.php" class="uk-text-muted"> <i class="fas fa-long-arrow-alt-left"></i> コース・レッスンに戻る</a>
                    </div>
                    <div class="uk-width-1-3@m">
                        <!--  night mode  -->
                        <a href="#" class="uk-link-reset"><i class="fas fa-moon icon-medium uk-margin-left"></i></a>
                        <div uk-drop="pos: top ;mode:click ; offset: 20;animation: uk-animation-scale-up" class="uk-drop">
                            <div class="uk-card-small uk-box-shadow-xlarge  uk-maring-small-left  border-radius-6">
                                <div class="uk-card uk-card-default   border-radius-6">
                                    <div class="uk-card-header">
                                        <h5 class="uk-card-title uk-margin-remove-bottom">Swich to night mode</h5>
                                    </div>
                                    <div class="uk-card-body">
                                        <p class="uk-text-small uk-align-left uk-margin-remove  uk-text-muted">DARK THEME </p>
                                        <!-- night mode button -->
                                        <div class="btn-night uk-align-right" id="night-mode">
                                            <label class="tm-switch">
                                                <div class="uk-switch-button"></div>
                                            </label>
                                        </div>
                                        <!-- end  night mode button -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>

        <div class="uk-container-small uk-margin-auto uk-margin-medium-top">
            <h3 class=" uk-text-light"> Simulinkの導入 </h3>
            <h1 class="uk-text-light"> コーステスト</h1>
        </div>
    </div>
    <div class="uk-container-small uk-padding-small uk-margin-auto">
        <!-- PreLoader -->
        <div id="spinneroverlay">
            <div class="spinner"></div>
        </div>
        <ul id="course-videos" class="uk-switcher uk-position-relative uk-margin-medium-bottom">
            <!--  Quiz -->
            <li>
                <form id="loginForm" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="uk-scrollable-content uk-background-white">
                    <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q1. ゲイン値は、正、負、浮動のいずれでもかまいませんが、ゼロになることはありません。</p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-1-answers" value="A"> 真</label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-1-answers" value="B"> 偽</label>
                                    <br><br>
                                </div>
                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q2. ゲインパラメータは5で、入力値は10です。出力値は </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-2-answers" value="A"> 05</label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-2-answers" value="B"> 10</label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-2-answers" value="C"> 50</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-2-answers" value="D"> 25</label>
                                </div>
                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q3. アブソルートブロック出力は常に………です。 </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-3-answers" value="A"> ネガティブ</label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-3-answers" value="B"> ポジティブ</label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-3-answers" value="C"> 両方</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-3-answers" value="D"> どちらでもない</label>
                                </div>
                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q4. アブソルートブロックは、ライブラリブラウザの……………..セクションにあります</p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-4-answers" value="A"> シンク</label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-4-answers" value="B"> ソース</label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-4-answers" value="C"> 論理演算とビット演算</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-4-answers" value="D"> 演算演算</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q5. 1D LUTの入力から出力への対応は</p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-5-answers" value="A">1:2 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-5-answers" value="B">1:1 </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-5-answers" value="C"> 1:3</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-5-answers" value="D"> 2:1</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q6. LUTの入力の診断方法は </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-6-answers" value="A">エラー </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-6-answers" value="B">警告 </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-6-answers" value="C"> すべて </label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-6-answers" value="D"> なし</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q7. バスクリエーターへの入力は </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-7-answers" value="A">同じタイプ </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-7-answers" value="B">異なるタイプ </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-7-answers" value="C"> 定数</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-7-answers" value="D"> すべて</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q8. バスの作成者には、…………..入力と出力の関係があります </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-8-answers" value="A">1対多 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-8-answers" value="B">1対1  </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-8-answers" value="C"> 多対1</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-8-answers" value="D"> なし</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q9. バスクリエーターの出力で、 </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-9-answers" value="A">構造 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-9-answers" value="B">配列 </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-9-answers" value="C"> 列挙型</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-9-answers" value="D"> 結合</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q10. バスクリエーターの出力は </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-10-answers" value="A">同じタイプ </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-10-answers" value="B">異なるタイプ </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-10-answers" value="C"> 定数</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-10-answers" value="D"> すべて</label>
                                </div>

                                

                        <div class="uk-clearfix uk-margin uk-margin-medium-right uk-animation-slide-bottom">
                            <div class="uk-float-right">
                                <input class="uk-button uk-button-success" name="quiz_submit" type="submit" class="button" value="Submit">
                            </div>
                        </div>

                    </div>
                </form>
            </li>
        </ul>
    </div>
    <!-- Sidebar-->
    <div class="tm-side-course" id="open-side-course" uk-offcanvas="overlay: true">
        <div class="uk-offcanvas-bar uk-padding-remove uk-preserve-color">
            <button class="uk-offcanvas-close" type="button" uk-close></button>
            <!-- Sidebar menu-->
            <ul class="uk-child-width-expand uk-tab tm-side-course-nav uk-margin-remove uk-position-z-index" uk-switcher="animation: uk-animation-slide-left-medium, uk-animation-slide-right-small" style="box-shadow: 0px 0px 7px 1px gainsboro;" uk-switcher>
                <li class="uk-active">
                    <a href="#" uk-tooltip="title: Course Videos ; delay: 200 ; pos: bottom-left ;animation:uk-animation-scale-up"> <i class="fas fa-video icon-medium"></i> Videos </a>
                </li>
                <li>
                    <a href="#" uk-tooltip="title: Course Files ; delay: 200 ; pos: bottom-center ;animation:uk-animation-scale-up"> <i class="fas fa-archive icon-medium"></i> Contents </a>
                </li>
            </ul>
            <!-- Sidebar contents -->
            <ul class="uk-switcher">
                <!-- Course Video tab  -->
                <li>
                    <div class="demo1" data-simplebar>
                        <ul class="course-list video-list" uk-switcher="connect: #course-videos; animation: uk-animation-slide-right-small, uk-animation-slide-left-medium">
                            <!--  Quiz  -->
                            <li>
                                <a href="#">
                                    <img src="assets/images/courses/course-lesson-quiz.png" alt="">
                                    <i class="fas fa-question-circle  play-icon"></i>
                                    <span class="uk-text-truncate"> Quiz </span>
                                    <time>30 Questions</time>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- app end -->
    <!--  Night mood -->
    <script>
        (function(window, document, undefined) {
            'use strict';
            if (!('localStorage' in window)) return;
            var nightMode = localStorage.getItem('gmtNightMode');
            if (nightMode) {
                document.documentElement.className += ' night-mode';
            }
        })(window, document);


        (function(window, document, undefined) {

            'use strict';

            // Feature test
            if (!('localStorage' in window)) return;

            // Get our newly insert toggle
            var nightMode = document.querySelector('#night-mode');
            if (!nightMode) return;

            // When clicked, toggle night mode on or off
            nightMode.addEventListener('click', function(event) {
                event.preventDefault();
                document.documentElement.classList.toggle('night-mode');
                if (document.documentElement.classList.contains('night-mode')) {
                    localStorage.setItem('gmtNightMode', true);
                    return;
                }
                localStorage.removeItem('gmtNightMode');
            }, false);

        })(window, document);

        // Preloader
        var spinneroverlay = document.getElementById("spinneroverlay");
        window.addEventListener('load', function() {
            spinneroverlay.style.display = 'none';
        });
    </script>
</body>

</html>