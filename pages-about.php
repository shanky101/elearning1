<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="assets/images/favicon.png">
    <meta name="description" content="">
    <title> Front About </title>
    <!-- Favicon -->
    <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">

    <!-- Your stylesheet-->
    <link rel="stylesheet" href="assets/css/uikit.css">
    <link rel="stylesheet" href="assets/css/main.css">

    <!-- font awesome -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">

    <!--  javascript -->
    <script src="assets/js/simplebar.js"></script>
    <script src="assets/js/uikit.js"></script>
</head>

<body>
    <!-- PreLoader -->
    <div id="spinneroverlay">
        <div class="spinner"></div>
    </div>
    <!-- Nav for  mobile  -->
    <div class="tm-mobile-header uk-hidden@m">
        <nav class="uk-navbar-container uk-navbar" uk-navbar="">
            <div class="uk-navbar-left">
                <a class="uk-hidden@m uk-logo" href="index.php" style="left: 0;"> IJBridge </a>
            </div>
            <div class="uk-navbar-right">
                <a class="uk-navbar-toggle" href="#tm-mobile" uk-toggle>
                    <div class="uk-navbar-toggle-icon">
                        <i class="far fa-bars icon-large uk-text-black"></i>
                    </div>
                </a>
            </div>
        </nav>
        <!-- model mobile menu -->
        <div id="tm-mobile" class="uk-modal-full uk-modal" uk-modal>
            <div class="uk-modal-dialog uk-modal-body uk-text-center uk-flex" uk-height-viewport>
                <button class="uk-modal-close-full uk-close uk-icon" type="button" uk-close></button>
                <div class="uk-margin-auto-vertical uk-width-1-1">
                    <div class="uk-child-width-1-1" uk-grid>
                        <div>
                            <div class="uk-panel">
                                <ul class="uk-nav uk-nav-primary uk-nav-center nav-white">
                                    <li>
                                        <a href="index.php">Home</a>
                                    </li>

                                    <li class="uk-active">
                                        <a href="landing-about.php">About</a>
                                    </li>

                                    <li>
                                        <a href="authentication.php">Sing in</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div>
                            <div class="uk-panel widget-text" id="widget-text-1">
                                <div class="textwidget">
                                    <p class="uk-text-center"><a class="uk-button uk-button-success uk-button-large" href="authentication.php"> Start now </a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Navbar   -->
    <div class="tm-header uk-visible@m tm-header-transparent uk-margin-top">
        <div uk-sticky media="768" animation="uk-animation-slide-top" cls-active="uk-navbar-sticky uk-nav-dark" sel-target=".uk-navbar-container" top=".tm-header ~ [class*=&quot;uk-section&quot;], .tm-header ~ * > [class*=&quot;uk-section&quot;]" cls-inactive="uk-navbar-transparent   uk-dark" class="uk-sticky uk-navbar-container">
            <div class="uk-container  uk-position-relative">
                <nav class="uk-navbar uk-navbar-transparent">
                    <!-- logo -->
                    <div class="uk-navbar-left">
                        <a href="#" class="uk-logo"><i class="fas fa-graduation-cap"></i> IJBridge</a>
                    </div>
                    <!-- right navbar  -->
                    <div class="uk-navbar-right">
                        <ul class="uk-navbar-nav toolbar-nav">
                            <li>
                                <a href="index.php">Home</a>
                            </li>

                            <li class="uk-active">
                                <a href="landing-about.php">About</a>
                            </li>

                            <li>
                                <a href="authentication.php">Japnese</a>
                            </li>
                        </ul>
                        <a class="el-content uk-button uk-button-success uk-circle" href="authentication.php">Sign In </a>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <div class="uk-section-default uk-height-large" uk-scrollspy="{&quot;target&quot;:&quot;[uk-scrollspy-class]&quot;,&quot;cls&quot;:&quot;uk-animation-slide-top-small&quot;,&quot;delay&quot;:false}" tm-header-transparent="dark" tm-header-transparent-placeholder="">
        <div data-src="assets/images/backgrounds/about.png" uk-img class="uk-background-norepeat uk-background-center-center uk-background-image@s uk-background-fixe uk-section uk-section-large uk-padding-remove-top uk-flex uk-flex-middle uk-background-cover" uk-height-viewport="offset-top: true" style="    background-color: rgba(0, 0, 0, 0.52);background-blend-mode: color-burn;">
            <div class="uk-container">
                <div class="uk-grid-margin uk-grid uk-grid-stack" uk-grid>
                    <div class="uk-width-1-1@m uk-text-center">
                        <h1 class=" uk-animation-slide-top-small uk-text-white"> About us </h1>
                        <p class="uk-text-white uk-text-large">
                            Deliver top quality products and services to customers.</br>Our goal of corporate activities is to enhance customer competitiveness.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="uk-section-default uk-section">
        <div class="uk-margin-auto uk-container-small">
            <h1 class="uk-text-center"> Company Profile</h1>
            <div>
                <table class="table table-striped text-left">
                    <tbody>
                        <tr>
                            <th scope="row">Company Profile</th>
                            <td>
                                <ul class="gaiyou_ul">
                                    <li>Aerospace and automobile,design engineering service</li>
                                    <li>Reverse engineering service</li>
                                    <li>CAD・CAE・Embedded software service</li>
                                    <li>Business consulting service</li>
                                    <li>IT consulting service</li>
                                    <li>Global training service</li>
                                    <li>Staffing service【 一般派遣事業No：派23-302215 有料職業紹介事業 No：23-ﾕ-301366 】</li>

                                </ul>
                            </td>
                        </tr>

                        <tr>
                            <th scope="row">Establishment</th>
                            <td>Feburuary,2014</td>
                        </tr>

                        <tr>
                            <th scope="row">Head Office</th>
                            <td>
                                <p>〒460-0011 OsuHonmachi Bldg 2F 3-1-76 Osu,Naka-ku,Nagoya,Aichi,Japan<br>
                                    TEL 052-684-5917 FAX 052-684-4097</p>
                            </td>
                        </tr>

                        <tr>
                            <th scope="row">Osaka Office</th>
                            <td>
                                <p>〒540-0013 Linkstyle Chuo Bldg.204 4-1-19 Uchikyuhoji-machi,Chio-ku,Osaka,Japan<br>
                                    TEL 06-6360-9963 FAX 06-6360-9960</p>
                            </td>
                        </tr>

                        <tr>
                            <th scope="row">India base</th>
                            <td>
                                <p>iJbridge Tech Innovations Pvt. Ltd.　Managing Director Mandar Kale<br>808, suratwala mark Plazo Near Fortune inn, Hinjewadi phase 1,Hinjewadi, Pune, Maharashtra India Pin: 411057
                                    Arnekt Solutions Pvt. Ltd.</br>Managing Director Mandar Kale,Director Vilas Shewale<br>
                                    302, Pentagon P-3, Magarpatta City, Hadapsar, Pune, Maharashtra India Pin: 411013</p>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Capital</th>
                            <td>
                                <p>\20,000,000</p>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">CEO</th>
                            <td>
                                <p>Bachhao Rajeev</p>
                            </td>
                        </tr>


                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- footer -->
    <div class="uk-section-small  uk-section-default">
        <hr class="uk-margin-remove">
        <div class="uk-container uk-align-center uk-margin-remove-bottom uk-position-relative">
            <div class="uk-margin-small" uk-grid>
                <div class="uk-width-1-3@m uk-width-1-2@s uk-first-column">
                    <a href="#" class="uk-link-heading uk-text-lead uk-text-bold"> <i class="fas fa-graduation-cap"></i> IJBridge </a>
                    <div class="uk-width-xlarge tm-footer-description">Deliver top quality products and services to customers.</br>Our goal of corporate activities is to enhance customer competitiveness.</div>
                </div>
                <div class="uk-width-expand@m uk-width-1-2@s">
                    <ul class="uk-list  tm-footer-list">

                        <li>
                            <a href="#">Articles </a>
                        </li>

                        <li>
                            <a href="#"> Browse our courses</a>
                        </li>
                    </ul>
                </div>
                <div class="uk-width-expand@m uk-width-1-2@s">
                    <ul class="uk-list tm-footer-list">
                        <li>
                            <a href="#"> About us </a>
                        </li>
                        <li>
                            <a href="#"> Contact Us </a>
                        </li>
                        <li>
                            <a href="#"> Privacy </a>
                        </li>
                        <li>
                            <a href="#"> Policy </a>
                        </li>
                    </ul>
                </div>
                <div class="uk-width-expand@m uk-width-1-2@s">
                    <ul class="uk-list  tm-footer-list">
                        <li>
                            <a href="#">MATLAB </a>
                        </li>
                        <li>
                            <a href="#">Cybersecurity </a>
                        </li>
                        <li>
                            <a href="#"> CAD/CAM </a>
                        </li>

                    </ul>
                </div>
            </div>
            <hr>
            <p class="uk-postion-absoult uk-margin-remove uk-position-bottom-right" style="bottom: 8px;right: 60px;" uk-tooltip="title: Visit Our Site; pos: top-center"> Powered By <a href="#" class="uk-text-bold uk-link-reset"> IJBridge </a></p>
            <div class="uk-margin-small" uk-grid>
                <div class="uk-width-1-2@m uk-width-1-2@s uk-first-column">
                    <p class="uk-text-small"><i class="far fa-copyright"></i> 2019 <span class="uk-text-bold">IJBridge</span> . All rights reserved.</p>
                </div>
                <div class="uk-width-1-3@m uk-width-1-2@s">
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Youtube Chanal; pos: top-center"><i class="fab fa-youtube" style=" color: #fb7575  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Facebook; pos: top-center"><i class="fab fa-Facebook" style=" color: #9160ec  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Instagram; pos: top-center"><i class="fab fa-Instagram" style=" color: #dc2d2d  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our linkedin; pos: top-center"><i class="fab fa-linkedin " style=" color: #6949a5  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our google-plus; pos: top-center"><i class="fab fa-google-plus" style=" color: #f77070 !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Twitter; pos: top-center"><i class="fab fa-twitter" style=" color: #6f23ff !important;"></i></a>
                </div>
            </div>
        </div>
    </div>
    <script>
        // Preloader
        var spinneroverlay = document.getElementById("spinneroverlay");
        window.addEventListener('load', function() {
            spinneroverlay.style.display = 'none';
        });
    </script>
</body>

</html>