<?php
// Initialize the session
session_start();

// Check if the user is already logged in, if yes then redirect him to welcome page
if (!isset($_SESSION["loggedin"])) {
    header("location: authentication.php");
    exit;
}

// Include config file
require_once "config.php";

//setting the quiz results to zero at start
$total_correct = "0";
$total_questions = "0";
$passing_status = false;
$retry_status = "NA";

if (isset($_SESSION["total_correct"]) && !empty($_SESSION["total_correct"])) {
    $total_correct = trim($_SESSION["total_correct"]);
}
if (isset($_SESSION["total_questions"]) && !empty($_SESSION["total_questions"])) {
    $total_questions = trim($_SESSION["total_questions"]);
}
if (isset($_SESSION["focus_areas"]) && !empty($_SESSION["focus_areas"])) {
    $focus_areas = $_SESSION["focus_areas"];
}

if ($total_correct <= 6) {
    $passing_status = false;
    $retry_status = "FromTop";
} else if ($total_correct > 6 && $total_correct <= 10) {
    $passing_status = true;
    $retry_status = "WithFocusAreas";
} 

//setting the username to empty at start
$name = "";

// Check if the user is already logged in, if yes then redirect him to welcome page
if (isset($_SESSION["name"]) && !empty($_SESSION["name"])) {
    //updating the user's name from session data
    $name = trim($_SESSION["name"]);
}
?>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="assets/images/favicon.png">
    <meta name="description" content="">
    <title> Results </title>
    <!-- Favicon -->
    <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Your stylesheet-->
    <link rel="stylesheet" href="assets/css/uikit.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/update.css">
    <!-- font awesome -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <!--  javascript -->
    <script src="assets/js/simplebar.js"></script>
    <script src="assets/js/uikit.js"></script>
</head>

<body>
    <!-- PreLoader -->
    <div id="spinneroverlay">
        <div class="spinner"></div>
    </div>
    <!-- header  -->
    <header class="tm-header" uk-sticky>
        <div class=" uk-background-grey uk-navbar-container uk-navbar-transparent uk-padding-small uk-navbar-sticky">
            <div class="uk-position-relative">
                <nav class="uk-navbar-transparent tm-mobile-header uk-animation-slide-top uk-position-z-index" uk-navbar>
                    <!-- logo -->
                    <!-- mobile icon for side nav on nav-mobile-->
                    <span class="uk-hidden@m tm-mobile-menu-icon" uk-toggle="target: #mobile-sidebar"><i class="fas fa-bars icon-large"></i></span>
                    <!-- mobile icon for user icon on nav-mobile -->
                    <span class="uk-hidden@m tm-mobile-user-icon uk-align-right" uk-toggle="target: #tm-show-on-mobile; cls: tm-show-on-mobile-active"><i class="fas fa-user icon-large"></i></span>
                    <!-- mobile logo -->
                    <a class="uk-hidden@m uk-logo" href="index.php"> <?php echo $platform_name; ?></a>
                    <div class="uk-navbar-left uk-visible@m">
                        <a href="index.php" class="uk-logo uk-margin-left"> <i class="fas fa-graduation-cap"> </i>
                            <?php echo $platform_name; ?></a>
                    </div>
                    <div class="uk-navbar-right tm-show-on-mobile uk-flex-right" id="tm-show-on-mobile">
                        <!-- this will clouse after display user icon -->
                        <span class="uk-hidden@m tm-mobile-user-close-icon uk-align-right" uk-toggle="target: #tm-show-on-mobile; cls: tm-show-on-mobile-active"><i class="fas fa-times icon-large"></i></span>
                        <ul class="uk-navbar-nav uk-flex-middle">
                            <li>
                                <!-- your courses -->
                                <a href="#"> <i class="fas fa-play uk-hidden@m"></i> <span class="uk-visible@m"> Your
                                        Courses</span> </a>
                                <div uk-dropdown="pos: top-right ;mode : click; animation: uk-animation-slide-bottom-medium" class="uk-dropdown border-radius-6  uk-dropdown-top-right tm-dropdown-large uk-padding-remove">
                                    <div class="uk-clearfix">
                                        <div class="uk-float-left">
                                            <h5 class="uk-padding-small uk-margin-remove uk-text-bold  uk-text-left">
                                                Your Courses </h5>
                                        </div>
                                        <div class="uk-float-right">
                                            <i class="fas fa-check uk-align-right  uk-margin-remove uk-margin-remove-left  uk-padding-small uk-text-small">
                                                Completed 2 / 4 </i>
                                        </div>
                                    </div>
                                    <hr class=" uk-margin-remove">
                                    <div class="uk-padding-smaluk-text-left uk-height-medium">
                                        <div class="demo1" data-simplebar>
                                            <div class="uk-child-width-1-2@s  uk-grid-small uk-padding-small" uk-scrollspy="target: > div; cls:uk-animation-slide-bottom-small; delay: 100 ;repeat: true" uk-grid>
                                                <div>
                                                    <a href="course-view.php" class="uk-link-reset">
                                                        <div class="uk-padding-small uk-card-default">
                                                            <progress id="js-progressbar" class="uk-progress progress-green uk-margin-small-bottom" value="100" max="100" style="height: 7px;"></progress>
                                                            <img src="assets/images/courses/tags/css3.JPG" class="uk-align-left  uk-margin-small-right uk-margin-small-bottom  uk-width-1-3  uk-visible@s" alt="">
                                                            <p class="uk-text-bold uk-margin-remove">CSS3 Introduction
                                                            </p>
                                                            <p class="uk-text-small uk-margin-remove"> by : John Doe
                                                            </p>
                                                            <div class="uk-margin-small">
                                                                <a class="Course-tags uk-margin-small-right   border-radius-6" href="course-lesson.php"> <i class="fas fa-play"></i> Resume</a>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div>
                                                    <a href="course-view.php" class="uk-link-reset">
                                                        <div class="uk-padding-small uk-card-default">
                                                            <progress id="js-progressbar" class="uk-progress progress-coral  uk-margin-small-bottom" value="15" max="100" style="height: 7px !important;"></progress>
                                                            <img src="assets/images/courses/tags/html5.jpg" class="uk-align-left  uk-margin-small-right uk-margin-small-bottom  uk-width-1-3  uk-visible@s" alt="">
                                                            <p class="uk-text-bold uk-margin-remove">MATLAB Introduction
                                                            </p>
                                                            <p class="uk-text-small uk-margin-remove"> by : John Doe
                                                            </p>
                                                            <div class="uk-margin-small">
                                                                <a class="Course-tags uk-margin-small-right   border-radius-6" href="course-lesson.php"> <i class="fas fa-play"></i> Resume</a>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div>
                                                    <a href="course-view.php" class="uk-link-reset">
                                                        <div class="uk-padding-small uk-card-default">
                                                            <progress id="js-progressbar" class="uk-progress progress-coral uk-margin-small-bottom" value="50" max="100" style="height: 7px;"></progress>
                                                            <img src="assets/images/courses/tags/css3.JPG" class="uk-align-left  uk-margin-small-right uk-margin-small-bottom  uk-width-1-3  uk-visible@s" alt="">
                                                            <p class="uk-text-bold uk-margin-remove">PHP Introduction
                                                            </p>
                                                            <p class="uk-text-small uk-margin-remove"> by : John Doe
                                                            </p>
                                                            <div class="uk-margin-small">
                                                                <a class="Course-tags uk-margin-small-right   border-radius-6" href="course-lesson.php"> <i class="fas fa-play"></i> Resume</a>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div>
                                                    <a href="course-view.php" class="uk-link-reset">
                                                        <div class="uk-padding-small uk-card-default">
                                                            <progress id="js-progressbar" class="uk-progress progress-green uk-margin-small-bottom" value="100" max="100" style="height: 7px;"></progress>
                                                            <img src="assets/images/courses/tags/css3.JPG" class="uk-align-left  uk-margin-small-right uk-margin-small-bottom  uk-width-1-3  uk-visible@s" alt="">
                                                            <p class="uk-text-bold uk-margin-remove">Python Introduction
                                                            </p>
                                                            <p class="uk-text-small uk-margin-remove"> by : John Doe
                                                            </p>
                                                            <div class="uk-margin-small">
                                                                <a class="Course-tags uk-margin-small-right   border-radius-6" href="course-lesson.php"> <i class="fas fa-play"></i> Resume</a>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class=" uk-margin-remove">
                                    <h5 class="uk-padding-small uk-margin-remove uk-text-bold uk-text-center"><a class="uk-link-heading" href="#"> See all </a> </h5>
                                </div>
                            </li>
                            <li>
                                <!-- User profile -->
                                <a href="#">
                                    <img src="assets/images/avatures/avature-2.png" alt="" class="uk-border-circle user-profile-tiny">
                                </a>
                                <div uk-dropdown="pos: top-right ;mode : click ;animation: uk-animation-slide-right" class="uk-dropdown uk-padding-small uk-dropdown-top-right  angle-top-right">
                                    <p class="uk-margin-remove-bottom uk-margin-small-top uk-text-bold"> <?php echo $name; ?>
                                    </p>
                                    <p class="uk-margin-remove-top uk-text-small uk-margin-small-bottom"> 
                                    </p>
                                    <ul class="uk-nav uk-dropdown-nav">
                                        <li>
                                            <a href="profile.php"> <i class="fas fa-user uk-margin-small-right"></i>
                                                Profile</a>
                                        </li>
                                        <li>
                                            <a href="#"> <i class="fas fa-cog uk-margin-small-right"></i> Setting</a>
                                        </li>
                                        <li class="uk-nav-divider"></li>
                                        <li>
                                            <a href="logout.php"> <i class="fas fa-sign-out-alt uk-margin-small-right"></i> Log out</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- Navigation for mobile -->
                    <div id="mobile-sidebar" class="mobile-sidebar" uk-offcanvas="overlay:true">
                        <div class="uk-offcanvas-bar uk-preserve-color uk-padding-remove">
                            <ul uk-accordion>
                                <li class="uk-open">
                                    <a href="#" class="uk-accordion-title uk-text-black uk-padding-small"> <i class="fas fa-play-circle uk-margin-small-right"></i> Courses </a>
                                    <div class="uk-accordion-content uk-margin-remove-top">
                                        <ul class="uk-list tm-drop-topic-list">
                                            <li>
                                                <a href="#"> All Development</a>
                                            </li>
                                            <li>
                                                <a href="#"> Web Development </a>
                                            </li>
                                            <li>
                                                <a href="#"> Mobile App </a>
                                            </li>
                                            <li>
                                                <a href="#"> Programming language </a>
                                            </li>
                                            <li>
                                                <a href="#"> Software </a>
                                            </li>
                                            <li>
                                                <a href="#"> Ecommerce </a>
                                            </li>
                                            <li>
                                                <a href="#"> Training</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- search box -->
                    <div id="modal-full" class="uk-modal-full uk-modal uk-animation-scale-down" uk-modal>
                        <div class="uk-modal-dialog uk-flex uk-flex-center" uk-height-viewport>
                            <button class="uk-modal-close-full" type="button" uk-close></button>
                            <form class="uk-search uk-margin-xlarge-top uk-search-large uk-animation-slide-bottom-medium">
                                <i class="fas fa-search uk-position-absolute uk-margin-top icon-xxlarge"></i>
                                <input class="uk-search-input uk-margin-large-left" type="search" placeholder="Search..." autofocus>
                            </form>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <!-- Navigation  -->
    <div class="uk-navbar-sticky uk-background-grey">
        <nav class="uk-padding-small uk-visible@m uk-navbar-transparent uk-animation-slide-to uk-position-z-index" uk-navbar>
            <div class="uk-flex uk-flex-center uk-width-expand">
                <ul class="uk-subnav uk-subnav-2">
                    <li class="uk-active">
                        <a href="#"> <i class="fas fa-play icon-medium uk-margin-small-right"></i> MATLAB </a>
                        <!-- drop topic list -->
                        <div uk-drop="pos: top-left ;mode:click ; offset: 10;animation: uk-animation-slide-bottom-small" class="uk-drop angle-top-left">
                            <div class="tm-drop-topic">
                                <div class="uk-grid-collapse uk-grid-match" uk-grid>
                                    <div class="uk-width-1-2">
                                        <ul uk-tab="connect: #component-tab-left;animation: uk-animation-slide-left-small, uk-animation-slide-right-small" class="tm-drop-topic-list uk-card uk-card-default uk-margin-remove uk-box-shadow-large">
                                            <li>
                                                <a href="#"> <i class="far fa-credit-card  uk-margin-small-right"></i> Web Development <i class="fas fa-chevron-right uk-position-center-right uk-margin-right"></i> </a>
                                            </li>
                                            <li>
                                                <a href="#"> <i class="fas fa-briefcase   uk-margin-small-right"></i> Business <i class="fas fa-chevron-right uk-position-center-right uk-margin-right"></i> </a>
                                            </li>
                                            <li>
                                                <a href="#"> <i class="fas fa-pencil-ruler  uk-margin-small-right"></i> Office Productivity <i class="fas fa-chevron-right uk-position-center-right uk-margin-right"></i> </a>
                                            </li>
                                            <li>
                                                <a href="#"> <i class="fas fa-brain  uk-margin-small-right"></i> Personal Development <i class="fas fa-chevron-right uk-position-center-right uk-margin-right"></i> </a>
                                            </li>
                                            <li>
                                                <a href="#"> <i class="fas fa-bullhorn uk-margin-small-right"></i> Marketing <i class="fas fa-chevron-right uk-position-center-right uk-margin-right"></i> </a>
                                            </li>
                                            <li>
                                                <a href="#"> <i class="fas fa-life-ring  uk-margin-small-right"></i> Life Style <i class="fas fa-chevron-right uk-position-center-right uk-margin-right"></i> </a>
                                            </li>
                                            <li>
                                                <a href="#"> <i class="fas fa-camera uk-margin-small-right"></i> Photography <i class="fas fa-chevron-right uk-position-center-right uk-margin-right"></i> </a>
                                            </li>
                                            <li>
                                                <a href="#"> <i class="fas fa-briefcase-medical uk-margin-small-right"></i> Health & Fitness <i class="fas fa-chevron-right uk-position-center-right uk-margin-right"></i> </a>
                                            </li>
                                            <li>
                                                <a href="#"> <i class="fas fa-shopping-bag  uk-margin-small-right"></i> Ecommerce <i class="fas fa-chevron-right uk-position-center-right uk-margin-right"></i> </a>
                                            </li>
                                            <li>
                                                <a href="#"> <i class="fas fa-utensils  uk-margin-small-right"></i> Food & cooking <i class="fas fa-chevron-right uk-position-center-right uk-margin-right"></i> </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="uk-width-1-2 uk-width-1-2">
                                        <ul id="component-tab-left" class="uk-switcher uk-card uk-card-default uk-box-shadow-large">
                                            <li>
                                                <!-- Web Development list -->
                                                <ul class="tm-drop-topic-list uk-padding-remove">
                                                    <li>
                                                        <a href="#"> All Development</a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Web Development </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Mobile App </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Programming language </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Game Development </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Software </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Development tools </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Ecommerce </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Training</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <!-- Business courses list -->
                                                <ul class="tm-drop-topic-list uk-padding-remove">
                                                    <li>
                                                        <a href="#"> All Business </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Finance </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Sales </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Startegy </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Sales </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Other </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <!-- IT & Software  courses list -->
                                                <ul class="tm-drop-topic-list uk-padding-remove">
                                                    <li>
                                                        <a href="#"> Hadware </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Operating system </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Network </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Other </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <!--Personal development  courses list -->
                                                <ul class="tm-drop-topic-list uk-padding-remove">
                                                    <li>
                                                        <a href="#"> Leadership </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Productivity </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Personal Finance </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Other </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <!-- Marketing  courses list -->
                                                <ul class="tm-drop-topic-list uk-padding-remove">
                                                    <li>
                                                        <a href="#"> All Marketing </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Digital Marketing </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Search Engine Optimization </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Other </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <!-- Life style  courses list -->
                                                <ul class="tm-drop-topic-list uk-padding-remove">
                                                    <li>
                                                        <a href="#"> Food </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Travel </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Gaming </a>
                                                    </li>
                                                    <li>
                                                        <a href="#"> Other </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="#"> <i class="fas fa-book-open icon-medium uk-margin-small-right"></i> Security </a>
                    </li>
                    <li>
                        <a href="#"> <i class="fas fa-file-alt icon-medium uk-margin-small-right"></i> CAD/CAM </a>
                    </li>
                    <li>
                        <a href="#"> <i class="fas fa-code icon-medium uk-margin-small-right"></i> Web </a>
                    </li>
                    <li>
                        <a href="#"> <i class="fas fa-clone icon-medium uk-margin-small-right"></i> Pages </a>
                        <div class="tm-dropdown-medium" uk-dropdown="mode:click ;">
                            <div class="uk-dropdown-grid uk-child-width-1-2@m" uk-grid>
                                <div>
                                    <ul class="uk-nav uk-dropdown-nav">
                                        <li>
                                            <a href="pages-about.php"> About Us </a>
                                        </li>
                                        <li>
                                            <a href="pages-contact.php"> Contact Us </a>
                                        </li>
                                        <li>
                                            <a href="pages-terms.php"> Terms &amp; Conditions </a>
                                        </li>
                                        <li>
                                            <a href="pages-faqs.php"> FAQs </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <main>
        <div class="uk-container">
            <div class="uk-margin-medium-top uk-grid-large" uk-grid>
                <div class="uk-width-2-3@m">
                    <div class="uk-margin-medium-bottom uk-text-left">
                    </div>
                    <article class="uk-article">
                        <p class="uk-text-lead uk-text-bold uk-text-white">Hi <?php echo "$name"; ?>,</p>
                        <p>Results: <?php echo "$total_correct / $total_questions correct"; ?></p>
                        <?php
                        if ($passing_status == true & $retry_status == "WithFocusAreas") {
                            echo "<p>You have successfully completed the \"Introduction to MATLAB\" course.</p>";
                            echo "<p>Please focus on below sections </p>";
                            for ($x = 0; $x < count($focus_areas); $x++) {
                                echo "<div class=\"uk-width-1-3@m\">
                                    <div class=\"vertical-menu active\" uk-sticky=\"offset: 160 ;bottom: true\">
                                        <a class=\"active\">$focus_areas[$x]</a>
                                    </div>
                                </div>";
                                echo "<br>";
                            }
                        } else if ($passing_status == false & $retry_status == "FromTop") {
                            echo "<p>Simulink Basicコースに合格していないことをお知らせします。</p>";
                            echo "<p>このコースをもう一度受講することをお勧めします.</p>";
                        }
                        ?>
                    </article>
                </div>
                <?php
                if ($passing_status == true) {
                    echo "
                    <div class=\"uk-width-1-3@m\">
                        <div class=\"vertical-menu\" uk-sticky=\"offset: 160 ;bottom: true\">
                            <a href=\"assets/images/courses/course-result.png\" class=\"active\">Download Certificate</a>
                        </div>
                    </div>";
                }
                ?>
            </div>
        </div>
    </main>
    <!-- footer -->
    <div class="uk-section-small uk-margin-medium-top">
        <hr class="uk-margin-remove">
        <div class="uk-container uk-align-center uk-margin-remove-bottom uk-position-relative">
            <div uk-grid>
                <div class="uk-width-1-3@m uk-width-1-2@s uk-first-column">
                    <a href="pages-about.php" class="uk-link-heading uk-text-lead uk-text-bold"> <i class="fas fa-graduation-cap"></i> <?php echo $platform_name; ?></a>
                    <div class="uk-width-xlarge tm-footer-description">最高品質の製品とサービスを顧客に提供します。 企業活動の目標は、顧客の競争力を高めることです。</div>
                </div>
                <div class="uk-width-expand@m uk-width-1-2@s">
                    <ul class="uk-list  tm-footer-list">
                        <li>
                            <a href="#"> Browse Our Library </a>
                        </li>
                        <li>
                            <a href="#"> Tutorials/Articles </a>
                        </li>
                        <li>
                            <a href="#"> Scripts and codes</a>
                        </li>
                        <li>
                            <a href="#"> Ebooks</a>
                        </li>
                    </ul>
                </div>
                <div class="uk-width-expand@m uk-width-1-2@s">
                    <ul class="uk-list tm-footer-list">
                        <li>
                            <a href="#"> About us </a>
                        </li>
                        <li>
                            <a href="#"> Contact Us </a>
                        </li>
                        <li>
                            <a href="#"> Privacy </a>
                        </li>
                        <li>
                            <a href="#"> Policy </a>
                        </li>
                    </ul>
                </div>
                <div class="uk-width-expand@m uk-width-1-2@s">
                    <ul class="uk-list  tm-footer-list">
                        <li>
                            <a href="#">Web Design </a>
                        </li>
                        <li>
                            <a href="#">Web Development </a>
                        </li>
                        <li>
                            <a href="#"> iOS Development </a>
                        </li>
                        <li>
                            <a href="#"> PHP Development </a>
                        </li>
                    </ul>
                </div>
            </div>
            <hr>
            <p class="uk-postion-absoult uk-margin-remove uk-position-bottom-right" style="bottom: 8px;right: 60px;" uk-tooltip="title: Visit Our Site; pos: top-center"> Powered By <a href="#" target="_blank" class="uk-text-bold uk-link-reset"> <?php echo $platform_name; ?></a></p>
            <div class="uk-margin-small" uk-grid>
                <div class="uk-width-1-2@m uk-width-1-2@s uk-first-column">
                    <p class="uk-text-small"><i class="fas fa-copyright"></i> 2019 <span class="uk-text-bold"><?php echo $platform_name; ?></span> . All rights reserved.</p>
                </div>
                <div class="uk-width-1-3@m uk-width-1-2@s">
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Youtube Chanal; pos: top-center"><i class="fab fa-youtube" style=" color: #fb7575  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Facebook; pos: top-center"><i class="fab fa-Facebook" style=" color: #9160ec  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Instagram; pos: top-center"><i class="fab fa-Instagram" style=" color: #dc2d2d  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our linkedin; pos: top-center"><i class="fab fa-linkedin " style=" color: #6949a5  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our google-plus; pos: top-center"><i class="fab fa-google-plus" style=" color: #f77070 !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Twitter; pos: top-center"><i class="fab fa-twitter" style=" color: #6f23ff !important;"></i></a>
                </div>
            </div>
        </div>
    </div>
    <!-- footer  end -->
    <!-- app end -->
    <!-- button scrollTop -->
    <button id="scrollTop" class="uk-animation-slide-bottom-medium">
        <a href="#" class="uk-text-white" uk-totop uk-scroll></a>
    </button>
    <!--  Night mood -->
    <script>
        (function(window, document, undefined) {
            'use strict';
            if (!('localStorage' in window)) return;
            var nightMode = localStorage.getItem('gmtNightMode');
            if (nightMode) {
                document.documentElement.className += ' night-mode';
            }
        })(window, document);


        (function(window, document, undefined) {

            'use strict';

            // Feature test
            if (!('localStorage' in window)) return;

            // Get our newly insert toggle
            var nightMode = document.querySelector('#night-mode');
            if (!nightMode) return;

            // When clicked, toggle night mode on or off
            nightMode.addEventListener('click', function(event) {
                event.preventDefault();
                document.documentElement.classList.toggle('night-mode');
                if (document.documentElement.classList.contains('night-mode')) {
                    localStorage.setItem('gmtNightMode', true);
                    return;
                }
                localStorage.removeItem('gmtNightMode');
            }, false);

        })(window, document);

        // Preloader
        var spinneroverlay = document.getElementById("spinneroverlay");
        window.addEventListener('load', function() {
            spinneroverlay.style.display = 'none';
        });

        //scrollTop
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("scrollTop").style.display = "block";
            } else {
                document.getElementById("scrollTop").style.display = "none";
            }
        }
        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
</body>

</html>