<?php
// Initialize the session
session_start();

// Check if the user is already logged in, if yes then redirect him to welcome page
if (!isset($_SESSION["loggedin"])) {
    header("location: authentication.php");
    exit;
}

// Processing form data when form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST['quiz_submit']) && $_POST['quiz_submit']) {

        $section_names = array("Gain", "Absolute", "2D LUT", "Bus creator", "Bus selector", "Data Dictionary", "Multiport Switch", "Manual Switch", "Bit Clear", "Bitwise Operator", "Ramp", "Scope", "Intro Simulink", "MBD Intro");
        $focus_areas = array();


          if (isset($_POST['question-1-answers']) && $_POST['question-1-answers']) {
            $answer1 = $_POST['question-1-answers'];
        } else {
            $answer1 = "";
        }
        if (isset($_POST['question-2-answers']) && $_POST['question-2-answers']) {
            $answer2 = $_POST['question-2-answers'];
        } else {
            $answer2 = "";
        }
        if (isset($_POST['question-3-answers']) && $_POST['question-3-answers']) {
            $answer3 = $_POST['question-3-answers'];
        } else {
            $answer3 = "";
        }
        if (isset($_POST['question-4-answers']) && $_POST['question-4-answers']) {
            $answer4 = $_POST['question-4-answers'];
        } else {
            $answer4 = "";
        }
        if (isset($_POST['question-5-answers']) && $_POST['question-5-answers']) {
            $answer5 = $_POST['question-5-answers'];
        } else {
            $answer5 = "";
        }
        if (isset($_POST['question-6-answers']) && $_POST['question-6-answers']) {
            $answer6 = $_POST['question-6-answers'];
        } else {
            $answer6 = "";
        }
        if (isset($_POST['question-7-answers']) && $_POST['question-7-answers']) {
            $answer7 = $_POST['question-7-answers'];
        } else {
            $answer7 = "";
        }
        if (isset($_POST['question-8-answers']) && $_POST['question-8-answers']) {
            $answer8 = $_POST['question-8-answers'];
        } else {
            $answer8 = "";
        }
        if (isset($_POST['question-9-answers']) && $_POST['question-9-answers']) {
            $answer9 = $_POST['question-9-answers'];
        } else {
            $answer9 = "";
        }
        if (isset($_POST['question-10-answers']) && $_POST['question-10-answers']) {
            $answer10 = $_POST['question-10-answers'];
        } else {
            $answer10 = "";
        }
        if (isset($_POST['question-11-answers']) && $_POST['question-11-answers']) {
            $answer11 = $_POST['question-11-answers'];
        } else {
            $answer11 = "";
        }
        if (isset($_POST['question-12-answers']) && $_POST['question-12-answers']) {
            $answer12 = $_POST['question-12-answers'];
        } else {
            $answer12 = "";
        }
        if (isset($_POST['question-13-answers']) && $_POST['question-13-answers']) {
            $answer13 = $_POST['question-13-answers'];
        } else {
            $answer13 = "";
        }
        if (isset($_POST['question-14-answers']) && $_POST['question-14-answers']) {
            $answer14 = $_POST['question-14-answers'];
        } else {
            $answer14 = "";
        }
        if (isset($_POST['question-15-answers']) && $_POST['question-15-answers']) {
            $answer15 = $_POST['question-15-answers'];
        } else {
            $answer15 = "";
        }
        if (isset($_POST['question-16-answers']) && $_POST['question-16-answers']) {
            $answer16 = $_POST['question-16-answers'];
        } else {
            $answer16 = "";
        }
        if (isset($_POST['question-17-answers']) && $_POST['question-17-answers']) {
            $answer17 = $_POST['question-17-answers'];
        } else {
            $answer17 = "";
        }
        if (isset($_POST['question-18-answers']) && $_POST['question-18-answers']) {
            $answer18 = $_POST['question-18-answers'];
        } else {
            $answer18 = "";
        }
        if (isset($_POST['question-19-answers']) && $_POST['question-19-answers']) {
            $answer19 = $_POST['question-19-answers'];
        } else {
            $answer19 = "";
        }
        if (isset($_POST['question-20-answers']) && $_POST['question-20-answers']) {
            $answer20 = $_POST['question-20-answers'];
        } else {
            $answer20 = "";
        }
        if (isset($_POST['question-21-answers']) && $_POST['question-21-answers']) {
            $answer21 = $_POST['question-21-answers'];
        } else {
            $answer21 = "";
        }
        if (isset($_POST['question-22-answers']) && $_POST['question-22-answers']) {
            $answer22 = $_POST['question-22-answers'];
        } else {
            $answer22 = "";
        }
        if (isset($_POST['question-23-answers']) && $_POST['question-23-answers']) {
            $answer23 = $_POST['question-23-answers'];
        } else {
            $answer23 = "";
        }
        if (isset($_POST['question-24-answers']) && $_POST['question-24-answers']) {
            $answer24 = $_POST['question-24-answers'];
        } else {
            $answer24 = "";
        }
        if (isset($_POST['question-25-answers']) && $_POST['question-25-answers']) {
            $answer25 = $_POST['question-25-answers'];
        } else {
            $answer25 = "";
        }
        if (isset($_POST['question-26-answers']) && $_POST['question-26-answers']) {
            $answer26 = $_POST['question-26-answers'];
        } else {
            $answer26 = "";
        }
        if (isset($_POST['question-27-answers']) && $_POST['question-27-answers']) {
            $answer27 = $_POST['question-27-answers'];
        } else {
            $answer27 = "";
        }
        if (isset($_POST['question-28-answers']) && $_POST['question-28-answers']) {
            $answer28 = $_POST['question-28-answers'];
        } else {
            $answer28 = "";
        }
        if (isset($_POST['question-29-answers']) && $_POST['question-29-answers']) {
            $answer29 = $_POST['question-29-answers'];
        } else {
            $answer29 = "";
        }
        if (isset($_POST['question-30-answers']) && $_POST['question-30-answers']) {
            $answer30 = $_POST['question-30-answers'];
        } else {
            $answer30 = "";
        }

        $totalCorrect = 0;

        if ($answer1 == "A") {
            $totalCorrect++;
        }
        if ($answer2 == "C") {
            $totalCorrect++;
        }

        if ($answer1 != "A" || $answer2 == "C") {
            array_push($focus_areas, $section_names[0]);
        }

        if ($answer3 == "B") {
            $totalCorrect++;
        }
        if ($answer4 == "D") {
            $totalCorrect++;
        }

        if ($answer3 != "B" || $answer4 != "D") {
            array_push($focus_areas, $section_names[1]);
        }

        if ($answer5 == "D") {
            $totalCorrect++;
        }
        if ($answer6 == "C") {
            $totalCorrect++;
        }

        if ($answer5 != "D" || $answer6 != "C") {
            array_push($focus_areas, $section_names[2]);
        }

        if ($answer7 == "D") {
            $totalCorrect++;
        }
        if ($answer8 == "C") {
            $totalCorrect++;
        }
        if ($answer9 == "A") {
            $totalCorrect++;
        }

        if ($answer7 != "D" || $answer8 != "C" || $answer9 != "A") {
            array_push($focus_areas, $section_names[3]);
        }

        if ($answer10 == "D") {
            $totalCorrect++;
        }
        if ($answer11 == "C") {
            $totalCorrect++;
        }
        if ($answer12 == "A") {
            $totalCorrect++;
        }

        if ($answer10 != "D" || $answer11 != "C" || $answer12 != "A") {
            array_push($focus_areas, $section_names[4]);
        }

        if ($answer13 == "B") {
            $totalCorrect++;
        }
        if ($answer14 == "D") {
            $totalCorrect++;
        }

        if ($answer13 != "B" || $answer14 != "D") {
            array_push($focus_areas, $section_names[5]);
        }

        if ($answer15 == "B") {
            $totalCorrect++;
        }
        if ($answer16 == "A") {
            $totalCorrect++;
        }

        if ($answer15 != "B" || $answer16 != "A") {
            array_push($focus_areas, $section_names[6]);
        }

        if ($answer17 == "B") {
            $totalCorrect++;
        }
        if ($answer18 == "C") {
            $totalCorrect++;
        }

        if ($answer17 != "B" || $answer18 != "C") {
            array_push($focus_areas, $section_names[7]);
        }

        if ($answer19 == "C") {
            $totalCorrect++;
        }
        if ($answer20 == "A") {
            $totalCorrect++;
        }

        if ($answer19 != "C" || $answer20 != "A") {
            array_push($focus_areas, $section_names[8]);
        }

        if ($answer21 == "C") {
            $totalCorrect++;
        } else {
            array_push($focus_areas, $section_names[9]);
        }

        if ($answer22 == "C") {
            $totalCorrect++;
        }
        if ($answer23 == "C") {
            $totalCorrect++;
        }

        if ($answer22 != "C" || $answer23 != "C") {
            array_push($focus_areas, $section_names[10]);
        }

        if ($answer24 == "B") {
            $totalCorrect++;
        }
        if ($answer25 == "A") {
            $totalCorrect++;
        }

        if ($answer24 != "B" || $answer25 != "A") {
            array_push($focus_areas, $section_names[11]);
        }

        if ($answer26 == "A") {
            $totalCorrect++;
        }
        if ($answer27 == "A") {
            $totalCorrect++;
        }

        if ($answer26 != "A" || $answer27 != "A") {
            array_push($focus_areas, $section_names[12]);
        }

        if ($answer28 == "B") {
            $totalCorrect++;
        }
        if ($answer29 == "D") {
            $totalCorrect++;
        }
        if ($answer30 == "A") {
            $totalCorrect++;
        }

        if ($answer28 != "B" || $answer29 != "D" || $answer30 != "A") {
            array_push($focus_areas, $section_names[13]);
        }


        // Store data in session variables
        $_SESSION["total_correct"] = $totalCorrect;
        $_SESSION["total_questions"] = "30";
        $_SESSION["focus_areas"] = $focus_areas;

        // Redirect user to welcome page
        header("location: course-result.php");
    }
}
?>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="assets/images/favicon.png">
    <meta name="description" content="">
    <title> Keep Learning </title>
    <!-- Favicon -->
    <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Your stylesheet-->
    <link rel="stylesheet" href="assets/css/uikit.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/update.css">
    <!-- font awesome -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <!--  javascript -->
    <script src="assets/js/simplebar.js"></script>
    <script src="assets/js/uikit.js"></script>
</head>

<body>
    <!-- Flash Message -->
    <div id="note">
        Congratulations, you have successfully registered this course. Enjoy it.
        <a class="uk-margin-medium-right uk-margin-remove-bottom uk-position-relative uk-icon-button uk-align-right  uk-margin-small-top" uk-toggle="target: #note; cls:uk-hidden"> <i class="fas fa-times  uk-position-center"></i> </a>
    </div>
    <div class="tm-course-lesson">
        <!-- mobile-sidebar  -->
        <i class="fas fa-video icon-large tm-side-right-mobile-icon uk-hidden@m" uk-toggle="target: #filters"></i>
        <div class="uk-grid-collapse" id="course-fliud" uk-grid>
            <div class="uk-width-3-4@m uk-margin-auto">
                <header class="tm-course-content-header uk-background-grey">
                    <a href="course-view.php" class="back-to-dhashboard uk-margin-small-left" uk-tooltip="title: Back to Course Dashboard  ; delay: 200 ; pos: bottom-left ;animation:uk-animation-scale-up ; offset:20">
                        Course Dashboard</a>
                </header>
                <!--Course-side icon make Hidden sidebar -->
                <i class="fas fa-angle-right icon-large uk-float-right tm-side-course-icon  uk-visible@m" uk-toggle="target: #course-fliud; cls: tm-course-fliud" uk-tooltip="title: Hide sidebar  ; delay: 200 ; pos: bottom-right ;animation:uk-animation-scale-up ; offset:20"></i>
                <!--Course-side active icon -->
                <i class="fas fa-angle-right icon-large uk-float-right tm-side-course-active-icon uk-visible@m" uk-toggle="target: #course-fliud; cls: tm-course-fliud" uk-tooltip="title: Open sidebar  ; delay: 200 ; pos: bottom-right ;animation:uk-animation-scale-up ; offset:20"></i>
                <!-- PreLoader -->
                <div id="spinneroverlay">
                    <div class="spinner"></div>
                </div>
                <ul id="component-nav" class="uk-switcher uk-position-relative uk-padding">
                    <!--  Lesson one -->
                    <li>
                        <div class="navigation-controls">
                            <a href="#" class="previous uk-animation-fade" uk-switcher-item="previous" uk-tooltip="title: Previous Video  ; pos: right ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-left"></i> </a>
                            <a href="#" class="next uk-animation-fade" uk-switcher-item="next" uk-tooltip="title: Next Video  ; pos: left ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-right"></i> </a>
                        </div>
                        <div class="video-responsive">
                            <iframe id="myvid1" src="https://player.vimeo.com/video/383703314" frameborder="0" uk-video="automute: true" allowfullscreen uk-responsive></iframe>
                        </div>
                        <br>
                    </li>
                    <!--  Lesson two  -->
                    <li>
                        <div class="navigation-controls">
                            <a href="#" class="previous uk-animation-fade" uk-switcher-item="previous" uk-tooltip="title: Previous Video  ; pos: right ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-left"></i> </a>
                            <a href="#" class="next uk-animation-fade" uk-switcher-item="next" uk-tooltip="title: Next Video  ; pos: left ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-right"></i> </a>
                        </div>
                        <div class="video-responsive">
                            <iframe id="myvid2" src="https://player.vimeo.com/video/383745665" frameborder="0" allowfullscreen uk-responsive></iframe>
                        </div>
                        <br>
                    </li>
                    <!--  Lesson three -->
                    <li>
                        <div class="navigation-controls">
                            <a href="#" class="previous uk-animation-fade" uk-switcher-item="previous" uk-tooltip="title: Previous Video  ; pos: right ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-left"></i> </a>
                            <a href="#" class="next uk-animation-fade" uk-switcher-item="next" uk-tooltip="title: Next Video  ; pos: left ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-right"></i> </a>
                        </div>
                        <div class="video-responsive">
                            <iframe id="myvid3" src="https://player.vimeo.com/video/383700978" frameborder="0" allowfullscreen uk-responsive></iframe>
                        </div>
                        <br>
                    </li>
                    <!--  Lesson Four -->
                    <li>
                        <div class="navigation-controls">
                            <a href="#" class="previous uk-animation-fade" uk-switcher-item="previous" uk-tooltip="title: Previous Video  ; pos: right ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-left"></i> </a>
                            <a href="#" class="next uk-animation-fade" uk-switcher-item="next" uk-tooltip="title: Next Video  ; pos: left ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-right"></i> </a>
                        </div>
                        <div class="video-responsive">
                            <iframe id="myvid4" src="https://player.vimeo.com/video/383701360" frameborder="0" allowfullscreen uk-responsive></iframe>
                        </div>
                        <br>
                    </li>
                    <!--  Lesson five -->
                    <li>
                        <div class="navigation-controls">
                            <a href="#" class="previous uk-animation-fade" uk-switcher-item="previous" uk-tooltip="title: Previous Video  ; pos: right ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-left"></i> </a>
                            <a href="#" class="next uk-animation-fade" uk-switcher-item="next" uk-tooltip="title: Next Video  ; pos: left ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-right"></i> </a>
                        </div>
                        <div class="video-responsive">
                            <iframe id="myvid5" src="https://player.vimeo.com/video/383702022" frameborder="0" allowfullscreen uk-responsive></iframe>
                        </div>
                        <br>
                    </li>
                    <!--  Lesson  -->
                    <li>
                        <div class="navigation-controls">
                            <a href="#" class="previous uk-animation-fade" uk-switcher-item="previous" uk-tooltip="title: Previous Video  ; pos: right ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-left"></i> </a>
                            <a href="#" class="next uk-animation-fade" uk-switcher-item="next" uk-tooltip="title: Next Video  ; pos: left ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-right"></i> </a>
                        </div>
                        <div class="video-responsive">
                            <iframe id="myvid6" src="https://player.vimeo.com/video/383702337" frameborder="0" allowfullscreen uk-responsive></iframe>
                        </div>
                        <br>
                    </li>
                    <!--  Lesson  -->
                    <li>
                        <div class="navigation-controls">
                            <a href="#" class="previous uk-animation-fade" uk-switcher-item="previous" uk-tooltip="title: Previous Video  ; pos: right ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-left"></i> </a>
                            <a href="#" class="next uk-animation-fade" uk-switcher-item="next" uk-tooltip="title: Next Video  ; pos: left ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-right"></i> </a>
                        </div>
                        <div class="video-responsive">
                            <iframe id="myvid7" src="https://player.vimeo.com/video/383703101" frameborder="0" allowfullscreen uk-responsive></iframe>
                        </div>
                        <br>
                    </li>
                    <!--  Lesson  -->
                    <li>
                        <div class="navigation-controls">
                            <a href="#" class="previous uk-animation-fade" uk-switcher-item="previous" uk-tooltip="title: Previous Video  ; pos: right ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-left"></i> </a>
                            <a href="#" class="next uk-animation-fade" uk-switcher-item="next" uk-tooltip="title: Next Video  ; pos: left ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-right"></i> </a>
                        </div>
                        <div class="video-responsive">
                            <iframe id="myvid8" src="https://player.vimeo.com/video/383703662" frameborder="0" allowfullscreen uk-responsive></iframe>
                        </div>
                        <br>
                    </li>
                    <!--  Lesson  -->
                    <li>
                        <div class="navigation-controls">
                            <a href="#" class="previous uk-animation-fade" uk-switcher-item="previous" uk-tooltip="title: Previous Video  ; pos: right ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-left"></i> </a>
                            <a href="#" class="next uk-animation-fade" uk-switcher-item="next" uk-tooltip="title: Next Video  ; pos: left ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-right"></i> </a>
                        </div>
                        <div class="video-responsive">
                            <iframe id="myvid9" src="https://player.vimeo.com/video/383700532" frameborder="0" allowfullscreen uk-responsive></iframe>
                        </div>
                        <br>
                    </li>
                    <!--  Lesson  -->
                    <li>
                        <div class="navigation-controls">
                            <a href="#" class="previous uk-animation-fade" uk-switcher-item="previous" uk-tooltip="title: Previous Video  ; pos: right ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-left"></i> </a>
                            <a href="#" class="next uk-animation-fade" uk-switcher-item="next" uk-tooltip="title: Next Video  ; pos: left ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-right"></i> </a>
                        </div>
                        <div class="video-responsive">
                            <iframe id="myvid10" src="https://player.vimeo.com/video/383700282" frameborder="0" allowfullscreen uk-responsive></iframe>
                        </div>
                        <br>
                    </li>
                    <!--  Lesson  -->
                    <li>
                        <div class="navigation-controls">
                            <a href="#" class="previous uk-animation-fade" uk-switcher-item="previous" uk-tooltip="title: Previous Video  ; pos: right ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-left"></i> </a>
                            <a href="#" class="next uk-animation-fade" uk-switcher-item="next" uk-tooltip="title: Next Video  ; pos: left ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-right"></i> </a>
                        </div>
                        <div class="video-responsive">
                            <iframe id="myvid11" src="https://player.vimeo.com/video/383700043" frameborder="0" allowfullscreen uk-responsive></iframe>
                        </div>
                        <br>
                    </li>
                    <!--  Lesson  -->
                    <li>
                        <div class="navigation-controls">
                            <a href="#" class="previous uk-animation-fade" uk-switcher-item="previous" uk-tooltip="title: Previous Video  ; pos: right ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-left"></i> </a>
                            <a href="#" class="next uk-animation-fade" uk-switcher-item="next" uk-tooltip="title: Next Video  ; pos: left ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-right"></i> </a>
                        </div>
                        <div class="video-responsive">
                            <iframe id="myvid12" src="https://player.vimeo.com/video/383699799" frameborder="0" allowfullscreen uk-responsive></iframe>
                        </div>
                        <br>
                    </li>
                    <!--  Lesson  -->
                    <li>
                        <div class="navigation-controls">
                            <a href="#" class="previous uk-animation-fade" uk-switcher-item="previous" uk-tooltip="title: Previous Video  ; pos: right ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-left"></i> </a>
                            <a href="#" class="next uk-animation-fade" uk-switcher-item="next" uk-tooltip="title: Next Video  ; pos: left ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-right"></i> </a>
                        </div>
                        <div class="video-responsive">
                            <iframe id="myvid13" src="https://player.vimeo.com/video/383701592" frameborder="0" allowfullscreen uk-responsive></iframe>
                        </div>
                        <br>
                    </li>
                    <!--  Lesson  -->
                    <li>
                        <div class="navigation-controls">
                            <a href="#" class="previous uk-animation-fade" uk-switcher-item="previous" uk-tooltip="title: Previous Video  ; pos: right ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-left"></i> </a>
                            <a href="#" class="next uk-animation-fade" uk-switcher-item="next" uk-tooltip="title: Next Video  ; pos: left ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-right"></i> </a>
                        </div>
                        <div class="video-responsive">
                            <iframe id="myvid14" src="https://player.vimeo.com/video/383694714" frameborder="0" allowfullscreen uk-responsive></iframe>
                        </div>
                        <br>
                    </li>
                    <!--  Lesson  -->
                    <li>
                        <div class="navigation-controls">
                            <a href="#" class="previous uk-animation-fade" uk-switcher-item="previous" uk-tooltip="title: Previous Video  ; pos: right ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-left"></i> </a>
                            <a href="#" class="next uk-animation-fade" uk-switcher-item="next" uk-tooltip="title: Next Video  ; pos: left ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-right"></i> </a>
                        </div>
                        <div class="video-responsive">
                            <iframe id="myvid15" src="https://player.vimeo.com/video/383695107" frameborder="0" allowfullscreen uk-responsive></iframe>
                        </div>
                        <br>
                    </li>

                    <!--  Quiz -->
                    <li>
                        <form id="loginForm" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                            <div class="uk-scrollable-content uk-background-white">
                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q1. The gain value can be positive, negative or floating, but never zero</p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-1-answers" value="A"> True</label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-1-answers" value="B"> False</label>
                                    <br><br>
                                </div>
                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q2. Gain parameter is 5 and the input value is 10. The output value is </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-2-answers" value="A"> 05</label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-2-answers" value="B"> 10</label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-2-answers" value="C"> 50</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-2-answers" value="D"> 25</label>
                                </div>
                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q3. Absolute block output is always ……… </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-3-answers" value="A"> Negative</label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-3-answers" value="B"> Positive</label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-3-answers" value="C"> Both</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-3-answers" value="D"> Neither</label>
                                </div>
                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q4. Absolute block is present in …………… section of library browser</p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-4-answers" value="A"> Sinks</label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-4-answers" value="B"> Sources</label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-4-answers" value="C"> Logic & bit operations</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-4-answers" value="D"> Math Operations</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q5. The input to output correspondence of 1D LUT is</p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-5-answers" value="A">1:2 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-5-answers" value="B">1:1 </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-5-answers" value="C"> 1:3</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-5-answers" value="D"> 2:1</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q6. The diagnostic method for input of LUT can be </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-6-answers" value="A">error </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-6-answers" value="B">warning </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-6-answers" value="C"> all </label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-6-answers" value="D"> none</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q7. The diagnostic method for input of LUT can be </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-7-answers" value="A">same type </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-7-answers" value="B">different tyype </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-7-answers" value="C"> constant</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-7-answers" value="D"> all</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q8. A bus creator has ………… relation between input and output </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-8-answers" value="A">one to one </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-8-answers" value="B">one to many </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-8-answers" value="C"> many to one</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-8-answers" value="D"> none</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q9. At the output of the bus creator we get an </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-9-answers" value="A">Stucture </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-9-answers" value="B">array </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-9-answers" value="C"> enum</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-9-answers" value="D"> union</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q10. The outputs of a bus creator can be </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-10-answers" value="A">same type </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-10-answers" value="B">different type </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-10-answers" value="C"> constant</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-10-answers" value="D"> all</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q11. The inputs to a bus selector can be rearranged </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-11-answers" value="A">Up </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-11-answers" value="B">Down </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-11-answers" value="C"> Either</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-11-answers" value="D"> None</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q12. A bus selector has ………… relation between input and output </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-12-answers" value="A">one to many </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-12-answers" value="B">one to one </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-12-answers" value="C"> many to one</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-12-answers" value="D"> none</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q13. Data dictionary provides additional option of change tracking and management </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-13-answers" value="A">False </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-13-answers" value="B">True </label>
                                    <br><br>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q14. The data dictionary stores </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-14-answers" value="A">parameter </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-14-answers" value="B">input </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-14-answers" value="C"> Output</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-14-answers" value="D"> All</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q15. The first input is ..... And last input is ………… </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-15-answers" value="A">Deafult,Select </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-15-answers" value="B">Select,Default </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-15-answers" value="C"> in1,in last</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-15-answers" value="D"> in1,in3</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q16. Multiport switch acts like a </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-16-answers" value="A">Mux </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-16-answers" value="B">Demux </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-16-answers" value="C"> Bus selector</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-16-answers" value="D"> All of the above</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q17. Manual switch does not behave same as single pole double throw switch </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-17-answers" value="A">False </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-17-answers" value="B">True </label>
                                    <br><br>

                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q18. When ground is connected at the input, it is considered as </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-18-answers" value="A">One </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-18-answers" value="B">Vcc</label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-18-answers" value="C"> 0</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-18-answers" value="D">Unknown</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q19. The Bit Clear block does not supports </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-19-answers" value="A">Integer</label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-19-answers" value="B">fixed point </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-19-answers" value="C"> Enumerated</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-19-answers" value="D"> Boolen</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q20. If the bit clear block is turned on for bit 1, bit is set to </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-20-answers" value="A">0 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-20-answers" value="B">1 </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-20-answers" value="C"> Dont care</label>
                                    <br><br>

                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q21. The Bitwise Operator block does not support shift operations. </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-21-answers" value="A">True </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-21-answers" value="B">False </label>
                                    <br><br>

                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q22. The Ramp block's Which parameters determine the characteristics of the output signal. </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-22-answers" value="A">Slope </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-22-answers" value="B">Start Time </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-22-answers" value="C"> Initial Output</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-22-answers" value="D"> All</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q23. Data type of ramp block is . </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-23-answers" value="A">Integer </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-23-answers" value="B">Fixed point </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-23-answers" value="C"> Enumarted</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-23-answers" value="D"> Double</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q24. Scope block is used for___ ? </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-24-answers" value="A">To generate the input signals </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-24-answers" value="B">To check the output </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-24-answers" value="C">All </label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q25. For the scope block, Plot multiple signals on the same ​y-axis (display) using multiple input ports. </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-25-answers" value="A">True </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-25-answers" value="B">False </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-25-answers" value="C">None </label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q26. The workspace contains variables that you create or import into MATLAB from data files or other programs. </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-26-answers" value="A">True </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-26-answers" value="B">False </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-26-answers" value="C">None </label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q27. The Command Window enables you to enter individual statements at the command line and view the generated results. </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-27-answers" value="A">True </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-27-answers" value="B">False </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-27-answers" value="C">None </label>
                                </div>
                                
                                
                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q28. MBD uses GUI which makes modelling more easy and code generation more convenient </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-28-answers" value="A">False </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-28-answers" value="B">True </label>
                                    
                                </div>
                                
                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q29. MBD is preferred over conventional method because it is  . </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-29-answers" value="A">	Cost effective </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-29-answers" value="B">Improved Quality </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-29-answers" value="C">Faster </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-29-answers" value="C">All </label>
                                </div>
                                
                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q30. Matlab is used only used for model development and not validation. </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-30-answers" value="A">False </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-30-answers" value="B">True </label>
                                    
                                </div>
                                
                                
                                <div class="uk-clearfix uk-margin uk-margin-medium-right uk-animation-slide-bottom">
                                    <div class="uk-float-right">
                                        <input class="uk-button uk-button-success" name="quiz_submit" type="submit" class="button" value="Submit">
                                    </div>
                                </div>

                            </div>
                        </form>
                    </li>
                </ul>
            </div>
            <div class="uk-width-1-4@m uk-offcanvas tm-filters uk-background-default tm-side-course uk-animation-slide-right-medium uk-overflow-hidden" id="filters" uk-offcanvas="overlay: true; container: false; flip: true">
                <div class="uk-offcanvas-bar uk-padding-remove uk-preserve-color">
                    <!-- Sidebar menu-->
                    <ul class="uk-child-width-expand uk-tab tm-side-course-nav uk-margin-remove uk-position-z-index" uk-switcher="animation: uk-animation-slide-right-medium, uk-animation-slide-left-small" style="box-shadow: 0px 0px 7px 1px gainsboro;" uk-switcher>
                        <li class="uk-active">
                            <a href="#" uk-tooltip="title: Course Videos ; delay: 200 ; pos: bottom-left ;animation:uk-animation-scale-up">
                                <i class="fas fa-video icon-medium"></i> Videos </a>
                        </li>
                        <li>
                            <a href="#" uk-tooltip="title: Course Files ; delay: 200 ; pos: bottom-center ;animation:uk-animation-scale-up">
                                <i class="fas fa-archive icon-medium"></i> Contents </a>
                        </li>
                    </ul>
                    <!-- Sidebar contents -->
                    <ul class="uk-switcher">
                        <!-- Course Video tab  -->
                        <li>
                            <div class="demo1" data-simplebar>
                                <ul class="course-list video-list" uk-switcher="connect: #component-nav; animation: uk-animation-slide-right-small, uk-animation-slide-left-medium">
                                    <!--  Lesson one -->
                                    <li>
                                        <a href="#" onclick="stop(document.getElementById('myvid1'));">
                                            <img src="assets/images/courses/course-lesson-video-list.jpg" alt="">
                                            <i class="fas fa-play-circle play-icon"></i>
                                            <span class="now-playing">Now Playing</span>
                                            <span class="uk-text-truncate">Introduction Simulink</span>
                                            <time>3 min. 40 sec.</time>
                                        </a>
                                    </li>
                                    <!--  Lesson two  -->
                                    <li>
                                        <a href="#" onclick="stop(document.getElementById('myvid2'));">
                                            <img src="assets/images/courses/course-lesson-video-list.jpg" alt="">
                                            <i class="fas fa-play-circle play-icon"></i>
                                            <span class="now-playing">Now Playing</span>
                                            <span class="uk-text-truncate"> MBD Intro </span>
                                            <time>5 min. 0 sec.</time>
                                        </a>
                                    </li>
                                    <!--  Lesson three  -->
                                    <li>
                                        <a href="#" onclick="stop(document.getElementById('myvid3'));">
                                            <img src="assets/images/courses/course-lesson-video-list.jpg" alt="">
                                            <i class="fas fa-play-circle play-icon"></i>
                                            <span class="now-playing">Now Playing</span>
                                            <span class="uk-text-truncate"> 2D LUT </span>
                                            <time>3 min. 30 sec.</time>
                                        </a>
                                    </li>
                                    <!--  Lesson Four  -->
                                    <li>
                                        <a href="#" onclick="stop(document.getElementById('myvid4'));">
                                            <img src="assets/images/courses/course-lesson-video-list.jpg" alt="">
                                            <i class="fas fa-play-circle play-icon"></i>
                                            <span class="now-playing">Now Playing</span>
                                            <span class="uk-text-truncate"> Absolute Block </span>
                                            <time>4 min. 50 sec.</time>
                                        </a>
                                    </li>
                                    <!--  Lesson five  -->
                                    <li>
                                        <a href="#" onclick="stop(document.getElementById('myvid5'));">
                                            <img src="assets/images/courses/course-lesson-video-list.jpg" alt="">
                                            <i class="fas fa-play-circle play-icon"></i>
                                            <span class="now-playing">Now Playing</span>
                                            <span class="uk-text-truncate"> Bus Creator </span>
                                            <time>3 min. 20 sec.</time>
                                        </a>
                                    </li>
                                    <!--  Lesson   -->
                                    <li>
                                        <a href="#" onclick="stop(document.getElementById('myvid6'));">
                                            <img src="assets/images/courses/course-lesson-video-list.jpg" alt="">
                                            <i class="fas fa-play-circle play-icon"></i>
                                            <span class="now-playing">Now Playing</span>
                                            <span class="uk-text-truncate"> Bus Selector </span>
                                            <time>3 min. 20 sec.</time>
                                        </a>
                                    </li>
                                    <!--  Lesson   -->
                                    <li>
                                        <a href="#" onclick="stop(document.getElementById('myvid7'));">
                                            <img src="assets/images/courses/course-lesson-video-list.jpg" alt="">
                                            <i class="fas fa-play-circle play-icon"></i>
                                            <span class="now-playing">Now Playing</span>
                                            <span class="uk-text-truncate"> Gain Block </span>
                                            <time>3 min. 20 sec.</time>
                                        </a>
                                    </li>
                                    <!--  Lesson   -->
                                    <li>
                                        <a href="#" onclick="stop(document.getElementById('myvid8'));">
                                            <img src="assets/images/courses/course-lesson-video-list.jpg" alt="">
                                            <i class="fas fa-play-circle play-icon"></i>
                                            <span class="now-playing">Now Playing</span>
                                            <span class="uk-text-truncate"> Manual Switch </span>
                                            <time>3 min. 20 sec.</time>
                                        </a>
                                    </li>
                                    <!--  Lesson   -->
                                    <li>
                                        <a href="#" onclick="stop(document.getElementById('myvid9'));">
                                            <img src="assets/images/courses/course-lesson-video-list.jpg" alt="">
                                            <i class="fas fa-play-circle play-icon"></i>
                                            <span class="now-playing">Now Playing</span>
                                            <span class="uk-text-truncate"> Scope Block </span>
                                            <time>3 min. 20 sec.</time>
                                        </a>
                                    </li>
                                    <!--  Lesson   -->
                                    <li>
                                        <a href="#" onclick="stop(document.getElementById('myvid10'));">
                                            <img src="assets/images/courses/course-lesson-video-list.jpg" alt="">
                                            <i class="fas fa-play-circle play-icon"></i>
                                            <span class="now-playing">Now Playing</span>
                                            <span class="uk-text-truncate"> Relational Operator </span>
                                            <time>3 min. 20 sec.</time>
                                        </a>
                                    </li>
                                    <!--  Lesson   -->
                                    <li>
                                        <a href="#" onclick="stop(document.getElementById('myvid11'));">
                                            <img src="assets/images/courses/course-lesson-video-list.jpg" alt="">
                                            <i class="fas fa-play-circle play-icon"></i>
                                            <span class="now-playing">Now Playing</span>
                                            <span class="uk-text-truncate"> Ramp Block </span>
                                            <time>3 min. 20 sec.</time>
                                        </a>
                                    </li>
                                    <!--  Lesson   -->
                                    <li>
                                        <a href="#" onclick="stop(document.getElementById('myvid12'));">
                                            <img src="assets/images/courses/course-lesson-video-list.jpg" alt="">
                                            <i class="fas fa-play-circle play-icon"></i>
                                            <span class="now-playing">Now Playing</span>
                                            <span class="uk-text-truncate"> Multi Port Switch Block </span>
                                            <time>3 min. 20 sec.</time>
                                        </a>
                                    </li>
                                    <!--  Lesson   -->
                                    <li>
                                        <a href="#" onclick="stop(document.getElementById('myvid13'));">
                                            <img src="assets/images/courses/course-lesson-video-list.jpg" alt="">
                                            <i class="fas fa-play-circle play-icon"></i>
                                            <span class="now-playing">Now Playing</span>
                                            <span class="uk-text-truncate"> Bitwise Operator </span>
                                            <time>3 min. 20 sec.</time>
                                        </a>
                                    </li>
                                    <!--  Lesson   -->
                                    <li>
                                        <a href="#" onclick="stop(document.getElementById('myvid14'));">
                                            <img src="assets/images/courses/course-lesson-video-list.jpg" alt="">
                                            <i class="fas fa-play-circle play-icon"></i>
                                            <span class="now-playing">Now Playing</span>
                                            <span class="uk-text-truncate"> Bit set </span>
                                            <time>3 min. 20 sec.</time>
                                        </a>
                                    </li>
                                    <!--  Lesson   -->
                                    <li>
                                        <a href="#" onclick="stop(document.getElementById('myvid15'));">
                                            <img src="assets/images/courses/course-lesson-video-list.jpg" alt="">
                                            <i class="fas fa-play-circle play-icon"></i>
                                            <span class="now-playing">Now Playing</span>
                                            <span class="uk-text-truncate"> Bit Clear </span>
                                            <time>3 min. 20 sec.</time>
                                        </a>
                                    </li>

                                    <!--  Quiz  -->
                                    <li>
                                        <a href="#" onclick="stop(document.getElementById('myvid1'));">
                                            <img src="assets/images/courses/course-lesson-quiz.png" alt="">
                                            <i class="fas fa-question-circle  play-icon"></i>
                                            <span class="uk-text-truncate"> Quiz </span>
                                            <time>30 Questions</time>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!--  Course resource  tab  -->
                        <li>
                            <div class="demo1 uk-background-default" data-simplebar>
                                <img src="assets/images/courses/course-lesson-icon.jpg" width="200" class="uk-margin-remove-bottom uk-align-center uk-width-2-3" alt="">
                                <p class="uk-padding-small uk-margin-remove uk-text-center uk-padding-remove-top">Lorem
                                    ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore .</p>
                                <h4 class="uk-heading-line uk-text-center uk-margin-top"><span>Requirements </span></h4>
                                <p class="uk-padding-small uk-margin-remove  uk-padding-remove-top uk-text-center">Etiam
                                    sit amet augue non velit aliquet consectetur molestie eros mauris .</p>
                                <h5 class="uk-padding-small uk-margin-remove uk-background-muted uk-text-black"> Web
                                    browser</h5>
                                <div class="uk-grid-small" uk-grid>
                                    <div class="uk-width-1-4  uk-margin-small-top">
                                        <img src="assets/images/icons/software-icon.jpg" alt="Image" class="uk-align-center img-small">
                                    </div>
                                    <div class="uk-width-3-4 uk-padding-remove-left">
                                        <p class="uk-margin-remove-bottom uk-text-bold  uk-text-small uk-margin-small-top">
                                            Firefox </p>
                                        <p class="uk-margin-remove-top uk-margin-small-bottom">Lorem ipsum dolor
                                            adipiscing elit, sed do eiusmod tempor ...</p>
                                        <a class="Course-tags uk-margin-small-right border-radius-6" href="#"> Official
                                            site </a>
                                        <a class="Course-tags uk-margin-small-right  border-radius-6 tags-active" href="#"> Download </a>
                                    </div>
                                </div>
                                <hr class="uk-margin-remove-bottom">
                                <div class="uk-grid-small uk-margin-small-top" uk-grid>
                                    <div class="uk-width-1-4  uk-margin-small-top">
                                        <img src="assets/images/icons/software-icon.jpg" alt="Image" class="uk-align-center img-small">
                                    </div>
                                    <div class="uk-width-3-4 uk-padding-remove-left">
                                        <p class="uk-margin-remove-bottom uk-text-bold  uk-text-small uk-margin-small-top">
                                            Google Chrome </p>
                                        <p class="uk-margin-remove-top uk-margin-small-bottom">Lorem ipsum dolor
                                            adipiscing elit, sed do eiusmod tempor ...</p>
                                        <a class="Course-tags uk-margin-small-right border-radius-6" href="#"> Official
                                            site </a>
                                        <a class="Course-tags uk-margin-small-right  border-radius-6 tags-active" href="#"> Download </a>
                                    </div>
                                </div>
                                <hr class="uk-margin-remove-bottom">
                                <div class="uk-grid-small uk-margin-small-top" uk-grid>
                                    <div class="uk-width-1-4  uk-margin-small-top">
                                        <img src="assets/images/icons/software-icon.jpg" alt="Image" class="uk-align-center img-small">
                                    </div>
                                    <div class="uk-width-3-4 uk-padding-remove-left">
                                        <p class="uk-margin-remove-bottom uk-text-bold  uk-text-small uk-margin-small-top">
                                            UCBrwoser </p>
                                        <p class="uk-margin-remove-top uk-margin-small-bottom">Lorem ipsum dolor
                                            adipiscing elit, sed do eiusmod tempor ...</p>
                                        <a class="Course-tags uk-margin-small-right border-radius-6" href="#"> Official
                                            site </a>
                                        <a class="Course-tags uk-margin-small-right  border-radius-6 tags-active" href="#"> Download </a>
                                    </div>
                                </div>
                                <h5 class="uk-padding-small uk-margin-remove-bottom uk-background-muted uk-text-black">
                                    Text Editor </h5>
                                <div class="uk-grid-small" uk-grid>
                                    <div class="uk-width-1-4  uk-margin-small-top">
                                        <img src="assets/images/icons/software-icon.jpg" alt="Image" class="uk-align-center img-small">
                                    </div>
                                    <div class="uk-width-3-4 uk-padding-remove-left">
                                        <p class="uk-margin-remove-bottom uk-text-bold  uk-text-small uk-margin-small-top">
                                            Atom </p>
                                        <p class="uk-margin-remove-top uk-margin-small-bottom">Lorem ipsum dolor
                                            adipiscing elit, sed do eiusmod tempor ...</p>
                                        <a class="Course-tags uk-margin-small-right border-radius-6" href="#"> Official
                                            site </a>
                                        <a class="Course-tags uk-margin-small-right  border-radius-6 tags-active" href="#"> Download </a>
                                    </div>
                                </div>
                                <hr class="uk-margin-remove-bottom">
                                <div class="uk-grid-small uk-margin-small-top" uk-grid>
                                    <div class="uk-width-1-4  uk-margin-small-top">
                                        <img src="assets/images/icons/software-icon.jpg" alt="Image" class="uk-align-center img-small">
                                    </div>
                                    <div class="uk-width-3-4 uk-padding-remove-left">
                                        <p class="uk-margin-remove-bottom uk-text-bold  uk-text-small uk-margin-small-top">
                                            Dreamweaver </p>
                                        <p class="uk-margin-remove-top uk-margin-small-bottom">Lorem ipsum dolor
                                            adipiscing elit, sed do eiusmod tempor ...</p>
                                        <a class="Course-tags uk-margin-small-right border-radius-6" href="#"> Official
                                            site </a>
                                        <a class="Course-tags uk-margin-small-right  border-radius-6 tags-active" href="#"> Download </a>
                                    </div>
                                </div>
                                <hr class="uk-margin-remove-bottom">
                                <div class="uk-grid-small uk-margin-small-top" uk-grid>
                                    <div class="uk-width-1-4  uk-margin-small-top">
                                        <img src="assets/images/icons/software-icon.jpg" alt="Image" class="uk-align-center img-small">
                                    </div>
                                    <div class="uk-width-3-4 uk-padding-remove-left">
                                        <p class="uk-margin-remove-bottom uk-text-bold  uk-text-small uk-margin-small-top">
                                            Sublime </p>
                                        <p class="uk-margin-remove-top uk-margin-small-bottom">Lorem ipsum dolor
                                            adipiscing elit, sed do eiusmod tempor ...</p>
                                        <a class="Course-tags uk-margin-small-right border-radius-6" href="#"> Official
                                            site </a>
                                        <a class="Course-tags uk-margin-small-right  border-radius-6 tags-active" href="#"> Download </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- app end -->
    <script>
        // Preloader
        var spinneroverlay = document.getElementById("spinneroverlay");
        window.addEventListener('load', function() {
            spinneroverlay.style.display = 'none';
        });

        var currentFrame = document.getElementById('myvid1');

        function stop(targetFrame) {
            if (currentFrame != targetFrame) {
                currentFrame.src = currentFrame.src;
                currentFrame = targetFrame;
            }
        }
    </script>
</body>

</html>