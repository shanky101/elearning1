<?php
// Initialize the session
session_start();



// Include config file
require_once "config.php";

//setting the username to empty at start
$name = "";

// Check if the user is already logged in, if yes then redirect him to welcome page
if (isset($_SESSION["name"]) && !empty($_SESSION["name"])) {
    //updating the user's name from session data
    $name = trim($_SESSION["name"]);
}
?>
<html lang="en">

<head>
    <meta name="google-site-verification" content="E_OkIy4zJzEx6ZZX5NmgjjYbgBemr3XzxTHgr9IL95w" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="assets/images/favicon.png">
    <meta name="description" content="">
    <title> Home </title>
    <!-- Favicon -->
    <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Your stylesheet-->
    <link rel="stylesheet" href="assets/css/uikit.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <!-- font awesome -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <!--  javascript -->
    <script src="assets/js/simplebar.js"></script>
    <script src="assets/js/uikit.js"></script>
</head>

<body>
    <!-- PreLoader -->
    <div id="spinneroverlay">
        <div class="spinner"></div>
    </div>
    <!-- Nav for  mobile  -->
    <div class="tm-mobile-header uk-hidden@m">
        <nav class="uk-navbar-container uk-navbar" uk-navbar="">
            <div class="uk-navbar-left">
                <a class="uk-hidden@m uk-logo" href="index.php" style="left: 0;"> <?php echo $platform_name; ?> </a>
            </div>
            <div class="uk-navbar-right">
                <a class="uk-navbar-toggle" href="#tm-mobile" uk-toggle>
                    <div class="uk-navbar-toggle-icon">
                        <i class="far fa-bars icon-large uk-text-black"></i>
                    </div>
                </a>
            </div>
        </nav>
        <!-- model mobile menu -->
        <div id="tm-mobile" class="uk-modal-full uk-modal" uk-modal>
            <div class="uk-modal-dialog uk-modal-body uk-text-center uk-flex" uk-height-viewport>
                <button class="uk-modal-close-full uk-close uk-icon" type="button" uk-close></button>
                <div class="uk-margin-auto-vertical uk-width-1-1">
                    <div class="uk-child-width-1-1" uk-grid>
                        <div>
                            <div class="uk-panel">
                                <ul class="uk-nav uk-nav-primary uk-nav-center nav-black">
                                    <li class="uk-active">
                                        <a href="index.php"><?php echo $text_home; ?></a>
                                    </li>
                                    
                                    <li>
                                        <a href="index.php?lang=jpn"><?php echo $text_language_name; ?></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div>
                            <div class="uk-panel widget-text" id="widget-text-1">
                                <div class="textwidget">
                                    <p class="uk-text-center"><a class="uk-button uk-button-success uk-button-large" href="authentication.php"> <?php echo $text_sign_in; ?> </a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Navbar   -->
    <div class="tm-header uk-visible@m tm-header-transparent uk-margin-top">
        <div uk-sticky media="768" animation="uk-animation-slide-top" cls-active="uk-navbar-sticky uk-nav-dark" sel-target=".uk-navbar-container" top=".tm-header ~ [class*=&quot;uk-section&quot;], .tm-header ~ * > [class*=&quot;uk-section&quot;]" cls-inactive="uk-navbar-transparent   uk-dark" class="uk-sticky uk-navbar-container">
            <div class="uk-container  uk-position-relative">
                <nav class="uk-navbar uk-navbar-transparent">
                    <!-- logo -->
                    <div class="uk-navbar-left">
                        <a href="#" class="uk-logo"><i class="fas fa-graduation-cap"></i> <?php echo $platform_name; ?></a>
                    </div>
                    <!-- right navbar  -->
                    <div class="uk-navbar-right">
                        <ul class="uk-navbar-nav toolbar-nav">
                            <li class="uk-active">
                                <a href="index.php"><?php echo $text_home; ?></a>
                            </li>
                           
                            <li>
                                <a href="index jp.php">Japanese</a>
                            </li>
                    
                        </ul>
                        <a class="el-content uk-button uk-button-success uk-circle" href="authentication.php"><?php echo $text_sign_in; ?></a>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <div class="uk-section-default" tm-header-transparent="dark" tm-header-transparent-placeholder="">
        <div data-src="assets/images/backgrounds/home-heros.png" uk-img class="uk-background-norepeat uk-background-center-center uk-background-fixe uk-section uk-section-large uk-padding-remove-top uk-flex uk-flex-middle uk-background-cover" uk-height-viewport="offset-top: true" style="box-sizing: border-box; min-height: calc(100vh - 0px);">
            <div class="uk-width-1-1 uk-margin-xlarge-top">
                <div class="uk-container">
                    <div class="uk-grid-margin uk-grid uk-grid-stack" uk-grid="">
                        <div class="uk-width-1-1@m uk-first-column uk-margin-large-top">
                            <h2 class="uk-margin-remove-vertical uk-text-black  uk-h1 uk-scrollspy-inview uk-animation-slide-top-small">
                                A smarter way to learn </h2>
                            <p class="uk-scrollspy-inview uk-light uk-animation-slide-top-small uk-text-medium uk-text-large  uk-text-black">
                                The <?php echo $platform_name; ?> delivers much more than <br> just online courses. You will challenge
                                yourself, <br> deepen your skills . </p>
                            <a class="el-content uk-button uk-button-success uk-button-large  uk-circle" href="authentication.php">
                                Sign In </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="uk-section uk-section-muted" style="background: #eef5fd;">
        <div class="uk-container uk-align-center" uk-scrollspy="cls:uk-animation-fade">
            <div uk-grid>
                <div class="uk-width-2-3@m uk-padding-large uk-padding-remove-bottom">
                    <img src="assets/images/demos/home-laptop.png" alt="">
                </div>
                <div class="uk-width-1-3@m uk-position-relative">
                    <img src="assets/images/demos/oncomputer-gray.png" class="uk-visible@m" style="position: absolute; width: 80%;left: -36%; bottom: 0;" alt="">
                    <h1 uk-scrollspy="cls:uk-animation-slide-right-medium"> Flexible </h1>
                    <p class="uk-scrollspy-inview  uk-animation-slide-top-small uk-text-medium uk-text-large  uk-text-black">
                        Manage your goals and time.<br>Learn Independently.<br>Be a self directed learner.<br>Go beyond the basic mastery.</p>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="uk-section-default uk-section" style="    background: #f3cd3e;">
        <div class="uk-container">
            <div class="uk-flex-middle" uk-grid>
                <div class="uk-width-1-2@m">
                    <h1 class="uk-text-bold uk-text-white"> Designed to help you retain knowledge more effectively </h1>
                </div>
                <div class="uk-width-1-2@m">
                    <img src="assets/images/demos/home-feature-4.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <img src="assets/images/backgrounds/diver-1.png" style="position: absolute; margin-top: -66px;" alt="">
    <div class="uk-section-grey uk-section">
        <div class="uk-container">
            <div class="uk-flex-middle" uk-grid>
                <div class="uk-width-1-2@m">
                    <h1 class="uk-text-bold uk-margin-small-bottom"> Experience the best </h1>
                    <p class="uk-text-medium uk-text-white uk-margin-remove-top ">
                        Get training from market leading professionals</p>
                    <a class="el-content uk-button uk-button-primary uk-button-large" href="authentication.php">
                        Start now </a>
                </div>
                <div class="uk-width-1-2@m uk-flex-first">
                    <img src="assets/images/demos/home-feature-5.png" alt="">
                </div>
            </div>
            <div uk-grid class="uk-child-width-1-2@m uk-text-white">
                <div>
                    <div class="uk-grid-small" uk-grid>
                        <div class="uk-width-1-5">
                            <img src="assets/images/demos/icons/TV Show_50px.png" class="img-small" alt="Image">
                        </div>
                        <div class="uk-width-3-4">
                            <h3> Video Learning </h3>
                            <p>High quality videos to deepen your understanding on topics</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="uk-grid-small" uk-grid>
                        <div class="uk-width-1-5">
                            <img src="assets/images/demos/icons/Discussion Forum_50px.png" class="img-small" alt="Image">
                        </div>
                        <div class="uk-width-3-4">
                            <h3> Discussions </h3>
                            <p>Discuss with your others and improve your knoweldge </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- footer -->
    <div class="uk-section  uk-section-default">
        <div class="uk-container uk-align-center uk-margin-remove-bottom uk-position-relative">
            <div class="uk-margin-small" uk-grid>
                <div class="uk-width-1-3@m uk-width-1-2@s uk-first-column">
                    <a href="pages-about.php" class="uk-link-heading uk-text-lead uk-text-bold"> <i class="fas fa-graduation-cap"></i> <?php echo $platform_name; ?> </a>
                    <div class="uk-width-xlarge tm-footer-description">Deliver top quality products and services to customers.
                        Our goal of corporate activities is to enhance customer competitiveness.</div>
                </div>
                <div class="uk-width-expand@m uk-width-1-2@s">
                    <ul class="uk-list  tm-footer-list">
                        <li>
                            <a href="#"> Browse Our Courses </a>
                        </li>
                        <li>
                            <a href="#"> Articles </a>
                        </li>
                        <li>
                            <a href="#"> Discussions</a>
                        </li>
                    </ul>
                </div>
                <div class="uk-width-expand@m uk-width-1-2@s">
                    <ul class="uk-list tm-footer-list">
                        <li>
                            <a href="pages-about.php"><?php echo $text_about_us; ?></a>
                        </li>
                    <li>
                            <a href="pages-contact.php">Contact Us</a>
                    </li>
                        <li>
                            <a href="pages-contact.php"> Terms &amp; Conditions </a>
                        </li>
                        <li>
                            <a href="#"> FAQs </a>
                        </li>
                    </ul>
                </div>
                <div class="uk-width-expand@m uk-width-1-2@s">
                    <ul class="uk-list  tm-footer-list">
                        <li>
                            <a href="#">MATLAB </a>
                        </li>
                        <li>
                            <a href="#">Cyber Security </a>
                        </li>
                        <li>
                            <a href="#"> CAD/CAM </a>
                        </li>
                    </ul>
                </div>
            </div>
            <hr>
            <p class="uk-postion-absoult uk-margin-remove uk-position-bottom-right" style="bottom: 8px;right: 60px;" uk-tooltip="title: Visit Our Site; pos: top-center"> Powered By <a href="#" class="uk-text-bold uk-link-reset"> <?php echo $platform_name; ?> </a></p>
            <div class="uk-margin-small" uk-grid>
                <div class="uk-width-1-2@m uk-width-1-2@s uk-first-column">
                    <p class="uk-text-small"><i class="far fa-copyright"></i> 2019 <span class="uk-text-bold"><?php echo $platform_name; ?></span> . All rights reserved.</p>
                </div>
                <div class="uk-width-1-3@m uk-width-1-2@s">
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Youtube Channel; pos: top-center"><i class="fab fa-youtube" style=" color: #fb7575  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Facebook Page; pos: top-center"><i class="fab fa-Facebook" style=" color: #9160ec  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Instagram Page; pos: top-center"><i class="fab fa-Instagram" style=" color: #dc2d2d  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our linkedin Page; pos: top-center"><i class="fab fa-linkedin " style=" color: #6949a5  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our google-plus Page; pos: top-center"><i class="fab fa-google-plus" style=" color: #f77070 !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Twitter Page; pos: top-center"><i class="fab fa-twitter" style=" color: #6f23ff !important;"></i></a>
                </div>
            </div>
        </div>
    </div>
    <!-- button scrollTop -->
    <button id="scrollTop" class="uk-animation-slide-bottom-medium">
        <a href="#" class="uk-text-white" uk-totop uk-scroll></a>
    </button>
    <script>
        // Preloader
        var spinneroverlay = document.getElementById("spinneroverlay");
        window.addEventListener('load', function() {
            spinneroverlay.style.display = 'none';
        });
        //scrollTop
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("scrollTop").style.display = "block";
            } else {
                document.getElementById("scrollTop").style.display = "none";
            }
        }
        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
</body>

</html>