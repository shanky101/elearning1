<?php
// Initialize the session
session_start();

// Check if the user is already logged in, if yes then redirect him to welcome page
if (!isset($_SESSION["loggedin"])) {
    header("location: authentication.php");
    exit;
}

// Include config file
require_once "config.php";

//setting the username to empty at start
$name = "";

// Check if the user is already logged in, if yes then redirect him to welcome page
if (isset($_SESSION["name"]) && !empty($_SESSION["name"])) {
    //updating the user's name from session data
    $name = trim($_SESSION["name"]);
}
?>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="assets/images/favicon.png">
    <meta name="description" content="">
    <title> Home </title>
    <!-- Favicon -->
    <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Your stylesheet-->
    <link rel="stylesheet" href="assets/css/uikit.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <!-- font awesome -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <!--  javascript -->
    <script src="assets/js/simplebar.js"></script>
    <script src="assets/js/uikit.js"></script>
</head>

<body>
    <!-- PreLoader -->
    <div id="spinneroverlay">
        <div class="spinner"></div>
    </div>
    <!-- header  -->
    <header class="tm-header" uk-sticky>
        <div class=" uk-background-grey uk-navbar-container uk-navbar-transparent uk-padding-small uk-navbar-sticky">
            <div class="uk-position-relative">
                <nav class="uk-navbar-transparent tm-mobile-header uk-animation-slide-top uk-position-z-index" uk-navbar>
                    <!-- logo -->
                    <!-- mobile icon for side nav on nav-mobile-->
                    <span class="uk-hidden@m tm-mobile-menu-icon" uk-toggle="target: #mobile-sidebar"><i class="fas fa-bars icon-large"></i></span>
                    <!-- mobile icon for user icon on nav-mobile -->
                    <span class="uk-hidden@m tm-mobile-user-icon uk-align-right" uk-toggle="target: #tm-show-on-mobile; cls: tm-show-on-mobile-active"><i class="fas fa-user icon-large"></i></span>
                    <!-- mobile logo -->
                    <a class="uk-hidden@m uk-logo" href="index.php"> <?php echo $platform_name; ?></a>
                    <div class="uk-navbar-left uk-visible@m">
                        <a href="index.php" class="uk-logo uk-margin-left"> <i class="fas fa-graduation-cap"> </i>
                            <?php echo $platform_name; ?></a>
                    </div>
                    <div class="uk-navbar-right tm-show-on-mobile uk-flex-right" id="tm-show-on-mobile">
                        <!-- this will clouse after display user icon -->
                        <span class="uk-hidden@m tm-mobile-user-close-icon uk-align-right" uk-toggle="target: #tm-show-on-mobile; cls: tm-show-on-mobile-active"><i class="fas fa-times icon-large"></i></span>
                        <ul class="uk-navbar-nav uk-flex-middle">
                            <li>
                                <!-- your courses -->
                                <a href="#"> <i class="fas fa-play uk-hidden@m"></i> <span class="uk-visible@m"> Your
                                        Courses</span> </a>
                                <div uk-dropdown="pos: top-right ;mode : click; animation: uk-animation-slide-bottom-medium" class="uk-dropdown border-radius-6  uk-dropdown-top-right tm-dropdown-large uk-padding-remove">
                                    <div class="uk-clearfix">
                                        <div class="uk-float-left">
                                            <h5 class="uk-padding-small uk-margin-remove uk-text-bold  uk-text-left">
                                                Your Courses </h5>
                                        </div>
                                        <div class="uk-float-right">
                                            <i class="fas fa-check uk-align-right  uk-margin-remove uk-margin-remove-left  uk-padding-small uk-text-small">
                                                Completed 2 / 4 </i>
                                        </div>
                                    </div>
                                    <hr class=" uk-margin-remove">
                                    <div class="uk-padding-smaluk-text-left uk-height-medium">
                                        <div class="demo1" data-simplebar>
                                            <div class="uk-child-width-1-2@s  uk-grid-small uk-padding-small" uk-scrollspy="target: > div; cls:uk-animation-slide-bottom-small; delay: 100 ;repeat: true" uk-grid>
                                                <div>
                                                    <a href="course-view.php" class="uk-link-reset">
                                                        <div class="uk-padding-small uk-card-default">
                                                            <progress id="js-progressbar" class="uk-progress progress-green uk-margin-small-bottom" value="100" max="100" style="height: 7px;"></progress>
                                                            <img src="assets/images/courses/tags/css3.JPG" class="uk-align-left  uk-margin-small-right uk-margin-small-bottom  uk-width-1-3  uk-visible@s" alt="">
                                                            <p class="uk-text-bold uk-margin-remove">CSS3 Introduction
                                                            </p>
                                                            <p class="uk-text-small uk-margin-remove"> by : John Doe
                                                            </p>
                                                            <div class="uk-margin-small">
                                                                <a class="Course-tags uk-margin-small-right   border-radius-6" href="#"> <i class="fas fa-play"></i> Resume</a>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div>
                                                    <a href="course-view.php" class="uk-link-reset">
                                                        <div class="uk-padding-small uk-card-default">
                                                            <progress id="js-progressbar" class="uk-progress progress-coral  uk-margin-small-bottom" value="15" max="100" style="height: 7px !important;"></progress>
                                                            <img src="assets/images/courses/tags/html5.jpg" class="uk-align-left  uk-margin-small-right uk-margin-small-bottom  uk-width-1-3  uk-visible@s" alt="">
                                                            <p class="uk-text-bold uk-margin-remove">MATLAB Introduction
                                                            </p>
                                                            <p class="uk-text-small uk-margin-remove"> by : John Doe
                                                            </p>
                                                            <div class="uk-margin-small">
                                                                <a class="Course-tags uk-margin-small-right   border-radius-6" href="course-video.html"> <i class="fas fa-play"></i> Resume</a>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div>
                                                    <a href="course-view.php" class="uk-link-reset">
                                                        <div class="uk-padding-small uk-card-default">
                                                            <progress id="js-progressbar" class="uk-progress progress-coral uk-margin-small-bottom" value="50" max="100" style="height: 7px;"></progress>
                                                            <img src="assets/images/courses/tags/css3.JPG" class="uk-align-left  uk-margin-small-right uk-margin-small-bottom  uk-width-1-3  uk-visible@s" alt="">
                                                            <p class="uk-text-bold uk-margin-remove">PHP Introduction
                                                            </p>
                                                            <p class="uk-text-small uk-margin-remove"> by : John Doe
                                                            </p>
                                                            <div class="uk-margin-small">
                                                                <a class="Course-tags uk-margin-small-right   border-radius-6" href="#"> <i class="fas fa-play"></i> Resume</a>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div>
                                                    <a href="course-view.php" class="uk-link-reset">
                                                        <div class="uk-padding-small uk-card-default">
                                                            <progress id="js-progressbar" class="uk-progress progress-green uk-margin-small-bottom" value="100" max="100" style="height: 7px;"></progress>
                                                            <img src="assets/images/courses/tags/css3.JPG" class="uk-align-left  uk-margin-small-right uk-margin-small-bottom  uk-width-1-3  uk-visible@s" alt="">
                                                            <p class="uk-text-bold uk-margin-remove">Python Introduction
                                                            </p>
                                                            <p class="uk-text-small uk-margin-remove"> by : John Doe
                                                            </p>
                                                            <div class="uk-margin-small">
                                                                <a class="Course-tags uk-margin-small-right   border-radius-6" href="#"> <i class="fas fa-play"></i> Resume</a>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class=" uk-margin-remove">
                                    <h5 class="uk-padding-small uk-margin-remove uk-text-bold uk-text-center"><a class="uk-link-heading" href="#"> See all </a> </h5>
                                </div>
                            </li>
                            <li>
                                <!-- User profile -->
                                <a href="#">
                                    <img src="assets/images/avatures/avature-2.png" alt="" class="uk-border-circle user-profile-tiny">
                                </a>
                                <div uk-dropdown="pos: top-right ;mode : click ;animation: uk-animation-slide-right" class="uk-dropdown uk-padding-small uk-dropdown-top-right  angle-top-right">
                                    <p class="uk-margin-remove-bottom uk-margin-small-top uk-text-bold"> <?php echo $name; ?>
                                    </p>
                                    <p class="uk-margin-remove-top uk-text-small uk-margin-small-bottom"> IJBridge
                                    </p>
                                    <ul class="uk-nav uk-dropdown-nav">
                                        <li>
                                            <a href="profile.php"> <i class="fas fa-user uk-margin-small-right"></i>
                                                Profile</a>
                                        </li>
                                        <li>
                                            <a href="#"> <i class="fas fa-cog uk-margin-small-right"></i> Setting</a>
                                        </li>
                                        <li class="uk-nav-divider"></li>
                                        <li>
                                            <a href="logout.php"> <i class="fas fa-sign-out-alt uk-margin-small-right"></i> Log out</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- Navigation for mobile -->
                    <div id="mobile-sidebar" class="mobile-sidebar" uk-offcanvas="overlay:true">
                        <div class="uk-offcanvas-bar uk-preserve-color uk-padding-remove">
                            <ul uk-accordion>
                                <li class="uk-open">
                                    <a href="#" class="uk-accordion-title uk-text-black uk-padding-small"> <i class="fas fa-play-circle uk-margin-small-right"></i> Courses </a>
                                    <div class="uk-accordion-content uk-margin-remove-top">
                                        <ul class="uk-list tm-drop-topic-list">
                                            <li>
                                                <a href="#"> All Development</a>
                                            </li>
                                            <li>
                                                <a href="#"> Web Development </a>
                                            </li>
                                            <li>
                                                <a href="#"> Mobile App </a>
                                            </li>
                                            <li>
                                                <a href="#"> Programming language </a>
                                            </li>
                                            <li>
                                                <a href="#"> Software </a>
                                            </li>
                                            <li>
                                                <a href="#"> Ecommerce </a>
                                            </li>
                                            <li>
                                                <a href="#"> Training</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- search box -->
                    <div id="modal-full" class="uk-modal-full uk-modal uk-animation-scale-down" uk-modal>
                        <div class="uk-modal-dialog uk-flex uk-flex-center" uk-height-viewport>
                            <button class="uk-modal-close-full" type="button" uk-close></button>
                            <form class="uk-search uk-margin-xlarge-top uk-search-large uk-animation-slide-bottom-medium">
                                <i class="fas fa-search uk-position-absolute uk-margin-top icon-xxlarge"></i>
                                <input class="uk-search-input uk-margin-large-left" type="search" placeholder="Search..." autofocus>
                            </form>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <main>
        <div class="course-intro uk-text-center topic4">
            <h2 class="uk-light uk-text-uppercase uk-text-bold uk-text-white uk-margin-top"> Simulink </h2>
            <p class="uk-light uk-text-bold">The easiest way to learn basics about Simulink.</p>
            <p class="uk-light uk-text-bold uk-text-small"> <i class="fas fa-star icon-small icon-rate"></i> <i class="fas fa-star icon-small icon-rate"></i> <i class="fas fa-star icon-small icon-rate"></i> <i class="far fa-star icon-small icon-rate"></i> <i class="far fa-star icon-small icon-rate"></i> <span class="uk-margin-small-right"> 4.0 (282 ratings) </span> <br> <i class="fas fa-user icon-small uk-margin-small-right"></i> 334 people enrolled</p>
            <a class="uk-button uk-button-white" href="course-video.html" uk-tooltip="title: Start this course now ; delay: 200 ; pos: top ;animation:	uk-animation-scale-up"> Start now</a>
            <a class="uk-button uk-button-white" href="#modal-sections" uk-toggle uk-tooltip="title: watch preview video ; delay: 200 ; pos: top ;animation:	uk-animation-scale-up"> Introduction </a>
            <!-- video demo model -->
            <div id="modal-sections" uk-modal>
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default uk-margin-small-top  uk-margin-small-right uk-light" type="button" uk-close></button>
                    <div class="uk-modal-header topic4 none-border">
                        <b class="uk-text-white uk-text-medium"> Introduction </b>
                    </div>
                    <div class="video-responsive">
                        <iframe src="https://www.youtube.com/embed/YZUqFt9ko9U" class="uk-padding-remove" uk-video="automute: true" frameborder="0" allowfullscreen uk-responsive></iframe>
                    </div>
                    <!-- custom video 
                        <video loop muted playsinline controls uk-video="autoplay: inview"> 
                            <source src="videos/course-intro.mp4" type="video/mp4"> 
                        </video>
                        -->
                    <div class="uk-modal-body">
                        <h3>Build Responsive Websites </h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                    <div class="uk-modal-footer uk-text-right">
                        <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                        <a href="course-dashboard.php">
                            <button class="uk-button uk-button-grey" type="button">Start now </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- navigation-->
        <ul class="uk-tab uk-flex-center  uk-margin-remove-top uk-background-default  uk-sticky" uk-sticky="animation: uk-animation-slide-top; bottom: #sticky-on-scroll-up ; media: @s;" uk-tab>
            <li aria-expanded="true" class="uk-active">
                <a href="#"> Course introduciton </a>
            </li>
            <li>
                <a href="#"> Instructure </a>
            </li>
            <li>
                <a href="#"> Reveiws </a>
            </li>
            <li>
                <a href="#"> Students </a>
            </li>
        </ul>
        <ul class="uk-switcher uk-margin uk-padding-small uk-container-small uk-margin-auto  uk-margin-top">
            <!-- tab 1 About the course -->
            <li class="uk-active">
                <h3> About this Course</h3>
                
                <p> Simulink is a block diagram environment for multidomain simulation and Model-Based Design. It supports system-level design, simulation, automatic code generation, and continuous test and verification of embedded systems. Simulink provides a graphical editor, customizable block libraries, and solvers for modelling and simulating dynamic systems. It is integrated with MATLAB®, enabling you to incorporate MATLAB algorithms into models and export simulation results to MATLAB for further analysis. </p>
               
                <h4> Key Features</h4>
                <ul class="uk-list uk-list-bullet">
                    <li>Graphical editor for building and managing hierarchical block diagrams .</li>
                    <li>Libraries of predefined blocks for modeling continuous-time and discrete-time systems.</li>
                    <li>Simulation engine with fixed-step and variable-step ODE solvers </li>
                    <li>Scopes and data displays for viewing simulation results • Project and data management tools for managing model files and data</li>
                    <li>Model analysis tools for refining model architecture and increasing simulation speed </li>
                    <li>MATLAB Function block for importing MATLAB algorithms into models </li>
                    <li>Legacy Code Tool for importing C and C++ code into models</li>
                </ul>
                <h3>Model-Based Design with Simulink  </h3>
                <p> Modelling is a way to create a virtual representation of a real-world system. You can simulate this virtual representation under a wide range of conditions to see how it behaves. Modelling and simulation are valuable for testing conditions that are difficult to reproduce with hardware prototypes alone. This is especially true in the early phase of the design process when hardware is not yet available. Iterating between modelling and simulation can improve the quality of the system design early, by reducing the number of errors found later in the design process. You can automatically generate code from a model and, when software and hardware implementation requirements are included, create test benches for system verification. Code generation saves time and prevents the introduction of manually coded errors. In Model-Based Design, a system model is at the center of the workflow. Model-Based Design enables fast and cost-effective development of dynamic systems, including control systems, signal processing systems, and communications systems. </p>
                  <h4> Model-Based Design allows you to</h4>
                  <ul class="uk-list uk-list-bullet">
                    <li>Use a common design environment across project teams  .</li>
                    <li>Link designs directly to requirements.</li>
                    <li>Identify and correct errors continuously by integrating testing with design </li>
                    <li>Refine algorithms through multidomain simulation • Automatically generate embedded software code and documentation  </li>
                    <li>Develop and reuse test suites </li>
                    
                  </ul>
            </li>

            <!-- tab 2 About the instructor -->
            <li>
                <h2 class="uk-heading-line uk-text-center"><span> Meet the instructor </span></h2>
                <div class="uk-grid-small  uk-margin-medium-top uk-padding-small" uk-grid>
                    <div class="uk-width-1-4@m uk-first-column">
                        <img alt="Image" class="uk-width-2-3 uk-margin-small-top uk-margin-small-bottom uk-border-circle uk-box-shadow-large  uk-animation-scale-up" src="assets/images/avatures/instructor.jpg">
                        <div class="uk-text-small uk-margin-small-top">
                            <p> <i class="fas fa-star"></i> 4.6 Instructor Rating </p>
                            <p> <i class="fas fa-comment-dots"></i> 89,072Reviews </p>
                            <p> <i class="fas fa-user"></i> 485,420Students </p>
                            <p> <i class="fas fa-play"></i> 4Courses </p>
                        </div>
                    </div>
                    <div class="uk-width-3-4@m uk-padding-remove-left">
                        <h4 class="uk-margin-remove"> Masriam keny </h4>
                        <span class="uk-text-small"> Web Developer, Designer, and Teacher </span>
                        <hr class="uk-margin-small">
                        <p class="uk-margin-remove-top uk-margin-small-bottom">Hi, I'm Jonas! I have Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat .</p>
                        <b> Magna consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud suscipit lobortis nisl ut aliquip ex ea commodo consequat</b>
                        <p> Laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat </p>
                        <p> aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat . </p>
                        <p> Lobortis nisl ut aliquip ex ea commodo consequat . </p>
                    </div>
                </div>
                <h3 class="uk-heading-line uk-text-center uk-margin-medium-top"><span> Other Courses of This Instructure </span></h3>
                <div class="uk-position-relative uk-visible-toggle" uk-slider>
                    <ul class="uk-slider-items ebook uk-child-width-1-3@m uk-child-width-1-3@s uk-grid">
                        <li class="uk-active">
                            <a href="#">
                                <div class="uk-card-default uk-card-hover  uk-card-small Course-card uk-inline-clip">
                                    <img src="assets/images/Courses/course-1.jpg" class="course-img">
                                    <div class="uk-card-body">
                                        <h4>Beginning JavaScript </h4>
                                        <p> Lorem ipsum dolor sit amet tempor consectetur adipiscing </p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="uk-active">
                            <a href="#">
                                <div class="uk-card-default uk-card-hover  uk-card-small Course-card uk-inline-clip">
                                    <img src="assets/images/Courses/course-3.jpg" class="course-img">
                                    <div class="uk-card-body">
                                        <h4>Beginning JavaScript </h4>
                                        <p> Lorem ipsum dolor sit amet tempor consectetur adipiscing </p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="uk-active">
                            <a href="#">
                                <div class="uk-card-default uk-card-hover  uk-card-small Course-card uk-inline-clip">
                                    <img src="assets/images/Courses/course-4.jpg" class="course-img">
                                    <div class="uk-card-body">
                                        <h4>Beginning JavaScript </h4>
                                        <p> Lorem ipsum dolor sit amet tempor consectetur adipiscing </p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="uk-card-default uk-card-hover  uk-card-small Course-card uk-inline-clip">
                                    <img src="assets/images/Courses/course-5.jpg" class="course-img">
                                    <div class="uk-card-body">
                                        <h4>Beginning JavaScript </h4>
                                        <p> Lorem ipsum dolor sit amet tempor consectetur adipiscing </p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="uk-card-default uk-card-hover  uk-card-small Course-card uk-inline-clip">
                                    <img src="assets/images/Courses/course-6.jpg" class="course-img">
                                    <div class="uk-card-body">
                                        <h4>Beginning JavaScript </h4>
                                        <p> Lorem ipsum dolor sit amet tempor consectetur adipiscing </p>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin">
                        <li uk-slider-item="0" class="">
                            <a href="#"></a>
                        </li>
                    </ul>
                </div>
            </li>

            <!-- tab 3 Reveiws -->
            <li>
                <h2 class="uk-heading-line uk-text-center"><span> Reveiws </span></h2>
                <div class="uk-grid-small  uk-margin-medium-top" uk-grid>
                    <div class="uk-width-1-5 uk-first-column">
                        <img alt="Image" class="uk-width-1-2 uk-margin-small-top uk-margin-small-bottom uk-border-circle uk-align-center  uk-box-shadow-large" src="assets/images/avatures/avature-2.png">
                    </div>
                    <div class="uk-width-4-5 uk-padding-remove-left">
                        <div class="uk-float-right">
                            <i class="fas fa-star icon-small"></i>
                            <i class="fas fa-star icon-small"></i>
                            <i class="fas fa-star icon-small"></i>
                            <i class="far fa-star icon-small"></i>
                            <i class="far fa-star icon-small"></i>
                        </div>
                        <h4 class="uk-margin-remove"> Masriam keny </h4>
                        <span class="uk-text-small"> 2 Hourse ago</span>
                        <hr class="uk-margin-small">
                        <p class="uk-margin-remove-top uk-margin-small-bottom">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat</p>
                    </div>
                </div>
                <div class="uk-grid-small  uk-margin-medium-top" uk-grid>
                    <div class="uk-width-1-5 uk-first-column">
                        <img alt="Image" class="uk-width-1-2 uk-margin-small-top uk-margin-small-bottom uk-border-circle uk-align-center  uk-box-shadow-large" src="assets/images/avatures/avature.jpg">
                    </div>
                    <div class="uk-width-4-5 uk-padding-remove-left">
                        <div class="uk-float-right">
                            <i class="fas fa-star icon-small"></i>
                            <i class="fas fa-star icon-small"></i>
                            <i class="fas fa-star icon-small"></i>
                            <i class="far fa-star icon-small"></i>
                            <i class="far fa-star icon-small"></i>
                        </div>
                        <h4 class="uk-margin-remove"> Sasriam keny </h4>
                        <span class="uk-text-small"> 2 Hourse ago</span>
                        <hr class="uk-margin-small">
                        <p class="uk-margin-remove-top uk-margin-small-bottom">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat .</p>
                    </div>
                </div>
                <div class="uk-grid-small uk-margin-medium-top" uk-grid>
                    <div class="uk-width-1-5 uk-first-column">
                        <img alt="Image" class="uk-width-1-2 uk-margin-small-top uk-margin-small-bottom uk-border-circle uk-align-center  uk-box-shadow-large" src="assets/images/avatures/avature-3.png">
                    </div>
                    <div class="uk-width-4-5 uk-padding-remove-left">
                        <div class="uk-float-right">
                            <i class="fas fa-star uk-text-danger icon-small"></i>
                            <i class="fa-star far icon-small"></i>
                            <i class="far fa-star icon-small"></i>
                            <i class="far fa-star icon-small"></i>
                            <i class="far fa-star icon-small"></i>
                        </div>
                        <h4 class="uk-margin-remove"> Masriam keny </h4>
                        <span class="uk-text-small"> 2 Hourse ago</span>
                        <hr class="uk-margin-small">
                        <p class="uk-margin-remove-top uk-margin-small-bottom">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat</p>
                    </div>
                </div>
                <div class="uk-grid-small uk-margin-medium-top" uk-grid>
                    <div class="uk-width-1-5 uk-first-column">
                        <img alt="Image" class="uk-width-1-2 uk-margin-small-top uk-margin-small-bottom uk-border-circle uk-align-center  uk-box-shadow-large" src="assets/images/avatures/avature-2.png">
                    </div>
                    <div class="uk-width-4-5 uk-padding-remove-left">
                        <div class="uk-float-right">
                            <i class="fas fa-star icon-small"></i>
                            <i class="fas fa-star icon-small"></i>
                            <i class="fas fa-star icon-small"></i>
                            <i class="far fa-star icon-small"></i>
                            <i class="far fa-star icon-small"></i>
                        </div>
                        <h4 class="uk-margin-remove"> Masriam keny </h4>
                        <span class="uk-text-small"> 2 Hourse ago</span>
                        <hr class="uk-margin-small">
                        <p class="uk-margin-remove-top uk-margin-small-bottom">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat</p>
                    </div>
                </div>
            </li>

            <!-- tab 4 Students -->
            <li>
                <h3 class="uk-heading-line uk-margin-small-bottom"><span> Students Enorell the course</span></h3>
                <p class="uk-margin-remove-top"> this are the Students takes the course</p>
                <div class="uk-child-width-1-4@m uk-child-width-1-2" uk-scrollspy="target: > div; cls:uk-animation-scale-up; delay: 200" uk-grid>
                    <div>
                        <a href="profile.php" class="uk-link-reset">
                            <div class="uk-padding-remove uk-card-hover uk-card-body uk-width-large border-radius-6 uk-text-center">
                                <img alt="Image" class="uk-width-1-2 uk-margin-top uk-margin-small-bottom uk-border-circle uk-align-center uk-box-shadow-large" src="assets/images/avatures/avature-3.png">
                                <h4 class="uk-margin-remove-bottom uk-margin-remove-top ">Quen smith</h4>
                                <h6 class="uk-margin-small-bottom uk-margin-remove-top "> Beginner fresh</h6>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="profile.php" class="uk-link-reset">
                            <div class="uk-padding-remove uk-card-hover uk-card-body uk-width-large border-radius-6 uk-text-center">
                                <img alt="Image" class="uk-width-1-2 uk-margin-top uk-margin-small-bottom uk-border-circle uk-align-center  uk-box-shadow-large" src="assets/images/avatures/avature-4.png">
                                <h4 class="uk-margin-remove-bottom uk-margin-remove-top "> Stocken smith</h4>
                                <h6 class="uk-margin-small-bottom uk-margin-remove-top "> Beginner fresh</h6>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="profile.php" class="uk-link-reset">
                            <div class="uk-padding-remove uk-card-hover uk-card-body uk-width-large border-radius-6 uk-text-center scale-up-medium">
                                <img alt="Image" class="uk-width-1-2 uk-margin-top uk-margin-small-bottom uk-border-circle uk-align-center  uk-box-shadow-large" src="assets/images/avatures/avature.jpg">
                                <h4 class="uk-margin-remove-bottom uk-margin-remove-top "> Stocken smith</h4>
                                <h6 class="uk-margin-small-bottom uk-margin-remove-top "> Beginner fresh</h6>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="profile.php" class="uk-link-reset">
                            <div class="uk-padding-remove uk-card-hover uk-card-body uk-width-large border-radius-6 uk-text-center scale-up-medium">
                                <img src="assets/images/avatures/avature-5.png" alt="Image" class="uk-width-1-2 uk-margin-top uk-margin-small-bottom uk-border-circle uk-align-center  uk-box-shadow-large">
                                <h4 class="uk-margin-remove-bottom uk-margin-remove-top "> Stocken smith</h4>
                                <h6 class="uk-margin-small-bottom uk-margin-remove-top "> Beginner fresh</h6>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="profile.php" class="uk-link-reset">
                            <div class="uk-padding-remove uk-card-hover uk-card-body uk-width-large border-radius-6 uk-text-center">
                                <img src="assets/images/avatures/avature-1.png" alt="Image" class="uk-width-1-2 uk-margin-top uk-margin-small-bottom uk-border-circle uk-align-center  uk-box-shadow-large">
                                <h4 class="uk-margin-remove-bottom uk-margin-remove-top ">katrina smith</h4>
                                <h6 class="uk-margin-small-bottom uk-margin-remove-top "> Beginner fresh</h6>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="profile.php" class="uk-link-reset">
                            <div class="uk-padding-remove uk-card-hover uk-card-body uk-width-large border-radius-6 uk-text-center">
                                <img src="assets/images/avatures/avature.jpg" alt="Image" class="uk-width-1-2 uk-margin-top uk-margin-small-bottom uk-border-circle uk-align-center  uk-box-shadow-large">
                                <h4 class="uk-margin-remove-bottom uk-margin-remove-top "> Stocken smith</h4>
                                <h6 class="uk-margin-small-bottom uk-margin-remove-top "> Beginner fresh</h6>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="profile.php" class="uk-link-reset">
                            <div class="uk-padding-remove uk-card-hover uk-card-body uk-width-large border-radius-6 uk-text-center scale-up-medium">
                                <img src="assets/images/avatures/avature-2.png" alt="Image" class="uk-width-1-2 uk-margin-top uk-margin-small-bottom uk-border-circle uk-align-center  uk-box-shadow-large">
                                <h4 class="uk-margin-remove-bottom uk-margin-remove-top "> Stocken smith</h4>
                                <h6 class="uk-margin-small-bottom uk-margin-remove-top "> Beginner fresh</h6>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="profile.php" class="uk-link-reset">
                            <div class="uk-padding-remove uk-card-hover uk-card-body uk-width-large border-radius-6 uk-text-center scale-up-medium">
                                <img src="assets/images/avatures/avature-3.png" alt="Image" class="uk-width-1-2 uk-margin-top uk-margin-small-bottom uk-border-circle uk-align-center  uk-box-shadow-large">
                                <h4 class="uk-margin-remove-bottom uk-margin-remove-top "> Stocken smith</h4>
                                <h6 class="uk-margin-small-bottom uk-margin-remove-top "> Beginner fresh</h6>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="profile.php" class="uk-link-reset">
                            <div class="uk-padding-remove uk-card-hover uk-card-body uk-width-large border-radius-6 uk-text-center">
                                <img src="assets/images/avatures/avature.jpg" alt="Image" class="uk-width-1-2 uk-margin-top uk-margin-small-bottom uk-border-circle uk-align-center  uk-box-shadow-large">
                                <h4 class="uk-margin-remove-bottom uk-margin-remove-top "> Stocken smith</h4>
                                <h6 class="uk-margin-small-bottom uk-margin-remove-top "> Beginner fresh</h6>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="profile.php" class="uk-link-reset">
                            <div class="uk-padding-remove uk-card-hover uk-card-body uk-width-large border-radius-6 uk-text-center">
                                <img src="assets/images/avatures/avature-4.png" alt="Image" class="uk-width-1-2 uk-margin-top uk-margin-small-bottom uk-border-circle uk-align-center  uk-box-shadow-large">
                                <h4 class="uk-margin-remove-bottom uk-margin-remove-top "> Stocken smith</h4>
                                <h6 class="uk-margin-small-bottom uk-margin-remove-top "> Beginner fresh</h6>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="profile.php" class="uk-link-reset">
                            <div class="uk-padding-remove uk-card-hover uk-card-body uk-width-large border-radius-6 uk-text-center">
                                <img src="assets/images/avatures/avature-5.png" alt="Image" class="uk-width-1-2 uk-margin-top uk-margin-small-bottom uk-border-circle uk-align-center  uk-box-shadow-large">
                                <h4 class="uk-margin-remove-bottom uk-margin-remove-top "> Stocken smith</h4>
                                <h6 class="uk-margin-small-bottom uk-margin-remove-top "> Beginner fresh</h6>
                            </div>
                        </a>
                    </div>
                </div>
            </li>
        </ul>

    </main>
    <!-- footer -->
    <div class="uk-section-small uk-margin-medium-top">
        <hr class="uk-margin-remove">
        <div class="uk-container uk-align-center uk-margin-remove-bottom uk-position-relative">
            <div uk-grid>
                <div class="uk-width-1-3@m uk-width-1-2@s uk-first-column">
                    <a href="pages-about.php" class="uk-link-heading uk-text-lead uk-text-bold"> <i class="fas fa-graduation-cap"></i> <?php echo $platform_name; ?></a>
                    <div class="uk-width-xlarge tm-footer-description">A unique and beautiful collection of UI elements
                        that are all flexible and modular. building the website of your dreams.</div>
                </div>
                <div class="uk-width-expand@m uk-width-1-2@s">
                    <ul class="uk-list  tm-footer-list">
                        <li>
                            <a href="#"> Browse Our Library </a>
                        </li>
                        <li>
                            <a href="#"> Tutorials/Articles </a>
                        </li>
                        <li>
                            <a href="#"> Scripts and codes</a>
                        </li>
                        <li>
                            <a href="#"> Ebooks</a>
                        </li>
                    </ul>
                </div>
                <div class="uk-width-expand@m uk-width-1-2@s">
                    <ul class="uk-list tm-footer-list">
                        <li>
                            <a href="#"> About us </a>
                        </li>
                        <li>
                            <a href="#"> Contact Us </a>
                        </li>
                        <li>
                            <a href="#"> Privacy </a>
                        </li>
                        <li>
                            <a href="#"> Policy </a>
                        </li>
                    </ul>
                </div>
                <div class="uk-width-expand@m uk-width-1-2@s">
                    <ul class="uk-list  tm-footer-list">
                        <li>
                            <a href="#">Web Design </a>
                        </li>
                        <li>
                            <a href="#">Web Development </a>
                        </li>
                        <li>
                            <a href="#"> iOS Development </a>
                        </li>
                        <li>
                            <a href="#"> PHP Development </a>
                        </li>
                    </ul>
                </div>
            </div>
            <hr>
            <p class="uk-postion-absoult uk-margin-remove uk-position-bottom-right" style="bottom: 8px;right: 60px;" uk-tooltip="title: Visit Our Site; pos: top-center"> Powered By <a href="#" target="_blank" class="uk-text-bold uk-link-reset"> <?php echo $platform_name; ?></a></p>
            <div class="uk-margin-small" uk-grid>
                <div class="uk-width-1-2@m uk-width-1-2@s uk-first-column">
                    <p class="uk-text-small"><i class="fas fa-copyright"></i> 2019 <span class="uk-text-bold"><?php echo $platform_name; ?></span> . All rights reserved.</p>
                </div>
                <div class="uk-width-1-3@m uk-width-1-2@s">
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Youtube Chanal; pos: top-center"><i class="fab fa-youtube" style=" color: #fb7575  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Facebook; pos: top-center"><i class="fab fa-Facebook" style=" color: #9160ec  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Instagram; pos: top-center"><i class="fab fa-Instagram" style=" color: #dc2d2d  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our linkedin; pos: top-center"><i class="fab fa-linkedin " style=" color: #6949a5  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our google-plus; pos: top-center"><i class="fab fa-google-plus" style=" color: #f77070 !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Twitter; pos: top-center"><i class="fab fa-twitter" style=" color: #6f23ff !important;"></i></a>
                </div>
            </div>
        </div>
    </div>
    <!-- footer  end -->
    <!-- app end -->
    <!-- button scrollTop -->
    <button id="scrollTop" class="uk-animation-slide-bottom-medium">
        <a href="#" class="uk-text-white" uk-totop uk-scroll></a>
    </button>
    <!--  Night mood -->
    <script>
        (function(window, document, undefined) {
            'use strict';
            if (!('localStorage' in window)) return;
            var nightMode = localStorage.getItem('gmtNightMode');
            if (nightMode) {
                document.documentElement.className += ' night-mode';
            }
        })(window, document);


        (function(window, document, undefined) {

            'use strict';

            // Feature test
            if (!('localStorage' in window)) return;

            // Get our newly insert toggle
            var nightMode = document.querySelector('#night-mode');
            if (!nightMode) return;

            // When clicked, toggle night mode on or off
            nightMode.addEventListener('click', function(event) {
                event.preventDefault();
                document.documentElement.classList.toggle('night-mode');
                if (document.documentElement.classList.contains('night-mode')) {
                    localStorage.setItem('gmtNightMode', true);
                    return;
                }
                localStorage.removeItem('gmtNightMode');
            }, false);

        })(window, document);

        // Preloader
        var spinneroverlay = document.getElementById("spinneroverlay");
        window.addEventListener('load', function() {
            spinneroverlay.style.display = 'none';
        });

        //scrollTop
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("scrollTop").style.display = "block";
            } else {
                document.getElementById("scrollTop").style.display = "none";
            }
        }
        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
</body>

</html>