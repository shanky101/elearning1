<?php
// Initialize the session
session_start();

// Check if the user is already logged in, if yes then redirect him to welcome page
if (!isset($_SESSION["loggedin"])) {
    header("location: authentication.php");
    exit;
}

// Processing form data when form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST['quiz_submit']) && $_POST['quiz_submit']) {

        $section_names = array("Gain", "Absolute", "2D LUT", "Bus creator", "Bus selector", "Data Dictionary", "Multiport Switch", "Manual Switch", "Bit Clear", "Bitwise Operator", "Ramp", "Scope", "Intro Simulink", "MBD Intro");
        $focus_areas = array();


        if (isset($_POST['question-1-answers']) && $_POST['question-1-answers']) {
            $answer1 = $_POST['question-1-answers'];
        } else {
            $answer1 = "";
        }
        if (isset($_POST['question-2-answers']) && $_POST['question-2-answers']) {
            $answer2 = $_POST['question-2-answers'];
        } else {
            $answer2 = "";
        }
        if (isset($_POST['question-3-answers']) && $_POST['question-3-answers']) {
            $answer3 = $_POST['question-3-answers'];
        } else {
            $answer3 = "";
        }
        if (isset($_POST['question-4-answers']) && $_POST['question-4-answers']) {
            $answer4 = $_POST['question-4-answers'];
        } else {
            $answer4 = "";
        }
        if (isset($_POST['question-5-answers']) && $_POST['question-5-answers']) {
            $answer5 = $_POST['question-5-answers'];
        } else {
            $answer5 = "";
        }
        if (isset($_POST['question-6-answers']) && $_POST['question-6-answers']) {
            $answer6 = $_POST['question-6-answers'];
        } else {
            $answer6 = "";
        }
        if (isset($_POST['question-7-answers']) && $_POST['question-7-answers']) {
            $answer7 = $_POST['question-7-answers'];
        } else {
            $answer7 = "";
        }
        if (isset($_POST['question-8-answers']) && $_POST['question-8-answers']) {
            $answer8 = $_POST['question-8-answers'];
        } else {
            $answer8 = "";
        }
        if (isset($_POST['question-9-answers']) && $_POST['question-9-answers']) {
            $answer9 = $_POST['question-9-answers'];
        } else {
            $answer9 = "";
        }
        if (isset($_POST['question-10-answers']) && $_POST['question-10-answers']) {
            $answer10 = $_POST['question-10-answers'];
        } else {
            $answer10 = "";
        }
        if (isset($_POST['question-11-answers']) && $_POST['question-11-answers']) {
            $answer11 = $_POST['question-11-answers'];
        } else {
            $answer11 = "";
        }
        if (isset($_POST['question-12-answers']) && $_POST['question-12-answers']) {
            $answer12 = $_POST['question-12-answers'];
        } else {
            $answer12 = "";
        }
        if (isset($_POST['question-13-answers']) && $_POST['question-13-answers']) {
            $answer13 = $_POST['question-13-answers'];
        } else {
            $answer13 = "";
        }
        if (isset($_POST['question-14-answers']) && $_POST['question-14-answers']) {
            $answer14 = $_POST['question-14-answers'];
        } else {
            $answer14 = "";
        }
        if (isset($_POST['question-15-answers']) && $_POST['question-15-answers']) {
            $answer15 = $_POST['question-15-answers'];
        } else {
            $answer15 = "";
        }
        if (isset($_POST['question-16-answers']) && $_POST['question-16-answers']) {
            $answer16 = $_POST['question-16-answers'];
        } else {
            $answer16 = "";
        }
        if (isset($_POST['question-17-answers']) && $_POST['question-17-answers']) {
            $answer17 = $_POST['question-17-answers'];
        } else {
            $answer17 = "";
        }
        if (isset($_POST['question-18-answers']) && $_POST['question-18-answers']) {
            $answer18 = $_POST['question-18-answers'];
        } else {
            $answer18 = "";
        }
        if (isset($_POST['question-19-answers']) && $_POST['question-19-answers']) {
            $answer19 = $_POST['question-19-answers'];
        } else {
            $answer19 = "";
        }
        if (isset($_POST['question-20-answers']) && $_POST['question-20-answers']) {
            $answer20 = $_POST['question-20-answers'];
        } else {
            $answer20 = "";
        }
        if (isset($_POST['question-21-answers']) && $_POST['question-21-answers']) {
            $answer21 = $_POST['question-21-answers'];
        } else {
            $answer21 = "";
        }
        if (isset($_POST['question-22-answers']) && $_POST['question-22-answers']) {
            $answer22 = $_POST['question-22-answers'];
        } else {
            $answer22 = "";
        }
        if (isset($_POST['question-23-answers']) && $_POST['question-23-answers']) {
            $answer23 = $_POST['question-23-answers'];
        } else {
            $answer23 = "";
        }
        if (isset($_POST['question-24-answers']) && $_POST['question-24-answers']) {
            $answer24 = $_POST['question-24-answers'];
        } else {
            $answer24 = "";
        }
        if (isset($_POST['question-25-answers']) && $_POST['question-25-answers']) {
            $answer25 = $_POST['question-25-answers'];
        } else {
            $answer25 = "";
        }
        if (isset($_POST['question-26-answers']) && $_POST['question-26-answers']) {
            $answer26 = $_POST['question-26-answers'];
        } else {
            $answer26 = "";
        }
        if (isset($_POST['question-27-answers']) && $_POST['question-27-answers']) {
            $answer27 = $_POST['question-27-answers'];
        } else {
            $answer27 = "";
        }
        if (isset($_POST['question-28-answers']) && $_POST['question-28-answers']) {
            $answer28 = $_POST['question-28-answers'];
        } else {
            $answer28 = "";
        }
        if (isset($_POST['question-29-answers']) && $_POST['question-29-answers']) {
            $answer29 = $_POST['question-29-answers'];
        } else {
            $answer29 = "";
        }
        if (isset($_POST['question-30-answers']) && $_POST['question-30-answers']) {
            $answer30 = $_POST['question-30-answers'];
        } else {
            $answer30 = "";
        }

        $totalCorrect = 0;

        if ($answer1 == "A") {
            $totalCorrect++;
        }
        if ($answer2 == "C") {
            $totalCorrect++;
        }

        if ($answer1 != "A" || $answer2 == "C") {
            array_push($focus_areas, $section_names[0]);
        }

        if ($answer3 == "B") {
            $totalCorrect++;
        }
        if ($answer4 == "D") {
            $totalCorrect++;
        }

        if ($answer3 != "B" || $answer4 != "D") {
            array_push($focus_areas, $section_names[1]);
        }

        if ($answer5 == "D") {
            $totalCorrect++;
        }
        if ($answer6 == "C") {
            $totalCorrect++;
        }

        if ($answer5 != "D" || $answer6 != "C") {
            array_push($focus_areas, $section_names[2]);
        }

        if ($answer7 == "D") {
            $totalCorrect++;
        }
        if ($answer8 == "C") {
            $totalCorrect++;
        }
        if ($answer9 == "A") {
            $totalCorrect++;
        }

        if ($answer7 != "D" || $answer8 != "C" || $answer9 != "A") {
            array_push($focus_areas, $section_names[3]);
        }

        if ($answer10 == "D") {
            $totalCorrect++;
        }
        if ($answer11 == "C") {
            $totalCorrect++;
        }
        if ($answer12 == "A") {
            $totalCorrect++;
        }

        if ($answer10 != "D" || $answer11 != "C" || $answer12 != "A") {
            array_push($focus_areas, $section_names[4]);
        }

        if ($answer13 == "B") {
            $totalCorrect++;
        }
        if ($answer14 == "D") {
            $totalCorrect++;
        }

        if ($answer13 != "B" || $answer14 != "D") {
            array_push($focus_areas, $section_names[5]);
        }

        if ($answer15 == "B") {
            $totalCorrect++;
        }
        if ($answer16 == "A") {
            $totalCorrect++;
        }

        if ($answer15 != "B" || $answer16 != "A") {
            array_push($focus_areas, $section_names[6]);
        }

        if ($answer17 == "B") {
            $totalCorrect++;
        }
        if ($answer18 == "C") {
            $totalCorrect++;
        }

        if ($answer17 != "B" || $answer18 != "C") {
            array_push($focus_areas, $section_names[7]);
        }

        if ($answer19 == "C") {
            $totalCorrect++;
        }
        if ($answer20 == "A") {
            $totalCorrect++;
        }

        if ($answer19 != "C" || $answer20 != "A") {
            array_push($focus_areas, $section_names[8]);
        }

        if ($answer21 == "C") {
            $totalCorrect++;
        } else {
            array_push($focus_areas, $section_names[9]);
        }

        if ($answer22 == "C") {
            $totalCorrect++;
        }
        if ($answer23 == "C") {
            $totalCorrect++;
        }

        if ($answer22 != "C" || $answer23 != "C") {
            array_push($focus_areas, $section_names[10]);
        }

        if ($answer24 == "B") {
            $totalCorrect++;
        }
        if ($answer25 == "A") {
            $totalCorrect++;
        }

        if ($answer24 != "B" || $answer25 != "A") {
            array_push($focus_areas, $section_names[11]);
        }

        if ($answer26 == "A") {
            $totalCorrect++;
        }
        if ($answer27 == "A") {
            $totalCorrect++;
        }

        if ($answer26 != "A" || $answer27 != "A") {
            array_push($focus_areas, $section_names[12]);
        }

        if ($answer28 == "B") {
            $totalCorrect++;
        }
        if ($answer29 == "D") {
            $totalCorrect++;
        }
        if ($answer30 == "A") {
            $totalCorrect++;
        }

        if ($answer28 != "B" || $answer29 != "D" || $answer30 != "A") {
            array_push($focus_areas, $section_names[13]);
        }

        // Store data in session variables
        $_SESSION["total_correct"] = $totalCorrect;
        $_SESSION["total_questions"] = "30";
        $_SESSION["focus_areas"] = $focus_areas;

        // Redirect user to welcome page
        header("location: course-result jp.php");
    }
}
?>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="assets/images/favicon.png">
    <meta name="description" content="">
    <title> Keep Learning </title>
    <!-- Favicon -->
    <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Your stylesheet-->
    <link rel="stylesheet" href="assets/css/uikit.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/update.css">
    <!-- font awesome -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <!--  javascript -->
    <script src="assets/js/simplebar.js"></script>
    <script src="assets/js/uikit.js"></script>
</head>

<body>
    <!-- Flash Message -->
    <div id="note">
        おめでとう , このコースは正常に登録されました。楽しんでください.
        <a class="uk-margin-medium-right uk-margin-remove-bottom uk-position-relative uk-icon-button uk-align-right  uk-margin-small-top" uk-toggle="target: #note; cls:uk-hidden"> <i class="fas fa-times  uk-position-center"></i> </a>
    </div>
    <div class="tm-course-lesson">
        <!-- mobile-sidebar  -->
        <i class="fas fa-video icon-large tm-side-right-mobile-icon uk-hidden@m" uk-toggle="target: #filters"></i>
        <div class="uk-grid-collapse" id="course-fliud" uk-grid>
            <div class="uk-width-3-4@m uk-margin-auto">
                <header class="tm-course-content-header uk-background-grey">
                    <a href="course-view.php" class="back-to-dhashboard uk-margin-small-left" uk-tooltip="title: Back to Course Dashboard  ; delay: 200 ; pos: bottom-left ;animation:uk-animation-scale-up ; offset:20">
                        コースダッシュボード</a>
                </header>
                <!--Course-side icon make Hidden sidebar -->
                <i class="fas fa-angle-right icon-large uk-float-right tm-side-course-icon  uk-visible@m" uk-toggle="target: #course-fliud; cls: tm-course-fliud" uk-tooltip="title: Hide sidebar  ; delay: 200 ; pos: bottom-right ;animation:uk-animation-scale-up ; offset:20"></i>
                <!--Course-side active icon -->
                <i class="fas fa-angle-right icon-large uk-float-right tm-side-course-active-icon uk-visible@m" uk-toggle="target: #course-fliud; cls: tm-course-fliud" uk-tooltip="title: Open sidebar  ; delay: 200 ; pos: bottom-right ;animation:uk-animation-scale-up ; offset:20"></i>
                <!-- PreLoader -->
                <div id="spinneroverlay">
                    <div class="spinner"></div>
                </div>
                <ul id="component-nav" class="uk-switcher uk-position-relative uk-padding">
                    <!--  Lesson one -->
                    <li>
                        <div class="navigation-controls">
                            <a href="#" class="previous uk-animation-fade" uk-switcher-item="previous" uk-tooltip="title: Previous Video  ; pos: right ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-left"></i> </a>
                            <a href="#" class="next uk-animation-fade" uk-switcher-item="next" uk-tooltip="title: Next Video  ; pos: left ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-right"></i> </a>
                        </div>
                        <div class="video-responsive">
                            <iframe id="myvid1" src="https://player.vimeo.com/video/385658124" frameborder="0" uk-video="automute: true" allowfullscreen uk-responsive></iframe>
                        </div>
                        <br>
                    </li>
                    <!--  Lesson two  -->
                    <li>
                        <div class="navigation-controls">
                            <a href="#" class="previous uk-animation-fade" uk-switcher-item="previous" uk-tooltip="title: Previous Video  ; pos: right ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-left"></i> </a>
                            <a href="#" class="next uk-animation-fade" uk-switcher-item="next" uk-tooltip="title: Next Video  ; pos: left ;animation:uk-animation-scale-up ; offset:20"><i class="fas fa-angle-right"></i> </a>
                        </div>
                        <div class="video-responsive">
                            <iframe id="myvid2" src="https://player.vimeo.com/video/385657941" frameborder="0" allowfullscreen uk-responsive></iframe>
                        </div>
                        <br>
                    </li>



                    <!--  Quiz -->
                    <li>
                        <form id="loginForm" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                            <div class="uk-scrollable-content uk-background-white">
                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q1. ゲイン値は、正、負、浮動のいずれでもかまいませんが、ゼロになることはありません。</p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-1-answers" value="A"> 真</label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-1-answers" value="B"> 偽</label>
                                    <br><br>
                                </div>
                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q2. ゲインパラメータは5で、入力値は10です。出力値は </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-2-answers" value="A"> 05</label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-2-answers" value="B"> 10</label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-2-answers" value="C"> 50</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-2-answers" value="D"> 25</label>
                                </div>
                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q3. アブソルートブロック出力は常に………です。 </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-3-answers" value="A"> ネガティブ</label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-3-answers" value="B"> ポジティブ</label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-3-answers" value="C"> 両方</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-3-answers" value="D"> どちらでもない</label>
                                </div>
                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q4. アブソルートブロックは、ライブラリブラウザの……………..セクションにあります</p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-4-answers" value="A"> シンク</label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-4-answers" value="B"> ソース</label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-4-answers" value="C"> 論理演算とビット演算</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-4-answers" value="D"> 演算演算</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q5. 1D LUTの入力から出力への対応は</p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-5-answers" value="A">1:2 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-5-answers" value="B">1:1 </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-5-answers" value="C"> 1:3</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-5-answers" value="D"> 2:1</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q6. LUTの入力の診断方法は </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-6-answers" value="A">エラー </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-6-answers" value="B">警告 </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-6-answers" value="C"> すべて </label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-6-answers" value="D"> なし</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q7. バスクリエーターへの入力は </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-7-answers" value="A">同じタイプ </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-7-answers" value="B">異なるタイプ </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-7-answers" value="C"> 定数</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-7-answers" value="D"> すべて</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q8. バスの作成者には、…………..入力と出力の関係があります </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-8-answers" value="A">1対多 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-8-answers" value="B">1対1 </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-8-answers" value="C"> 多対1</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-8-answers" value="D"> なし</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q9. バスクリエーターの出力で、 </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-9-answers" value="A">構造 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-9-answers" value="B">配列 </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-9-answers" value="C"> 列挙型</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-9-answers" value="D"> 結合</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q10. バスクリエーターの出力は </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-10-answers" value="A">同じタイプ </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-10-answers" value="B">異なるタイプ </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-10-answers" value="C"> 定数</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-10-answers" value="D"> すべて</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q11. バスセレクターへの入力を再配置できます。 </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-11-answers" value="A">上 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-11-answers" value="B">下 </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-11-answers" value="C"> どちら</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-11-answers" value="D"> なし</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q12. バスセレクターには、…………..入力と出力の関係があります。 </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-12-answers" value="A">1対多 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-12-answers" value="B">1対1 </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-12-answers" value="C"> 多対1</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-12-answers" value="D"> なし</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q13. データディクショナリは、変更の追跡と管理の追加オプションを提供します </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-13-answers" value="A">偽 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-13-answers" value="B">真 </label>
                                    <br><br>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q14. データディクショナリストア </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-14-answers" value="A">パラメータ </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-14-answers" value="B">入力 </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-14-answers" value="C"> 出力</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-14-answers" value="D"> すべて</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q15. 最初の入力は……です。そして最後の入力は…………です。 </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-15-answers" value="A">デフォルト、 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-15-answers" value="B">選択、デフォルト </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-15-answers" value="C"> In1、最後に</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-15-answers" value="D"> In1、In3</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q16. マルチポートスイッチは次のように動作します </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-16-answers" value="A">Mux </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-16-answers" value="B">Demux </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-16-answers" value="C"> バスセレクター</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-16-answers" value="D"> 上記のすべて</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q17. .手動スイッチは、単極双投スイッチと同じようには動作しません </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-17-answers" value="A">偽 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-17-answers" value="B">真 </label>
                                    <br><br>

                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q18. 入力にグランドが接続されている場合、次のように見なされます。s </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-18-answers" value="A">One </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-18-answers" value="B">Vcc</label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-18-answers" value="C"> 0</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-18-answers" value="D">不明</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q19. Bit Clearブロックはサポートしていません </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-19-answers" value="A">整数</label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-19-answers" value="B">固定小数点 </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-19-answers" value="C"> 列挙</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-19-answers" value="D">ブーレン</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q20. ビット1のビットクリアブロックがオンになっている場合、ビットは </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-20-answers" value="A">0 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-20-answers" value="B">1 </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-20-answers" value="C"> 気にしない</label>
                                    <br><br>

                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q21. Bitwise Operatorブロックはシフト操作をサポートしていません。 </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-21-answers" value="A">真 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-21-answers" value="B">偽 </label>
                                    <br><br>

                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q22. Rampブロックの[Which]パラメーターは、出力信号の特性を決定します。 </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-22-answers" value="A">勾配 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-22-answers" value="B"> 開始時間 </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-22-answers" value="C"> 初期出力</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-22-answers" value="D"> すべて</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q23. ランプブロックのデータタイプは. </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-23-answers" value="A">整数 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-23-answers" value="B">固定小数点 </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-23-answers" value="C"> 列挙</label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-23-answers" value="D"> Double</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q24. Scopeブロックは </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-24-answers" value="A">入力信号を生成する </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-24-answers" value="B">出力を確認する </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-24-answers" value="C">すべて</label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q25. スコープブロック 複数の入力ポートを使用して、同じy軸（ディスプレイ）に複数の信号をプロットします。</p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-25-answers" value="A">真 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-25-answers" value="B">偽 </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-25-answers" value="C">なし </label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q26. ワークスペースには、データファイルまたは他のプログラムから作成またはMATLABにインポートする変数が含まれています。 </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-26-answers" value="A">真 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-26-answers" value="B">偽 </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-26-answers" value="C">なし </label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q27. コマンドウィンドウを使用すると、コマンドラインで個々のステートメントを入力し、生成された結果を表示できます。 </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-27-answers" value="A">真 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-27-answers" value="B">偽 </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-27-answers" value="C">なし </label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q28. MBDはGUIを使用して、モデリングをより簡単にし、コード生成をより便利にします </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-28-answers" value="A">偽 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-28-answers" value="B">真 </label>

                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q29. MBDは従来の方法よりも好ましい </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-29-answers" value="A">費用対効果 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-29-answers" value="B">品質の向上 </label>
                                    <br><br>
                                    <label>C) <input class="uk-radio" type="radio" name="question-29-answers" value="C">高速化 </label>
                                    <br><br>
                                    <label>D) <input class="uk-radio" type="radio" name="question-29-answers" value="C">すべて </label>
                                </div>

                                <p class="uk-margin-medium-top uk-text-medium uk-visible@s uk-margin-medium-left"> Q30. Matlabはモデル開発にのみ使用され、検証には使用されません </p>
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-margin-medium-left">
                                    <label>A) <input class="uk-radio" type="radio" name="question-30-answers" value="A">偽 </label>
                                    <br><br>
                                    <label>B) <input class="uk-radio" type="radio" name="question-30-answers" value="B">真 </label>

                                </div>

                                <div class="uk-clearfix uk-margin uk-margin-medium-right uk-animation-slide-bottom">
                                    <div class="uk-float-right">
                                        <input class="uk-button uk-button-success" name="quiz_submit" type="submit" class="button" value="Submit">
                                    </div>
                                </div>

                            </div>
                        </form>
                    </li>
                </ul>
            </div>
            <div class="uk-width-1-4@m uk-offcanvas tm-filters uk-background-default tm-side-course uk-animation-slide-right-medium uk-overflow-hidden" id="filters" uk-offcanvas="overlay: true; container: false; flip: true">
                <div class="uk-offcanvas-bar uk-padding-remove uk-preserve-color">
                    <!-- Sidebar menu-->
                    <ul class="uk-child-width-expand uk-tab tm-side-course-nav uk-margin-remove uk-position-z-index" uk-switcher="animation: uk-animation-slide-right-medium, uk-animation-slide-left-small" style="box-shadow: 0px 0px 7px 1px gainsboro;" uk-switcher>
                        <li class="uk-active">
                            <a href="#" uk-tooltip="title: Course Videos ; delay: 200 ; pos: bottom-left ;animation:uk-animation-scale-up">
                                <i class="fas fa-video icon-medium"></i> Videos </a>
                        </li>
                        <li>
                            <a href="#" uk-tooltip="title: Course Files ; delay: 200 ; pos: bottom-center ;animation:uk-animation-scale-up">
                                <i class="fas fa-archive icon-medium"></i> Contents </a>
                        </li>
                    </ul>
                    <!-- Sidebar contents -->
                    <ul class="uk-switcher">
                        <!-- Course Video tab  -->
                        <li>
                            <div class="demo1" data-simplebar>
                                <ul class="course-list video-list" uk-switcher="connect: #component-nav; animation: uk-animation-slide-right-small, uk-animation-slide-left-medium">
                                    <!--  Lesson one -->
                                    <li>
                                        <a href="#">
                                            <img src="assets/images/courses/course-lesson-video-list.jpg" alt="">
                                            <i class="fas fa-play-circle play-icon"></i>
                                            <span class="now-playing">Now Playing</span>
                                            <span class="uk-text-truncate">MBD Intro</span>
                                            <time>3 min. 40 sec.</time>
                                        </a>
                                    </li>
                                    <!--  Lesson two  -->
                                    <li>
                                        <a href="#" onclick="stop(document.getElementById('myvid2'));">
                                            <img src="assets/images/courses/course-lesson-video-list.jpg" alt="">
                                            <i class="fas fa-play-circle play-icon"></i>
                                            <span class="now-playing">Now Playing</span>
                                            <span class="uk-text-truncate"> Bitwise operator </span>
                                            <time>5 min. 0 sec.</time>
                                        </a>
                                    </li>



                                    <!--  Quiz  -->
                                    <li>
                                        <a href="#" onclick="stop(document.getElementById('myvid1'));">
                                            <img src="assets/images/courses/course-lesson-quiz.png" alt="">
                                            <i class="fas fa-question-circle  play-icon"></i>
                                            <span class="uk-text-truncate"> Quiz </span>
                                            <time>30 Questions</time>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!--  Course resource  tab  -->
                        <li>
                            <div class="demo1 uk-background-default" data-simplebar>
                                <img src="assets/images/courses/course-lesson-icon.jpg" width="200" class="uk-margin-remove-bottom uk-align-center uk-width-2-3" alt="">
                                <p class="uk-padding-small uk-margin-remove uk-text-center uk-padding-remove-top">Lorem
                                    ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod dolore .</p>
                                <h4 class="uk-heading-line uk-text-center uk-margin-top"><span>Requirements </span></h4>
                                <p class="uk-padding-small uk-margin-remove  uk-padding-remove-top uk-text-center">Etiam
                                    sit amet augue non velit aliquet consectetur molestie eros mauris .</p>
                                <h5 class="uk-padding-small uk-margin-remove uk-background-muted uk-text-black"> Web
                                    browser</h5>
                                <div class="uk-grid-small" uk-grid>
                                    <div class="uk-width-1-4  uk-margin-small-top">
                                        <img src="assets/images/icons/software-icon.jpg" alt="Image" class="uk-align-center img-small">
                                    </div>
                                    <div class="uk-width-3-4 uk-padding-remove-left">
                                        <p class="uk-margin-remove-bottom uk-text-bold  uk-text-small uk-margin-small-top">
                                            Firefox </p>
                                        <p class="uk-margin-remove-top uk-margin-small-bottom">Lorem ipsum dolor
                                            adipiscing elit, sed do eiusmod tempor ...</p>
                                        <a class="Course-tags uk-margin-small-right border-radius-6" href="#"> Official
                                            site </a>
                                        <a class="Course-tags uk-margin-small-right  border-radius-6 tags-active" href="#"> Download </a>
                                    </div>
                                </div>
                                <hr class="uk-margin-remove-bottom">
                                <div class="uk-grid-small uk-margin-small-top" uk-grid>
                                    <div class="uk-width-1-4  uk-margin-small-top">
                                        <img src="assets/images/icons/software-icon.jpg" alt="Image" class="uk-align-center img-small">
                                    </div>
                                    <div class="uk-width-3-4 uk-padding-remove-left">
                                        <p class="uk-margin-remove-bottom uk-text-bold  uk-text-small uk-margin-small-top">
                                            Google Chrome </p>
                                        <p class="uk-margin-remove-top uk-margin-small-bottom">Lorem ipsum dolor
                                            adipiscing elit, sed do eiusmod tempor ...</p>
                                        <a class="Course-tags uk-margin-small-right border-radius-6" href="#"> Official
                                            site </a>
                                        <a class="Course-tags uk-margin-small-right  border-radius-6 tags-active" href="#"> Download </a>
                                    </div>
                                </div>
                                <hr class="uk-margin-remove-bottom">
                                <div class="uk-grid-small uk-margin-small-top" uk-grid>
                                    <div class="uk-width-1-4  uk-margin-small-top">
                                        <img src="assets/images/icons/software-icon.jpg" alt="Image" class="uk-align-center img-small">
                                    </div>
                                    <div class="uk-width-3-4 uk-padding-remove-left">
                                        <p class="uk-margin-remove-bottom uk-text-bold  uk-text-small uk-margin-small-top">
                                            UCBrwoser </p>
                                        <p class="uk-margin-remove-top uk-margin-small-bottom">Lorem ipsum dolor
                                            adipiscing elit, sed do eiusmod tempor ...</p>
                                        <a class="Course-tags uk-margin-small-right border-radius-6" href="#"> Official
                                            site </a>
                                        <a class="Course-tags uk-margin-small-right  border-radius-6 tags-active" href="#"> Download </a>
                                    </div>
                                </div>
                                <h5 class="uk-padding-small uk-margin-remove-bottom uk-background-muted uk-text-black">
                                    Text Editor </h5>
                                <div class="uk-grid-small" uk-grid>
                                    <div class="uk-width-1-4  uk-margin-small-top">
                                        <img src="assets/images/icons/software-icon.jpg" alt="Image" class="uk-align-center img-small">
                                    </div>
                                    <div class="uk-width-3-4 uk-padding-remove-left">
                                        <p class="uk-margin-remove-bottom uk-text-bold  uk-text-small uk-margin-small-top">
                                            Atom </p>
                                        <p class="uk-margin-remove-top uk-margin-small-bottom">Lorem ipsum dolor
                                            adipiscing elit, sed do eiusmod tempor ...</p>
                                        <a class="Course-tags uk-margin-small-right border-radius-6" href="#"> Official
                                            site </a>
                                        <a class="Course-tags uk-margin-small-right  border-radius-6 tags-active" href="#"> Download </a>
                                    </div>
                                </div>
                                <hr class="uk-margin-remove-bottom">
                                <div class="uk-grid-small uk-margin-small-top" uk-grid>
                                    <div class="uk-width-1-4  uk-margin-small-top">
                                        <img src="assets/images/icons/software-icon.jpg" alt="Image" class="uk-align-center img-small">
                                    </div>
                                    <div class="uk-width-3-4 uk-padding-remove-left">
                                        <p class="uk-margin-remove-bottom uk-text-bold  uk-text-small uk-margin-small-top">
                                            Dreamweaver </p>
                                        <p class="uk-margin-remove-top uk-margin-small-bottom">Lorem ipsum dolor
                                            adipiscing elit, sed do eiusmod tempor ...</p>
                                        <a class="Course-tags uk-margin-small-right border-radius-6" href="#"> Official
                                            site </a>
                                        <a class="Course-tags uk-margin-small-right  border-radius-6 tags-active" href="#"> Download </a>
                                    </div>
                                </div>
                                <hr class="uk-margin-remove-bottom">
                                <div class="uk-grid-small uk-margin-small-top" uk-grid>
                                    <div class="uk-width-1-4  uk-margin-small-top">
                                        <img src="assets/images/icons/software-icon.jpg" alt="Image" class="uk-align-center img-small">
                                    </div>
                                    <div class="uk-width-3-4 uk-padding-remove-left">
                                        <p class="uk-margin-remove-bottom uk-text-bold  uk-text-small uk-margin-small-top">
                                            Sublime </p>
                                        <p class="uk-margin-remove-top uk-margin-small-bottom">Lorem ipsum dolor
                                            adipiscing elit, sed do eiusmod tempor ...</p>
                                        <a class="Course-tags uk-margin-small-right border-radius-6" href="#"> Official
                                            site </a>
                                        <a class="Course-tags uk-margin-small-right  border-radius-6 tags-active" href="#"> Download </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <h1 class="mt-4"> MBD の概要とその利点</h1>
    <h2 class="mt-4"> 内容</h2>



    <ul class="uk-list uk-list-bullet">
        <li>MBDとは?</li>
        <li>何故 MBD?</li>
        <li>MBDの利点</li>
        <li>MBD と以前の開発方法の対比</li>
    </ul>
    <h2 class="mt-4">MBD(Model based development)とは?</h2>
    <ul class="uk-list uk-list-bullet">
        <li>MBDは、モデルを作るために設計された視覚的なプログラミングインターフェイスです</li>
        <li>それはブロック表現と階層を使用しています。</li>
        <li>コードは、モデルから自動生成されるものとします。</li>
        <li>コードのピクトリアル表現。</li>
    </ul>
    <h2 class="mt-4">MBDの例</h2>
    <img src="assets/images/backgrounds/mbd1.jpg" style="width:800px;height:500px;">
    <h2 class="mt-4">何故MBD?</h2>
    <ul class="uk-list uk-list-bullet">
        <li>システムの複雑さの軽減 </li>
        <li>テスト スイートの再利用性 </li>
        <li>複数のハードウェア ターゲット展開に対する設計の再利用可能性</li>
        <li>自動コード生成 継続的なエラーの識別と修正 により、最大 50% の市場投入時間と全体的なコストの削減開発</li>
    </ul>
    <h2 class="mt-4">MBD利点</h2>
    <ul class="uk-list uk-list-bullet">
        <li>制御システムを開発するための効率的で費用対効果の高い方法 </li>
        <li>シミュレーションを使用して信頼性の高い検証を実装しやすい </li>
        <li> 開発時間を短縮し、製品の品質を向上</li>

    </ul>
    <h2 class="mt-4">MBDと以前の開発方法の対比</h2>
    <ul class="uk-list uk-list-bullet">
        <li>開発はより柔軟で信頼性が高い </li>
        <li>バグの特定が初期段階で行われるように、テストと開発が並行して実行される </li>
        <li> 変更の実装がより簡単でコスト効率が高い</li>
        <li> エラーが少ない</li>
        <li>視覚化とGUIにより、プロセスが容易になり、speedy</li>

    </ul>



    <!-- app end -->
    <script>
        // Preloader
        var spinneroverlay = document.getElementById("spinneroverlay");
        window.addEventListener('load', function() {
            spinneroverlay.style.display = 'none';
        });

        var currentFrame = document.getElementById('myvid1');

        function stop(targetFrame) {
            // Open a new tab
            if (targetFrame.id == "myvid2") {
                var win1 = window.open('course-lesson%20jp1.php', '_self');
                win1.focus();
            }else if(targetFrame.id == "myvid1") {
                var qz = window.open('course-quiz.php', '_self');
                qz.focus();
            }

            // Stop the video playback
            if (currentFrame != targetFrame) {
                currentFrame.src = currentFrame.src;
                currentFrame = targetFrame;
            }
        }
    </script>
</body>

</html>