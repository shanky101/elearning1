<?php
// Initialize the session
session_start();

// Check if the user is already logged in, if yes then redirect him to welcome page
if (!isset($_SESSION["loggedin"])) {
    header("location: authentication.php");
    exit;
}

// Include config file
require_once "config.php";

//setting the username to empty at start
$name = "";

// Check if the user is already logged in, if yes then redirect him to welcome page
if (isset($_SESSION["name"]) && !empty($_SESSION["name"])) {
    //updating the user's name from session data
    $name = trim($_SESSION["name"]);
}
?>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="assets/images/favicon.png">
    <meta name="description" content="">
    <title> Course Dashboard </title>
    <!-- Favicon -->
    <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Your stylesheet-->
    <link rel="stylesheet" href="assets/css/uikit.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <!-- font awesome -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <!--  javascript -->
    <script src="assets/js/simplebar.js"></script>
    <script src="assets/js/uikit.js"></script>
</head>

<body>
    <!-- PreLoader -->
    <div id="spinneroverlay">
        <div class="spinner"></div>
    </div>
    <!-- header  -->
    <header class="tm-header" uk-sticky>
        <div class=" uk-background-grey uk-navbar-container uk-navbar-transparent uk-padding-small uk-navbar-sticky">
            <div class="uk-position-relative">
                <nav class="uk-navbar-transparent tm-mobile-header uk-animation-slide-top uk-position-z-index" uk-navbar>
                    <!-- logo -->
                    <!-- mobile icon for side nav on nav-mobile-->
                    <span class="uk-hidden@m tm-mobile-menu-icon" uk-toggle="target: #mobile-sidebar"><i class="fas fa-bars icon-large"></i></span>
                    <!-- mobile icon for user icon on nav-mobile -->
                    <span class="uk-hidden@m tm-mobile-user-icon uk-align-right" uk-toggle="target: #tm-show-on-mobile; cls: tm-show-on-mobile-active"><i class="fas fa-user icon-large"></i></span>
                    <!-- mobile logo -->
                    <a class="uk-hidden@m uk-logo" href="course-dashboard.php"> <?php echo $platform_name; ?></a>
                    <div class="uk-navbar-left uk-visible@m">
                        <a href="course-dashboard.php" class="uk-logo uk-margin-left"> <i class="fas fa-graduation-cap"> </i>
                            <?php echo $platform_name; ?></a>
                    </div>
                    <div class="uk-navbar-right tm-show-on-mobile uk-flex-right" id="tm-show-on-mobile">
                        <!-- this will clouse after display user icon -->
                        <span class="uk-hidden@m tm-mobile-user-close-icon uk-align-right" uk-toggle="target: #tm-show-on-mobile; cls: tm-show-on-mobile-active"><i class="fas fa-times icon-large"></i></span>
                        <ul class="uk-navbar-nav uk-flex-middle">
                            <li>
                                <!-- your courses -->
                                <a href="#"> <i class="fas fa-play uk-hidden@m"></i> <span class="uk-visible@m"> Your
                                        Courses</span> </a>
                                <div uk-dropdown="pos: top-right ;mode : click; animation: uk-animation-slide-bottom-medium" class="uk-dropdown border-radius-6  uk-dropdown-top-right tm-dropdown-large uk-padding-remove">
                                    <div class="uk-clearfix">
                                        <div class="uk-float-left">
                                            <h5 class="uk-padding-small uk-margin-remove uk-text-bold  uk-text-left">
                                                Your Courses </h5>
                                        </div>
                                        <div class="uk-float-right">
                                            <i class="fas fa-check uk-align-right  uk-margin-remove uk-margin-remove-left  uk-padding-small uk-text-small">
                                                Completed 2 / 4 </i>
                                        </div>
                                    </div>
                                    <hr class=" uk-margin-remove">
                                    <div class="uk-padding-smaluk-text-left uk-height-medium">
                                        <div class="demo1" data-simplebar>
                                            <div class="uk-child-width-1-2@s  uk-grid-small uk-padding-small" uk-scrollspy="target: > div; cls:uk-animation-slide-bottom-small; delay: 100 ;repeat: true" uk-grid>
                                                <div>
                                                    <a href="course-view.php" class="uk-link-reset">
                                                        <div class="uk-padding-small uk-card-default">
                                                            <progress id="js-progressbar" class="uk-progress progress-green uk-margin-small-bottom" value="100" max="100" style="height: 7px;"></progress>
                                                            <img src="assets/images/courses/tags/css3.JPG" class="uk-align-left  uk-margin-small-right uk-margin-small-bottom  uk-width-1-3  uk-visible@s" alt="">
                                                            <p class="uk-text-bold uk-margin-remove">CSS3 Introduction
                                                            </p>
                                                            <p class="uk-text-small uk-margin-remove"> by : John Doe
                                                            </p>
                                                            <div class="uk-margin-small">
                                                                <a class="Course-tags uk-margin-small-right   border-radius-6" href="#"> <i class="fas fa-play"></i> Resume</a>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div>
                                                    <a href="course-view.php" class="uk-link-reset">
                                                        <div class="uk-padding-small uk-card-default">
                                                            <progress id="js-progressbar" class="uk-progress progress-coral  uk-margin-small-bottom" value="15" max="100" style="height: 7px !important;"></progress>
                                                            <img src="assets/images/courses/tags/html5.jpg" class="uk-align-left  uk-margin-small-right uk-margin-small-bottom  uk-width-1-3  uk-visible@s" alt="">
                                                            <p class="uk-text-bold uk-margin-remove">Introduction to Simulink
                                                            </p>
                                                            <p class="uk-text-small uk-margin-remove"> by : John Doe
                                                            </p>
                                                            <div class="uk-margin-small">
                                                                <a class="Course-tags uk-margin-small-right   border-radius-6" href="course-lesson.php"> <i class="fas fa-play"></i> Resume</a>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div>
                                                    <a href="course-view.php" class="uk-link-reset">
                                                        <div class="uk-padding-small uk-card-default">
                                                            <progress id="js-progressbar" class="uk-progress progress-coral uk-margin-small-bottom" value="50" max="100" style="height: 7px;"></progress>
                                                            <img src="assets/images/courses/tags/css3.JPG" class="uk-align-left  uk-margin-small-right uk-margin-small-bottom  uk-width-1-3  uk-visible@s" alt="">
                                                            <p class="uk-text-bold uk-margin-remove">PHP Introduction
                                                            </p>
                                                            <p class="uk-text-small uk-margin-remove"> by : John Doe
                                                            </p>
                                                            <div class="uk-margin-small">
                                                                <a class="Course-tags uk-margin-small-right   border-radius-6" href="#"> <i class="fas fa-play"></i> Resume</a>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div>
                                                    <a href="course-view.php" class="uk-link-reset">
                                                        <div class="uk-padding-small uk-card-default">
                                                            <progress id="js-progressbar" class="uk-progress progress-green uk-margin-small-bottom" value="100" max="100" style="height: 7px;"></progress>
                                                            <img src="assets/images/courses/tags/css3.JPG" class="uk-align-left  uk-margin-small-right uk-margin-small-bottom  uk-width-1-3  uk-visible@s" alt="">
                                                            <p class="uk-text-bold uk-margin-remove">Python Introduction
                                                            </p>
                                                            <p class="uk-text-small uk-margin-remove"> by : John Doe
                                                            </p>
                                                            <div class="uk-margin-small">
                                                                <a class="Course-tags uk-margin-small-right   border-radius-6" href="#"> <i class="fas fa-play"></i> Resume</a>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class=" uk-margin-remove">
                                    <h5 class="uk-padding-small uk-margin-remove uk-text-bold uk-text-center"><a class="uk-link-heading" href="#"> See all </a> </h5>
                                </div>
                            </li>
                            <li>
                                <!-- messages -->
                                <a href="#"><i class="fas fa-envelope icon-medium"></i></a>
                                <div uk-dropdown="pos: top-right ;mode : click; animation: uk-animation-slide-bottom-small" class="uk-dropdown uk-dropdown-top-right  tm-dropdown-medium border-radius-6 uk-padding-remove uk-box-shadow-large angle-top-right">
                                    <h5 class="uk-padding-small uk-margin-remove uk-text-bold  uk-text-left"> Messages
                                    </h5>
                                    <a href="#" class="uk-position-top-right uk-link-reset"> <i class="fas fa-trash uk-align-right uk-text-small uk-padding-small"> Clear
                                            all</i> </a>
                                    <hr class=" uk-margin-remove">
                                    <div class="uk-text-left uk-height-medium">
                                        <div uk-scrollspy="target: > div; cls:uk-animation-slide-bottom-small; delay: 100" data-simplebar>
                                            <hr class="uk-margin-remove uk-animation-slide-top-small">
                                            <div class=" uk-padding-small  uk-background-light uk-inline-clip uk-transition-toggle" tabindex="0">
                                                <div class="uk-transition-slide-right-small uk-position-top-right uk-position-z-index">
                                                    <a class="uk-button uk-padding-small-right uk-padding-remove-left" href="#"> Delete </a>
                                                </div>
                                                <div class="uk-transition-slide-right-medium uk-position-top-right uk-margin-medium-right">
                                                    <a class="uk-button uk-margin-small-right" href="#"> Replay </a>
                                                </div>
                                                <div class="uk-flex-middle uk-grid-small uk-grid" uk-grid="">
                                                    <div class="uk-width-3-4">
                                                        <p class="uk-margin-remove-bottom uk-text-bold">John keni </p>
                                                        <p class="uk-margin-remove">Lorem ipsum dolor sit ame ..</p>
                                                        <p class="uk-margin-remove-top uk-text-small uk-text-muted">25
                                                            min</p>
                                                    </div>
                                                    <div class="uk-width-1-4 uk-flex-first uk-first-column">
                                                        <img src="assets/images/avatures/avature-3.png" alt="Image" class="uk-border-circle uk-animation-slide-left-small">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr class=" uk-margin-remove">
                                            <div class=" uk-padding-small  uk-background-light uk-inline-clip uk-transition-toggle" tabindex="0">
                                                <div class="uk-transition-slide-right-small uk-position-top-right uk-position-z-index">
                                                    <a class="uk-button uk-padding-small-right uk-padding-remove-left" href="#"> Delete </a>
                                                </div>
                                                <div class="uk-transition-slide-right-medium uk-position-top-right uk-margin-medium-right">
                                                    <a class="uk-button uk-margin-small-right" href="#"> Replay </a>
                                                </div>
                                                <div class="uk-flex-middle uk-grid-small uk-grid" uk-grid="">
                                                    <div class="uk-width-3-4">
                                                        <p class="uk-margin-remove-bottom uk-text-bold">John keni </p>
                                                        <p class="uk-margin-remove">Lorem ipsum dolor sit ame ..</p>
                                                        <p class="uk-margin-remove-top uk-text-small uk-text-muted">25
                                                            min</p>
                                                    </div>
                                                    <div class="uk-width-1-4 uk-flex-first uk-first-column">
                                                        <img src="assets/images/avatures/avature.jpg" alt="Image" class="uk-border-circle uk-animation-slide-left-small">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr class=" uk-margin-remove">
                                            <div class=" uk-padding-small  uk-background-light uk-inline-clip uk-transition-toggle" tabindex="0">
                                                <div class="uk-transition-slide-right-small uk-position-top-right uk-position-z-index">
                                                    <a class="uk-button uk-padding-small-right uk-padding-remove-left" href="#"> Delete </a>
                                                </div>
                                                <div class="uk-transition-slide-right-medium uk-position-top-right uk-margin-medium-right">
                                                    <a class="uk-button uk-margin-small-right" href="#"> Replay </a>
                                                </div>
                                                <div class="uk-flex-middle uk-grid-small uk-grid" uk-grid="">
                                                    <div class="uk-width-3-4">
                                                        <p class="uk-margin-remove-bottom uk-text-bold">John keni </p>
                                                        <p class="uk-margin-remove">Lorem ipsum dolor sit ame ..</p>
                                                        <p class="uk-margin-remove-top uk-text-small uk-text-muted">25
                                                            min</p>
                                                    </div>
                                                    <div class="uk-width-1-4 uk-flex-first uk-first-column">
                                                        <img src="assets/images/avatures/avature-5.png" alt="Image" class="uk-border-circle uk-animation-slide-left-small">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr class=" uk-margin-remove">
                                            <div class=" uk-padding-small  uk-background-light uk-inline-clip uk-transition-toggle" tabindex="0">
                                                <div class="uk-transition-slide-right-small uk-position-top-right uk-position-z-index">
                                                    <a class="uk-button uk-padding-small-right uk-padding-remove-left" href="#"> Delete </a>
                                                </div>
                                                <div class="uk-transition-slide-right-medium uk-position-top-right uk-margin-medium-right">
                                                    <a class="uk-button uk-margin-small-right" href="#"> Replay </a>
                                                </div>
                                                <div class="uk-flex-middle uk-grid-small uk-grid" uk-grid="">
                                                    <div class="uk-width-3-4">
                                                        <p class="uk-margin-remove-bottom uk-text-bold">John keni </p>
                                                        <p class="uk-margin-remove">Lorem ipsum dolor sit ame ..</p>
                                                        <p class="uk-margin-remove-top uk-text-small uk-text-muted">25
                                                            min</p>
                                                    </div>
                                                    <div class="uk-width-1-4 uk-flex-first uk-first-column">
                                                        <img src="assets/images/avatures/avature-2.png" alt="Image" class="uk-border-circle uk-animation-slide-left-small">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr class=" uk-margin-remove">
                                            <div class=" uk-padding-small  uk-background-light uk-inline-clip uk-transition-toggle" tabindex="0">
                                                <div class="uk-transition-slide-right-small uk-position-top-right uk-position-z-index">
                                                    <a class="uk-button uk-padding-small-right uk-padding-remove-left" href="#"> Delete </a>
                                                </div>
                                                <div class="uk-transition-slide-right-medium uk-position-top-right uk-margin-medium-right">
                                                    <a class="uk-button uk-margin-small-right" href="#"> Replay </a>
                                                </div>
                                                <div class="uk-flex-middle uk-grid-small uk-grid" uk-grid="">
                                                    <div class="uk-width-3-4">
                                                        <p class="uk-margin-remove-bottom uk-text-bold">John keni </p>
                                                        <p class="uk-margin-remove">Lorem ipsum dolor sit ame ..</p>
                                                        <p class="uk-margin-remove-top uk-text-small uk-text-muted">25
                                                            min</p>
                                                    </div>
                                                    <div class="uk-width-1-4 uk-flex-first uk-first-column">
                                                        <img src="assets/images/avatures/avature-1.png" alt="Image" class="uk-border-circle">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr class=" uk-margin-remove">
                                            <div class=" uk-padding-small  uk-background-light uk-inline-clip uk-transition-toggle" tabindex="0">
                                                <div class="uk-transition-slide-right-small uk-position-top-right uk-position-z-index">
                                                    <a class="uk-button uk-padding-small-right uk-padding-remove-left" href="#"> Delete </a>
                                                </div>
                                                <div class="uk-transition-slide-right-medium uk-position-top-right uk-margin-medium-right">
                                                    <a class="uk-button uk-margin-small-right" href="#"> Replay </a>
                                                </div>
                                                <div class="uk-flex-middle uk-grid-small uk-grid" uk-grid="">
                                                    <div class="uk-width-3-4">
                                                        <p class="uk-margin-remove-bottom uk-text-bold">John keni </p>
                                                        <p class="uk-margin-remove">Lorem ipsum dolor sit ame ..</p>
                                                        <p class="uk-margin-remove-top uk-text-small uk-text-muted">25
                                                            min</p>
                                                    </div>
                                                    <div class="uk-width-1-4 uk-flex-first uk-first-column">
                                                        <img src="assets/images/avatures/avature.jpg" alt="Image" class="uk-border-circle">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class=" uk-margin-remove">
                                    <h5 class="uk-padding-small uk-margin-remove uk-text-bold uk-text-center"><a class="uk-link-heading" href=""> See all </a> </h5>
                                </div>
                            </li>
                            <li>
                                <!-- Notifications -->
                                <a href="#"><i class="fas fa-bell icon-medium"></i></a>
                                <div uk-dropdown="pos: top-right ;mode : hover; animation: uk-animation-slide-bottom-small" class="uk-dropdown uk-dropdown-top-right  tm-dropdown-small border-radius-6 uk-padding-remove uk-box-shadow-large angle-top-right">
                                    <h5 class="uk-padding-small uk-margin-remove uk-text-bold  uk-text-left">
                                        Notifications </h5>
                                    <a href="#" class="uk-position-top-right uk-link-reset"> <i class="fas fa-trash uk-align-right uk-text-small uk-padding-small"> Clear
                                            all</i></a>
                                    <hr class=" uk-margin-remove">
                                    <div class="uk-padding-smaluk-text-left uk-height-medium">
                                        <div data-simplebar>
                                            <div class="uk-padding-small" uk-scrollspy="target: > div; cls:uk-animation-slide-bottom-small; delay: 100 ; repeat: true">
                                                <div class="uk-flex-middle uk-grid-small" uk-grid>
                                                    <div class="uk-width-3-4">
                                                        <p class="uk-margin-remove">Lorem ipsum dolor ame ..</p>
                                                        <p class="uk-margin-remove-top uk-text-small uk-text-muted">25
                                                            min</p>
                                                    </div>
                                                    <div class="uk-width-1-5 uk-flex-first">
                                                        <img src="assets/images/avatures/avature-4.png" alt="Image" class="uk-border-circle">
                                                    </div>
                                                </div>
                                                <div class="uk-flex-middle uk-grid-small" uk-grid>
                                                    <div class="uk-width-3-4">
                                                        <p class="uk-margin-remove">Lorem ipsum dolor ame ..</p>
                                                        <p class="uk-margin-remove-top uk-text-small uk-text-muted">25
                                                            min</p>
                                                    </div>
                                                    <div class="uk-width-1-5 uk-flex-first">
                                                        <img src="assets/images/avatures/avature-1.png" alt="Image" class="uk-border-circle">
                                                    </div>
                                                </div>
                                                <div class="uk-flex-middle uk-grid-small" uk-grid>
                                                    <div class="uk-width-3-4">
                                                        <p class="uk-margin-remove">Lorem ipsum dolor ame ..</p>
                                                        <p class="uk-margin-remove-top uk-text-small uk-text-muted">25
                                                            min</p>
                                                    </div>
                                                    <div class="uk-width-1-5 uk-flex-first">
                                                        <img src="assets/images/avatures/avature.jpg" alt="Image" class="uk-border-circle">
                                                    </div>
                                                </div>
                                                <div class="uk-flex-middle uk-grid-small" uk-grid>
                                                    <div class="uk-width-3-4">
                                                        <p class="uk-margin-remove">Lorem ipsum dolor ame ..</p>
                                                        <p class="uk-margin-remove-top uk-text-small uk-text-muted">25
                                                            min</p>
                                                    </div>
                                                    <div class="uk-width-1-5 uk-flex-first">
                                                        <img src="assets/images/avatures/avature-2.png" alt="Image" class="uk-border-circle">
                                                    </div>
                                                </div>
                                                <div class="uk-flex-middle uk-grid-small" uk-grid>
                                                    <div class="uk-width-3-4">
                                                        <p class="uk-margin-remove">Lorem ipsum dolor ame ..</p>
                                                        <p class="uk-margin-remove-top uk-text-small uk-text-muted">25
                                                            min</p>
                                                    </div>
                                                    <div class="uk-width-1-5 uk-flex-first">
                                                        <img src="assets/images/avatures/avature-3.png" alt="Image" class="uk-border-circle">
                                                    </div>
                                                </div>
                                                <div class="uk-flex-middle uk-grid-small" uk-grid>
                                                    <div class="uk-width-3-4">
                                                        <p class="uk-margin-remove">Lorem ipsum dolor ame ..</p>
                                                        <p class="uk-margin-remove-top uk-text-small uk-text-muted">25
                                                            min</p>
                                                    </div>
                                                    <div class="uk-width-1-5 uk-flex-first">
                                                        <img src="assets/images/avatures/avature-4.png" alt="Image" class="uk-border-circle">
                                                    </div>
                                                </div>
                                                <div class="uk-flex-middle uk-grid-small" uk-grid>
                                                    <div class="uk-width-3-4">
                                                        <p class="uk-margin-remove">Lorem ipsum dolor ame ..</p>
                                                        <p class="uk-margin-remove-top uk-text-small uk-text-muted">25
                                                            min</p>
                                                    </div>
                                                    <div class="uk-width-1-5 uk-flex-first">
                                                        <img src="assets/images/avatures/avature-1.png" alt="Image" class="uk-border-circle">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <!-- User profile -->
                                <a href="#">
                                    <img src="assets/images/avatures/avature-2.png" alt="" class="uk-border-circle user-profile-tiny">
                                </a>
                                <div uk-dropdown="pos: top-right ;mode : click ;animation: uk-animation-slide-right" class="uk-dropdown uk-padding-small uk-dropdown-top-right  angle-top-right">
                                    <p class="uk-margin-remove-bottom uk-margin-small-top uk-text-bold"> <?php echo $name; ?>
                                    </p>
                                    <p class="uk-margin-remove-top uk-text-small uk-margin-small-bottom"> IJBridge
                                    </p>
                                    <ul class="uk-nav uk-dropdown-nav">
                                        <li>
                                            <a href="profile.php"> <i class="fas fa-user uk-margin-small-right"></i>
                                                Profile</a>
                                        </li>
                                        <li>
                                            <a href="#"> <i class="fas fa-envelope uk-margin-small-right"></i> Messages
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#"> <i class="fas fa-share uk-margin-small-right"></i> Invite
                                                freind</a>
                                        </li>
                                        <li>
                                            <a href="#"> <i class="fas fa-cog uk-margin-small-right"></i> Setting</a>
                                        </li>
                                        <li class="uk-nav-divider"></li>
                                        <li>
                                            <a href="logout.php"> <i class="fas fa-sign-out-alt uk-margin-small-right"></i> Log out</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- Navigation for mobile -->
                    <div id="mobile-sidebar" class="mobile-sidebar" uk-offcanvas="overlay:true">
                        <div class="uk-offcanvas-bar uk-preserve-color uk-padding-remove">
                            <ul uk-accordion>
                                <li class="uk-open">
                                    <a href="#" class="uk-accordion-title uk-text-black uk-padding-small"> <i class="fas fa-play-circle uk-margin-small-right"></i> Courses </a>
                                    <div class="uk-accordion-content uk-margin-remove-top">
                                        <ul class="uk-list tm-drop-topic-list">
                                            <li>
                                                <a href="#"> All Development</a>
                                            </li>
                                            <li>
                                                <a href="#"> Web Development </a>
                                            </li>
                                            <li>
                                                <a href="#"> Mobile App </a>
                                            </li>
                                            <li>
                                                <a href="#"> Programming language </a>
                                            </li>
                                            <li>
                                                <a href="#"> Software </a>
                                            </li>
                                            <li>
                                                <a href="#"> Ecommerce </a>
                                            </li>
                                            <li>
                                                <a href="#"> Training</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="uk-margin-remove-top">
                                    <a href="#" class="uk-accordion-title uk-text-black uk-padding-small"> <i class="fas fa-code uk-margin-small-right"></i> Scripts </a>
                                    <div class="uk-accordion-content uk-margin-remove-top">
                                        <ul class="uk-list tm-drop-topic-list">
                                            <li>
                                                <a href="#"> Php scrips </a>
                                            </li>
                                            <li>
                                                <a href="#"> HTML templates </a>
                                            </li>
                                            <li>
                                                <a href="#"> Wordpress plugins </a>
                                            </li>
                                            <li>
                                                <a href="#"> Wordpress themes </a>
                                            </li>
                                            <li>
                                                <a href="#"> App source codes </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="uk-margin-remove-top uk-padding-small">
                                    <a href="#" class="uk-text-black"> <i class="fas fa-book-open uk-margin-small-right"></i> Books </a>
                                </li>
                                <li class="uk-margin-remove-top uk-padding-small">
                                    <a href="#" class="uk-text-black"> <i class="fas fa-file-alt uk-margin-small-right"></i> Blog </a>
                                </li>
                                <li class="uk-margin-remove-top uk-padding-small">
                                    <a href="#" class="uk-text-black"> <i class="fas fa-comment-alt uk-margin-small-right"></i> Discussion </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- search box -->
                    <div id="modal-full" class="uk-modal-full uk-modal uk-animation-scale-down" uk-modal>
                        <div class="uk-modal-dialog uk-flex uk-flex-center" uk-height-viewport>
                            <button class="uk-modal-close-full" type="button" uk-close></button>
                            <form class="uk-search uk-margin-xlarge-top uk-search-large uk-animation-slide-bottom-medium">
                                <i class="fas fa-search uk-position-absolute uk-margin-top icon-xxlarge"></i>
                                <input class="uk-search-input uk-margin-large-left" type="search" placeholder="Search..." autofocus>
                            </form>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <main>
        <div class="course-dhashboard-2 topic1">
            <div class="uk-container course-intro-content uk-padding-remove-bottom">
                <div uk-grid>
                    <div class="uk-width-1-3@m">
                        <div class="uk-inline uk-visible-toggle">
                            <img src="assets/images/courses/course-1.jpg" alt="">
                            <a class="uk-position-absolute uk-position-center uk-invisible-hover uk-link-reset" href="#preview-video-1" uk-toggle> <i class="fas fa-play-circle" style="font-size: 60px"></i> </a>
                        </div>
                    </div>
                    <div class="uk-width-2-3@m info">
                        <h2 class="uk-light uk-text-uppercase uk-text-bold uk-text-white">Introduction of Simulink
                        </h2>
                        <p class="uk-light uk-text-bold">The easiest way to learn everything about Simulink and its operations.</p>
                        <!-- students images  -->
                        <div class="avatar-group uk-margin" uk-scrollspy="target: > img; cls:uk-animation-slide-right; delay: 200">
                            <img src="assets/images/avatures/avature-1.png" alt="">
                            <img src="assets/images/avatures/avature.jpg" alt="">
                            <img src="assets/images/avatures/avature-2.png" alt="">
                            <span class="uk-text-bold uk-light"> 134 + students enrolled </span>
                        </div>
                        <div class="uk-grid-small uk-width-2-3@m" uk-grid>
                            <div class="uk-width-auto">
                                <a class="uk-button uk-button-white uk-float-left" href="course-lesson.php" uk-tooltip="title: Continue where you left; delay: 300 ; pos: top ;animation:	uk-animation-slide-bottom-small">
                                    Continue</a>
                            </div>
                            <div class="uk-width-expand">
                                <span class="uk-light uk-text-small uk-text-bold"> My progress </span>
                                <progress id="js-progressbar" class="uk-progress progress-green uk-margin-small-bottom uk-margin-small-top" value="50" max="100" style="height: 8px;"></progress>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- navigation-->
        <ul class="uk-tab uk-flex-center  uk-margin-remove-top uk-background-default  uk-sticky" cls-active="uk-background-muted" uk-sticky="animation: uk-animation-slide-top; bottom: #sticky-on-scroll-up ; media: @s;offset:85" uk-tab>
            <li aria-expanded="true" class="uk-active">
                <a href="#"> Course contents </a>
            </li>
            <li>
                <a href="#"> Discussions </a>
            </li>
            <li>
                <a href="#"> Exercise </a>
            </li>
            <li>
                <a href="#"> About </a>
            </li>
            <li>
                <a href="#"> Reveiws </a>
            </li>
        </ul>
        <ul class="uk-switcher uk-margin uk-padding-small uk-container-small uk-margin-auto  uk-margin-medium-top">
            <!-- Course-Videos tab  -->
            <li class="uk-active">
                <ul uk-accordion class="uk-accordion">
                    <li class="uk-open tm-course-lesson-section uk-background-default">
                        <a class="uk-accordion-title uk-padding-small" href="#">
                            <h6> section one</h6>
                            <h4 class="uk-margin-remove"> Introduction to MATLAB</h4>
                        </a>
                        <div class="uk-accordion-content uk-margin-remove-top">
                            <div class="tm-course-section-list">
                                <ul>
                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <!-- Play icon  -->
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <!-- Course title  -->
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right">
                                                Multi port switch </div>
                                            <!-- preview link -->
                                        </a>
                                        <a class="uk-link-reset uk-margin-large-right uk-position-center-right uk-padding-small uk-text-small uk-visible@s" href="#preview-video-1" uk-toggle> <i class="fas fa-play icon-small uk-text-grey"></i> Preview </a>
                                        <!-- time -->
                                        <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 2 min</span>
                                    </li>
                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right">
                                                Gain </div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 6 min</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right">
                                                Bus selector</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 8 min</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <!-- Play icon  -->
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <!-- Course title  -->
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right">
                                                Bus creator </div>
                                            <!-- preview link -->
                                        </a>
                                        <a class="uk-link-reset uk-margin-large-right uk-position-center-right uk-padding-small uk-text-small uk-visible@s" href="#preview-video-2" uk-toggle> <i class="fas fa-play icon-small uk-text-grey"></i> Preview </a>
                                        <!-- time -->
                                        <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 2 min</span>
                                    </li>
                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right"> Abs block</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 12 min</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right"> Manual Switch</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 12 min</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right"> ADD Block</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 12 min</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right"> Create New Model</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 12 min</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right"> Introduction to Simulink</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 12 min</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right"> Logical Blocks</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 12 min</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right"> Pulse Generator</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 12 min</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right"> Scope</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 12 min</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right"> Set Clear Bit</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 12 min</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right"> 1D Block</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 12 min</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right"> 2D LUT</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 12 min</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right"> Data Dictonary</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 12 min</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right"> MBD Intro</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 12 min</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right"> Minmax Block</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 12 min</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right"> Solver And its type</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 12 min</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right"> Subsystems</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 12 min</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right"> Switch</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 12 min</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right"> Ramp</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 12 min</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right"> Step</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 12 min</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="course-lesson.php" class="uk-link-reset">
                                            <span class="uk-icon-button icon-play"> <i class="fas fa-play icon-small"></i> </span>
                                            <div class="uk-panel uk-panel-box uk-text-truncate uk-margin-medium-right"> Signal Generator</div>
                                            <span class="uk-position-center-right time uk-margin-right"> <i class="fas fa-clock icon-small"></i> 12 min</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
                <!-- Model  Preview videos-->
                <div id="preview-video-1" uk-modal>
                    <div class="uk-modal-dialog uk-margin-auto-vertical">
                        <button class="uk-modal-close-outside" type="button" uk-close></button>
                        <div class="video-responsive">
                            <iframe src="https://www.youtube.com/embed/0wvm6f7MuZM?autoplay=1&showinfo=1&rel=0" class="uk-padding-remove" uk-video="automute: true" frameborder="0" allowfullscreen uk-responsive></iframe>
                        </div>
                    </div>
                </div>
                <!-- Model  Preview videos-->
                <div id="preview-video-2" uk-modal>
                    <div class="uk-modal-dialog uk-margin-auto-vertical">
                        <button class="uk-modal-close-outside" type="button" uk-close></button>
                        <div class="video-responsive">
                            <iframe src="https://www.youtube.com/embed/OWEQ7CSL6JE?autoplay=1&showinfo=1&rel=0" class="uk-padding-remove" uk-video="automute: true" frameborder="0" allowfullscreen uk-responsive></iframe>
                        </div>
                    </div>
                </div>
            </li>
            <!-- Discussions tab  -->
            <li uk-scrollspy="target: > div; cls:uk-animation-slide-bottom-medium; delay: 200">
                <h3>Discussions</h3>
                <div class="uk-padding-small uk-background-light uk-position-relative">
                    <div class="uk-flex-middle uk-grid-small uk-grid-stack" uk-grid>
                        <div class="uk-width-1-6 uk-flex-first uk-first-column">
                            <img src="assets/images/avatures/avature-3.png" alt="Image" class="uk-width-1-2 uk-border-circle">
                        </div>
                        <div class="uk-width-5-6">
                            <p class="uk-margin-remove-bottom uk-text-bold uk-text-small">John keniss </p>
                            <p class="uk-margin-remove">Etiam sit amet augue non velit aliquet consectetur. Proin
                                gravida, odio in facilisis pharetra, neque enim aliquam eros,.</p>
                        </div>
                    </div>
                </div>
                <div class="uk-padding-small uk-background-light uk-position-relative">
                    <div class="uk-flex-middle uk-grid-small uk-grid-stack" uk-grid>
                        <div class="uk-width-1-6 uk-flex-first uk-first-column">
                            <img src="assets/images/avatures/avature-5.png" alt="Image" class="uk-width-1-2 uk-border-circle">
                        </div>
                        <div class="uk-width-5-6">
                            <p class="uk-margin-remove-bottom uk-text-bold uk-text-small">John keniss </p>
                            <p class="uk-margin-remove">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>
                <div class="uk-padding-small uk-background-light uk-position-relative">
                    <div class="uk-flex-middle uk-grid-small uk-grid-stack" uk-grid>
                        <div class="uk-width-1-6 uk-flex-first uk-first-column">
                            <img alt="Image" src="assets/images/avatures/avature.jpg" class="uk-width-1-2 uk-border-circle">
                        </div>
                        <div class="uk-width-5-6">
                            <p class="uk-margin-remove-bottom uk-text-bold uk-text-small">John keniss </p>
                            <p class="uk-margin-remove">Maecenas vel dolor sit amet ligula interdum tempor id eu ipsum.
                                Suspendisse pharetra risus ut metus elementum pulvinar. Mauris eget varius tellus. Cras
                                et lorem vel nunc gravida porttitor.</p>
                        </div>
                    </div>
                </div>
                <div class="uk-padding-small uk-background-light uk-position-relative">
                    <div class="uk-flex-middle uk-grid-small uk-grid-stack" uk-grid>
                        <div class="uk-width-1-6 uk-flex-first uk-first-column">
                            <img src="assets/images/avatures/avature-1.png" alt="Image" class="uk-width-1-2 uk-border-circle">
                        </div>
                        <div class="uk-width-5-6">
                            <p class="uk-margin-remove-bottom uk-text-bold uk-text-small">John keniss </p>
                            <p class="uk-margin-remove">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>
                <div class="uk-padding-small uk-background-light uk-position-relative">
                    <div class="uk-flex-middle uk-grid-small uk-grid-stack" uk-grid>
                        <div class="uk-width-1-6 uk-flex-first uk-first-column">
                            <img src="assets/images/avatures/avature-2.png" alt="Image" class="uk-width-1-2 uk-border-circle">
                        </div>
                        <div class="uk-width-5-6">
                            <p class="uk-margin-remove-bottom uk-text-bold uk-text-small">John keniss </p>
                            <p class="uk-margin-remove">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>
                <div class="uk-padding-small uk-background-light uk-position-relative">
                    <div class="uk-flex-middle uk-grid-small uk-grid-stack" uk-grid>
                        <div class="uk-width-1-6 uk-flex-first uk-first-column">
                            <img src="assets/images/avatures/avature-4.png" alt="Image" class="uk-width-1-2 uk-border-circle">
                        </div>
                        <div class="uk-width-5-6">
                            <p class="uk-margin-remove-bottom uk-text-bold uk-text-small">John keniss </p>
                            <p class="uk-margin-remove">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>
                <div class="uk-padding-small uk-background-light uk-position-relative">
                    <div class="uk-flex-middle uk-grid-small uk-grid-stack" uk-grid>
                        <div class="uk-width-1-6 uk-flex-first uk-first-column">
                            <img src="assets/images/avatures/avature-3.png" alt="Image" class="uk-width-1-2 uk-border-circle">
                        </div>
                        <div class="uk-width-5-6">
                            <p class="uk-margin-remove-bottom uk-text-bold uk-text-small">John keniss </p>
                            <p class="uk-margin-remove">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>
                <div class="uk-padding-small uk-background-light uk-position-relative">
                    <div class="uk-flex-middle uk-grid-small uk-grid-stack" uk-grid>
                        <div class="uk-width-1-6 uk-flex-first uk-first-column">
                            <img src="assets/images/avatures/avature-4.png" alt="Image" class="uk-width-1-2 uk-border-circle">
                        </div>
                        <div class="uk-width-5-6">
                            <p class="uk-margin-remove-bottom uk-text-bold uk-text-small">John keniss </p>
                            <p class="uk-margin-remove">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                    </div>
                </div>
                </div>
                <!-- Discussions tab  -->
                <div id="Discussions" class="tabcontent animation:uk-animation-fade">
                    <h3>Discussions</h3>
                    <div class="uk-padding-small uk-background-light uk-position-relative">
                        <div class="uk-flex-middle uk-grid-small uk-grid-stack" uk-grid>
                            <div class="uk-width-1-6 uk-flex-first uk-first-column">
                                <img src="assets/images/avatures/avature-3.png" alt="Image" class="uk-width-1-2 uk-border-circle">
                            </div>
                            <div class="uk-width-5-6">
                                <p class="uk-margin-remove-bottom uk-text-bold uk-text-small">John keniss </p>
                                <p class="uk-margin-remove">Etiam sit amet augue non velit aliquet consectetur. Proin
                                    gravida, odio in facilisis pharetra, neque enim aliquam eros,.</p>
                            </div>
                        </div>
                    </div>
                    <div class="uk-padding-small uk-background-light uk-position-relative">
                        <div class="uk-flex-middle uk-grid-small uk-grid-stack" uk-grid>
                            <div class="uk-width-1-6 uk-flex-first uk-first-column">
                                <img src="assets/images/avatures/avature-5.png" alt="Image" class="uk-width-1-2 uk-border-circle">
                            </div>
                            <div class="uk-width-5-6">
                                <p class="uk-margin-remove-bottom uk-text-bold uk-text-small">John keniss </p>
                                <p class="uk-margin-remove">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                    <div class="uk-padding-small uk-background-light uk-position-relative">
                        <div class="uk-flex-middle uk-grid-small uk-grid-stack" uk-grid>
                            <div class="uk-width-1-6 uk-flex-first uk-first-column">
                                <img alt="Image" src="assets/images/avatures/avature.jpg" class="uk-width-1-2 uk-border-circle">
                            </div>
                            <div class="uk-width-5-6">
                                <p class="uk-margin-remove-bottom uk-text-bold uk-text-small">John keniss </p>
                                <p class="uk-margin-remove">Maecenas vel dolor sit amet ligula interdum tempor id eu
                                    ipsum. Suspendisse pharetra risus ut metus elementum pulvinar. Mauris eget varius
                                    tellus. Cras et lorem vel nunc gravida porttitor.</p>
                            </div>
                        </div>
                    </div>
                    <div class="uk-padding-small uk-background-light uk-position-relative">
                        <div class="uk-flex-middle uk-grid-small uk-grid-stack" uk-grid>
                            <div class="uk-width-1-6 uk-flex-first uk-first-column">
                                <img src="assets/images/avatures/avature-1.png" alt="Image" class="uk-width-1-2 uk-border-circle">
                            </div>
                            <div class="uk-width-5-6">
                                <p class="uk-margin-remove-bottom uk-text-bold uk-text-small">John keniss </p>
                                <p class="uk-margin-remove">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                    <div class="uk-padding-small uk-background-light uk-position-relative">
                        <div class="uk-flex-middle uk-grid-small uk-grid-stack" uk-grid>
                            <div class="uk-width-1-6 uk-flex-first uk-first-column">
                                <img src="assets/images/avatures/avature-2.png" alt="Image" class="uk-width-1-2 uk-border-circle">
                            </div>
                            <div class="uk-width-5-6">
                                <p class="uk-margin-remove-bottom uk-text-bold uk-text-small">John keniss </p>
                                <p class="uk-margin-remove">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                    <div class="uk-padding-small uk-background-light uk-position-relative">
                        <div class="uk-flex-middle uk-grid-small uk-grid-stack" uk-grid>
                            <div class="uk-width-1-6 uk-flex-first uk-first-column">
                                <img src="assets/images/avatures/avature-4.png" alt="Image" class="uk-width-1-2 uk-border-circle">
                            </div>
                            <div class="uk-width-5-6">
                                <p class="uk-margin-remove-bottom uk-text-bold uk-text-small">John keniss </p>
                                <p class="uk-margin-remove">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                    <div class="uk-padding-small uk-background-light uk-position-relative">
                        <div class="uk-flex-middle uk-grid-small uk-grid-stack" uk-grid>
                            <div class="uk-width-1-6 uk-flex-first uk-first-column">
                                <img src="assets/images/avatures/avature-3.png" alt="Image" class="uk-width-1-2 uk-border-circle">
                            </div>
                            <div class="uk-width-5-6">
                                <p class="uk-margin-remove-bottom uk-text-bold uk-text-small">John keniss </p>
                                <p class="uk-margin-remove">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                    <div class="uk-padding-small uk-background-light uk-position-relative">
                        <div class="uk-flex-middle uk-grid-small uk-grid-stack" uk-grid>
                            <div class="uk-width-1-6 uk-flex-first uk-first-column">
                                <img src="assets/images/avatures/avature-4.png" alt="Image" class="uk-width-1-2 uk-border-circle">
                            </div>
                            <div class="uk-width-5-6">
                                <p class="uk-margin-remove-bottom uk-text-bold uk-text-small">John keniss </p>
                                <p class="uk-margin-remove">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
            </li>
            <!-- Exercise-Files  -->
            <li>
                <div uk-grid class="uk-grid-stack">
                    <div class="uk-width-1-2@m uk-margin-bottom">
                        <ul uk-accordion="" class="uk-accordion">
                            <!-- accordion 1 -->
                            <li class="tm-course-lesson-section uk-background-default uk-open">
                                <a class="uk-accordion-title uk-padding-small" href="#">
                                    <h6> section one</h6>
                                    <h4 class="uk-margin-remove"> Introduction to basic HTML</h4>
                                </a>
                                <div class="uk-accordion-content uk-margin-remove-top" aria-hidden="false">
                                    <!-- Exerice files-->
                                    <ul class="uk-active uk-background-default uk-accordion" uk-accordion="">
                                        <li class="uk-open tm-course-lesson-section">
                                            <a class="uk-accordion-title uk-padding-small uk-background-muted uk-padding-remove-bottom" href="#">
                                                <h6> <i class="fas fa-folder"></i> Lesson one </h6>
                                            </a>
                                            <div class="uk-accordion-content uk-margin-remove-top" aria-hidden="false">
                                                <div class="tm-course-section-list">
                                                    <ul>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File One
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File two
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            file three
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File four
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="tm-course-lesson-section uk-margin-remove-top">
                                            <a class="uk-accordion-title uk-padding-small uk-background-muted uk-padding-remove-bottom" href="#">
                                                <h6> <i class="fas fa-folder"></i> Lesson two</h6>
                                            </a>
                                            <div class="uk-accordion-content uk-margin-remove-top" hidden="" aria-hidden="true">
                                                <div class="tm-course-section-list">
                                                    <ul>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File One
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File two
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!--  accordion 2 -->
                            <li class="tm-course-lesson-section uk-background-default">
                                <a class="uk-accordion-title uk-padding-small" href="#">
                                    <h6> section Two </h6>
                                    <h4 class="uk-margin-remove"> HTML5 features</h4>
                                </a>
                                <div class="uk-accordion-content uk-margin-remove-top" hidden="" aria-hidden="true">
                                    <!-- Exerice files-->
                                    <ul class="uk-active uk-background-default uk-accordion" uk-accordion="">
                                        <li class="uk-open tm-course-lesson-section">
                                            <a class="uk-accordion-title uk-padding-small uk-background-muted uk-padding-remove-bottom" href="#">
                                                <h6> <i class="fas fa-folder"></i> Lesson one</h6>
                                            </a>
                                            <div class="uk-accordion-content uk-margin-remove-top" aria-hidden="false">
                                                <div class="tm-course-section-list">
                                                    <ul>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File One
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File two
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            file three
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File four
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="tm-course-lesson-section uk-margin-remove-top">
                                            <a class="uk-accordion-title uk-padding-small uk-background-muted uk-padding-remove-bottom" href="#">
                                                <h6> <i class="fas fa-folder"></i> Lesson two</h6>
                                            </a>
                                            <div class="uk-accordion-content uk-margin-remove-top" hidden="" aria-hidden="true">
                                                <div class="tm-course-section-list">
                                                    <ul>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            Lesson One
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            Lesson One
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!-- accordion 3 -->
                            <li class="tm-course-lesson-section uk-background-default">
                                <a class="uk-accordion-title uk-padding-small" href="#">
                                    <h6> section Two </h6>
                                    <h4 class="uk-margin-remove"> HTML5 features</h4>
                                </a>
                                <div class="uk-accordion-content uk-margin-remove-top" hidden="" aria-hidden="true">
                                    <!-- Exerice files-->
                                    <ul class="uk-active uk-background-default uk-accordion" uk-accordion="">
                                        <li class="uk-open tm-course-lesson-section">
                                            <a class="uk-accordion-title uk-padding-small uk-background-muted uk-padding-remove-bottom" href="#">
                                                <h6> <i class="fas fa-folder"></i> Lesson one</h6>
                                            </a>
                                            <div class="uk-accordion-content uk-margin-remove-top" aria-hidden="false">
                                                <div class="tm-course-section-list">
                                                    <ul>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File One
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File two
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            file three
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File four
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="tm-course-lesson-section uk-margin-remove-top">
                                            <a class="uk-accordion-title uk-padding-small uk-background-muted uk-padding-remove-bottom" href="#">
                                                <h6> <i class="fas fa-folder"></i> Lesson two</h6>
                                            </a>
                                            <div class="uk-accordion-content uk-margin-remove-top" hidden="" aria-hidden="true">
                                                <div class="tm-course-section-list">
                                                    <ul>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            Lesson One
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            Lesson One
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="uk-width-1-2@m uk-margin-bottom">
                        <ul uk-accordion="" class="uk-accordion">
                            <!-- accordion 1 -->
                            <li class="tm-course-lesson-section uk-background-default uk-open">
                                <a class="uk-accordion-title uk-padding-small" href="#">
                                    <h6> section one</h6>
                                    <h4 class="uk-margin-remove"> Introduction to basic HTML</h4>
                                </a>
                                <div class="uk-accordion-content uk-margin-remove-top" aria-hidden="false">
                                    <!-- Exerice files-->
                                    <ul class="uk-active uk-background-default uk-accordion" uk-accordion="">
                                        <li class="uk-open tm-course-lesson-section">
                                            <a class="uk-accordion-title uk-padding-small uk-background-muted uk-padding-remove-bottom" href="#">
                                                <h6> <i class="fas fa-folder"></i> Lesson one </h6>
                                            </a>
                                            <div class="uk-accordion-content uk-margin-remove-top" aria-hidden="false">
                                                <div class="tm-course-section-list">
                                                    <ul>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File One
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File two
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            file three
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File four
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="tm-course-lesson-section uk-margin-remove-top">
                                            <a class="uk-accordion-title uk-padding-small uk-background-muted uk-padding-remove-bottom" href="#">
                                                <h6> <i class="fas fa-folder"></i> Lesson two</h6>
                                            </a>
                                            <div class="uk-accordion-content uk-margin-remove-top" hidden="" aria-hidden="true">
                                                <div class="tm-course-section-list">
                                                    <ul>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File One
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File two
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!--  accordion 2 -->
                            <li class="tm-course-lesson-section uk-background-default">
                                <a class="uk-accordion-title uk-padding-small" href="#">
                                    <h6> section Two </h6>
                                    <h4 class="uk-margin-remove"> HTML5 features</h4>
                                </a>
                                <div class="uk-accordion-content uk-margin-remove-top" hidden="" aria-hidden="true">
                                    <!-- Exerice files-->
                                    <ul class="uk-active uk-background-default uk-accordion" uk-accordion="">
                                        <li class="uk-open tm-course-lesson-section">
                                            <a class="uk-accordion-title uk-padding-small uk-background-muted uk-padding-remove-bottom" href="#">
                                                <h6> <i class="fas fa-folder"></i> Lesson one</h6>
                                            </a>
                                            <div class="uk-accordion-content uk-margin-remove-top" aria-hidden="false">
                                                <div class="tm-course-section-list">
                                                    <ul>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File One
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File two
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            file three
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File four
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="tm-course-lesson-section uk-margin-remove-top">
                                            <a class="uk-accordion-title uk-padding-small uk-background-muted uk-padding-remove-bottom" href="#">
                                                <h6> <i class="fas fa-folder"></i> Lesson two</h6>
                                            </a>
                                            <div class="uk-accordion-content uk-margin-remove-top" hidden="" aria-hidden="true">
                                                <div class="tm-course-section-list">
                                                    <ul>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            Lesson One
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            Lesson One
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <!-- accordion 3 -->
                            <li class="tm-course-lesson-section uk-background-default">
                                <a class="uk-accordion-title uk-padding-small" href="#">
                                    <h6> section Two </h6>
                                    <h4 class="uk-margin-remove"> HTML5 features</h4>
                                </a>
                                <div class="uk-accordion-content uk-margin-remove-top" hidden="" aria-hidden="true">
                                    <!-- Exerice files-->
                                    <ul class="uk-active uk-background-default uk-accordion" uk-accordion="">
                                        <li class="uk-open tm-course-lesson-section">
                                            <a class="uk-accordion-title uk-padding-small uk-background-muted uk-padding-remove-bottom" href="#">
                                                <h6> <i class="fas fa-folder"></i> Lesson one</h6>
                                            </a>
                                            <div class="uk-accordion-content uk-margin-remove-top" aria-hidden="false">
                                                <div class="tm-course-section-list">
                                                    <ul>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File One
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File two
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            file three
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            File four
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="tm-course-lesson-section uk-margin-remove-top">
                                            <a class="uk-accordion-title uk-padding-small uk-background-muted uk-padding-remove-bottom" href="#">
                                                <h6> <i class="fas fa-folder"></i> Lesson two</h6>
                                            </a>
                                            <div class="uk-accordion-content uk-margin-remove-top" hidden="" aria-hidden="true">
                                                <div class="tm-course-section-list">
                                                    <ul>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            Lesson One
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-file-alt uk-margin-small-right icon-medium"></i>
                                                            Lesson One
                                                            <a class="uk-icon-button uk-margin-small-right uk-button-success uk-position-center-right" href="#"> <i class="fas fa-download icon-small"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
            <!-- tab 1 About the course -->
            <li>
                <h3> About this Course</h3>
                <p> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
                    laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim laoreet dolore magna aliquam erat
                    volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis
                    nisl ut aliquip ex ea commodo consequat</p>
                <p> Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut
                    aliquip ex ea commodo consequa mod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
                <p>Magna consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore aliquam
                    erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit
                    lobortis nisl ut aliquip ex ea commodo consequat. Nam liber tempor cum soluta nobis eleifend option
                    congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet,
                    consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna
                    aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit
                    lobortis nisl ut aliquip ex ea commodo consequat </p>
                <p>consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam
                    erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci </p>
                <b> Interdum et malesuada fames ipsum </b>
                <p> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
                    laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim laoreet dolore magna aliquam erat
                    volutpat. Ut wisi enim ad n ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat
                </p>
                <h4> Elementum tellus id mauris faucibus</h4>
                <ul class="uk-list uk-list-bullet">
                    <li>At vero eos et accusam et justo .</li>
                    <li>Consetetur sadipscing elitr, eirmod tempor invidunt .</li>
                    <li>Lorem ipsum dolor sit amet .</li>
                </ul>
                <h4>Uismod tincidunt ut laoreet dolore </h4>
                <p> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
                    laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim laoreet dolore magna aliquam erat
                    volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis
                    nisl ut aliquip ex ea commodo consequat</p>
            </li>
            <!-- tab 3 Reveiws -->
            <li>
                <h2 class="uk-heading-line uk-text-center"><span> Reveiws </span></h2>
                <div class="uk-grid-small  uk-margin-medium-top" uk-grid>
                    <div class="uk-width-1-5 uk-first-column">
                        <img alt="Image" class="uk-width-1-2 uk-margin-small-top uk-margin-small-bottom uk-border-circle uk-align-center  uk-box-shadow-large" src="assets/images/avatures/avature-2.png">
                    </div>
                    <div class="uk-width-4-5 uk-padding-remove-left">
                        <div class="uk-float-right">
                            <i class="fas fa-star icon-small"></i>
                            <i class="fas fa-star icon-small"></i>
                            <i class="fas fa-star icon-small"></i>
                            <i class="far fa-star icon-small"></i>
                            <i class="far fa-star icon-small"></i>
                        </div>
                        <h4 class="uk-margin-remove"> Masriam keny </h4>
                        <span class="uk-text-small"> 2 Hourse ago</span>
                        <hr class="uk-margin-small">
                        <p class="uk-margin-remove-top uk-margin-small-bottom">Lorem ipsum dolor sit amet, consectetuer
                            adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam
                            erat volutpat. Ut wisi enim ad minim laoreet dolore magna aliquam erat volutpat. Ut wisi
                            enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut
                            aliquip ex ea commodo consequat</p>
                    </div>
                </div>
                <div class="uk-grid-small  uk-margin-medium-top" uk-grid>
                    <div class="uk-width-1-5 uk-first-column">
                        <img alt="Image" class="uk-width-1-2 uk-margin-small-top uk-margin-small-bottom uk-border-circle uk-align-center  uk-box-shadow-large" src="assets/images/avatures/avature.jpg">
                    </div>
                    <div class="uk-width-4-5 uk-padding-remove-left">
                        <div class="uk-float-right">
                            <i class="fas fa-star icon-small"></i>
                            <i class="fas fa-star icon-small"></i>
                            <i class="fas fa-star icon-small"></i>
                            <i class="far fa-star icon-small"></i>
                            <i class="far fa-star icon-small"></i>
                        </div>
                        <h4 class="uk-margin-remove"> Sasriam keny </h4>
                        <span class="uk-text-small"> 2 Hourse ago</span>
                        <hr class="uk-margin-small">
                        <p class="uk-margin-remove-top uk-margin-small-bottom">Lorem ipsum dolor sit amet, consectetuer
                            adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam
                            erat volutpat. Ut wisi enim ad minim laoreet dolore magna aliquam erat volutpat. Ut wisi
                            enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut
                            aliquip ex ea commodo consequat .</p>
                    </div>
                </div>
                <div class="uk-grid-small uk-margin-medium-top" uk-grid>
                    <div class="uk-width-1-5 uk-first-column">
                        <img alt="Image" class="uk-width-1-2 uk-margin-small-top uk-margin-small-bottom uk-border-circle uk-align-center  uk-box-shadow-large" src="assets/images/avatures/avature-3.png">
                    </div>
                    <div class="uk-width-4-5 uk-padding-remove-left">
                        <div class="uk-float-right">
                            <i class="fas fa-star uk-text-danger icon-small"></i>
                            <i class="fa-star far icon-small"></i>
                            <i class="far fa-star icon-small"></i>
                            <i class="far fa-star icon-small"></i>
                            <i class="far fa-star icon-small"></i>
                        </div>
                        <h4 class="uk-margin-remove"> Masriam keny </h4>
                        <span class="uk-text-small"> 2 Hourse ago</span>
                        <hr class="uk-margin-small">
                        <p class="uk-margin-remove-top uk-margin-small-bottom">Lorem ipsum dolor sit amet, consectetuer
                            adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam
                            erat volutpat. Ut wisi enim ad minim laoreet dolore magna aliquam erat volutpat. Ut wisi
                            enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut
                            aliquip ex ea commodo consequat</p>
                    </div>
                </div>
                <div class="uk-grid-small uk-margin-medium-top" uk-grid>
                    <div class="uk-width-1-5 uk-first-column">
                        <img alt="Image" class="uk-width-1-2 uk-margin-small-top uk-margin-small-bottom uk-border-circle uk-align-center  uk-box-shadow-large" src="assets/images/avatures/avature-2.png">
                    </div>
                    <div class="uk-width-4-5 uk-padding-remove-left">
                        <div class="uk-float-right">
                            <i class="fas fa-star icon-small"></i>
                            <i class="fas fa-star icon-small"></i>
                            <i class="fas fa-star icon-small"></i>
                            <i class="far fa-star icon-small"></i>
                            <i class="far fa-star icon-small"></i>
                        </div>
                        <h4 class="uk-margin-remove"> Masriam keny </h4>
                        <span class="uk-text-small"> 2 Hourse ago</span>
                        <hr class="uk-margin-small">
                        <p class="uk-margin-remove-top uk-margin-small-bottom">Lorem ipsum dolor sit amet, consectetuer
                            adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam
                            erat volutpat. Ut wisi enim ad minim laoreet dolore magna aliquam erat volutpat. Ut wisi
                            enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut
                            aliquip ex ea commodo consequat</p>
                    </div>
                </div>
            </li>
    </main>
    <!-- footer -->
    <div class="uk-section-small uk-margin-medium-top">
        <hr class="uk-margin-remove">
        <div class="uk-container uk-align-center uk-margin-remove-bottom uk-position-relative">
            <div uk-grid>
                <div class="uk-width-1-3@m uk-width-1-2@s uk-first-column">
                    <a href="pages-about.php" class="uk-link-heading uk-text-lead uk-text-bold"> <i class="fas fa-graduation-cap"></i> <?php echo $platform_name; ?></a>
                    <div class="uk-width-xlarge tm-footer-description">A unique and beautiful collection of UI elements
                        that are all flexible and modular. building the website of your dreams.</div>
                </div>
                <div class="uk-width-expand@m uk-width-1-2@s">
                    <ul class="uk-list  tm-footer-list">
                        <li>
                            <a href="#"> Browse Our Library </a>
                        </li>
                        <li>
                            <a href="#"> Tutorials/Articles </a>
                        </li>
                        <li>
                            <a href="#"> Scripts and codes</a>
                        </li>
                        <li>
                            <a href="#"> Ebooks</a>
                        </li>
                    </ul>
                </div>
                <div class="uk-width-expand@m uk-width-1-2@s">
                    <ul class="uk-list tm-footer-list">
                        <li>
                            <a href="#"> About us </a>
                        </li>
                        <li>
                            <a href="#"> Contact Us </a>
                        </li>
                        <li>
                            <a href="#"> Privacy </a>
                        </li>
                        <li>
                            <a href="#"> Policy </a>
                        </li>
                    </ul>
                </div>
                <div class="uk-width-expand@m uk-width-1-2@s">
                    <ul class="uk-list  tm-footer-list">
                        <li>
                            <a href="#">Web Design </a>
                        </li>
                        <li>
                            <a href="#">Web Development </a>
                        </li>
                        <li>
                            <a href="#"> iOS Development </a>
                        </li>
                        <li>
                            <a href="#"> PHP Development </a>
                        </li>
                    </ul>
                </div>
            </div>
            <hr>
            <p class="uk-postion-absoult uk-margin-remove uk-position-bottom-right" style="bottom: 8px;right: 60px;" uk-tooltip="title: Visit Our Site; pos: top-center"> Powered By <a href="#" target="_blank" class="uk-text-bold uk-link-reset"> <?php echo $platform_name; ?></a></p>
            <div class="uk-margin-small" uk-grid>
                <div class="uk-width-1-2@m uk-width-1-2@s uk-first-column">
                    <p class="uk-text-small"><i class="fas fa-copyright"></i> 2019 <span class="uk-text-bold"><?php echo $platform_name; ?></span> . All rights reserved.</p>
                </div>
                <div class="uk-width-1-3@m uk-width-1-2@s">
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Youtube Chanal; pos: top-center"><i class="fab fa-youtube" style=" color: #fb7575  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Facebook; pos: top-center"><i class="fab fa-Facebook" style=" color: #9160ec  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Instagram; pos: top-center"><i class="fab fa-Instagram" style=" color: #dc2d2d  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our linkedin; pos: top-center"><i class="fab fa-linkedin " style=" color: #6949a5  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our google-plus; pos: top-center"><i class="fab fa-google-plus" style=" color: #f77070 !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Twitter; pos: top-center"><i class="fab fa-twitter" style=" color: #6f23ff !important;"></i></a>
                </div>
            </div>
        </div>
    </div>
    <!-- footer  end -->
    <!-- app end -->
    <!-- button scrollTop -->
    <button id="scrollTop" class="uk-animation-slide-bottom-medium">
        <a href="#" class="uk-text-white" uk-totop uk-scroll></a>
    </button>
    <!--  Night mood -->
    <script>
        (function(window, document, undefined) {
            'use strict';
            if (!('localStorage' in window)) return;
            var nightMode = localStorage.getItem('gmtNightMode');
            if (nightMode) {
                document.documentElement.className += ' night-mode';
            }
        })(window, document);


        (function(window, document, undefined) {

            'use strict';

            // Feature test
            if (!('localStorage' in window)) return;

            // Get our newly insert toggle
            var nightMode = document.querySelector('#night-mode');
            if (!nightMode) return;

            // When clicked, toggle night mode on or off
            nightMode.addEventListener('click', function(event) {
                event.preventDefault();
                document.documentElement.classList.toggle('night-mode');
                if (document.documentElement.classList.contains('night-mode')) {
                    localStorage.setItem('gmtNightMode', true);
                    return;
                }
                localStorage.removeItem('gmtNightMode');
            }, false);

        })(window, document);

        // Preloader
        var spinneroverlay = document.getElementById("spinneroverlay");
        window.addEventListener('load', function() {
            spinneroverlay.style.display = 'none';
        });

        //scrollTop
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("scrollTop").style.display = "block";
            } else {
                document.getElementById("scrollTop").style.display = "none";
            }
        }
        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
</body>

</html>