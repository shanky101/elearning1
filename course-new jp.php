<?php
// Initialize the session
session_start();

// Check if the user is already logged in, if yes then redirect him to welcome page
if (!isset($_SESSION["loggedin"])) {
    header("location: authentication.php");
    exit;
}

// Include config file
require_once "config.php";

//setting the username to empty at start
$name = "";

// Check if the user is already logged in, if yes then redirect him to welcome page
if (isset($_SESSION["name"]) && !empty($_SESSION["name"])) {
    //updating the user's name from session data
    $name = trim($_SESSION["name"]);
}
?>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="assets/images/favicon.png">
    <meta name="description" content="">
    <title> Home </title>
    <!-- Favicon -->
    <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Your stylesheet-->
    <link rel="stylesheet" href="assets/css/uikit.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <!-- font awesome -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <!--  javascript -->
    <script src="assets/js/simplebar.js"></script>
    <script src="assets/js/uikit.js"></script>
</head>

<body>
    <!-- PreLoader -->
    <div id="spinneroverlay">
        <div class="spinner"></div>
    </div>
    <!-- header  -->
    <header class="tm-header" uk-sticky>
        <div class=" uk-background-grey uk-navbar-container uk-navbar-transparent uk-padding-small uk-navbar-sticky">
            <div class="uk-position-relative">
                <nav class="uk-navbar-transparent tm-mobile-header uk-animation-slide-top uk-position-z-index" uk-navbar>
                    <!-- logo -->
                    <!-- mobile icon for side nav on nav-mobile-->
                    <span class="uk-hidden@m tm-mobile-menu-icon" uk-toggle="target: #mobile-sidebar"><i class="fas fa-bars icon-large"></i></span>
                    <!-- mobile icon for user icon on nav-mobile -->
                    <span class="uk-hidden@m tm-mobile-user-icon uk-align-right" uk-toggle="target: #tm-show-on-mobile; cls: tm-show-on-mobile-active"><i class="fas fa-user icon-large"></i></span>
                    <!-- mobile logo -->
                    <a class="uk-hidden@m uk-logo" href="index.php"> <?php echo $platform_name; ?></a>
                    <div class="uk-navbar-left uk-visible@m">
                        <a href="index.php" class="uk-logo uk-margin-left"> <i class="fas fa-graduation-cap"> </i>
                            <?php echo $platform_name; ?></a>
                    </div>
                    <div class="uk-navbar-right tm-show-on-mobile uk-flex-right" id="tm-show-on-mobile">
                        <!-- this will clouse after display user icon -->
                        <span class="uk-hidden@m tm-mobile-user-close-icon uk-align-right" uk-toggle="target: #tm-show-on-mobile; cls: tm-show-on-mobile-active"><i class="fas fa-times icon-large"></i></span>
                        <ul class="uk-navbar-nav uk-flex-middle">
                            <li>
                                <!-- your courses -->
                                <a href="#"> <i class="fas fa-play uk-hidden@m"></i> <span class="uk-visible@m"> あなたのコース</span> </a>
                                <div uk-dropdown="pos: top-right ;mode : click; animation: uk-animation-slide-bottom-medium" class="uk-dropdown border-radius-6  uk-dropdown-top-right tm-dropdown-large uk-padding-remove">
                                    <div class="uk-clearfix">
                                        <div class="uk-float-left">
                                            <h5 class="uk-padding-small uk-margin-remove uk-text-bold  uk-text-left">
                                            あなたのコース</h5>
                                        </div>
                                        <div class="uk-float-right">
                                            <i class="fas fa-check uk-align-right  uk-margin-remove uk-margin-remove-left  uk-padding-small uk-text-small">
                                                Completed 2 / 4 </i>
                                        </div>
                                    </div>
                                    <hr class=" uk-margin-remove">
                                    <div class="uk-padding-smaluk-text-left uk-height-medium">
                                        <div class="demo1" data-simplebar>
                                            <div class="uk-child-width-1-2@s  uk-grid-small uk-padding-small" uk-scrollspy="target: > div; cls:uk-animation-slide-bottom-small; delay: 100 ;repeat: true" uk-grid>
                                                <div>
                                                    <a href="course-view.php" class="uk-link-reset">
                                                        <div class="uk-padding-small uk-card-default">
                                                            <progress id="js-progressbar" class="uk-progress progress-green uk-margin-small-bottom" value="100" max="100" style="height: 7px;"></progress>
                                                            <img src="assets/images/courses/tags/css3.JPG" class="uk-align-left  uk-margin-small-right uk-margin-small-bottom  uk-width-1-3  uk-visible@s" alt="">
                                                            <p class="uk-text-bold uk-margin-remove">CSS3 Introduction
                                                            </p>
                                                            <p class="uk-text-small uk-margin-remove"> by : John Doe
                                                            </p>
                                                            <div class="uk-margin-small">
                                                                <a class="Course-tags uk-margin-small-right   border-radius-6" href="#"> <i class="fas fa-play"></i> Resume</a>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div>
                                                    <a href="course-view.php" class="uk-link-reset">
                                                        <div class="uk-padding-small uk-card-default">
                                                            <progress id="js-progressbar" class="uk-progress progress-coral  uk-margin-small-bottom" value="15" max="100" style="height: 7px !important;"></progress>
                                                            <img src="assets/images/courses/tags/html5.jpg" class="uk-align-left  uk-margin-small-right uk-margin-small-bottom  uk-width-1-3  uk-visible@s" alt="">
                                                            <p class="uk-text-bold uk-margin-remove">MATLAB Introduction
                                                            </p>
                                                            <p class="uk-text-small uk-margin-remove"> by : John Doe
                                                            </p>
                                                            <div class="uk-margin-small">
                                                                <a class="Course-tags uk-margin-small-right   border-radius-6" href="course-lesson jp.php"> <i class="fas fa-play"></i> Resume</a>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div>
                                                    <a href="course-view.php" class="uk-link-reset">
                                                        <div class="uk-padding-small uk-card-default">
                                                            <progress id="js-progressbar" class="uk-progress progress-coral uk-margin-small-bottom" value="50" max="100" style="height: 7px;"></progress>
                                                            <img src="assets/images/courses/tags/css3.JPG" class="uk-align-left  uk-margin-small-right uk-margin-small-bottom  uk-width-1-3  uk-visible@s" alt="">
                                                            <p class="uk-text-bold uk-margin-remove">PHP Introduction
                                                            </p>
                                                            <p class="uk-text-small uk-margin-remove"> by : John Doe
                                                            </p>
                                                            <div class="uk-margin-small">
                                                                <a class="Course-tags uk-margin-small-right   border-radius-6" href="#"> <i class="fas fa-play"></i> Resume</a>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div>
                                                    <a href="course-view.php" class="uk-link-reset">
                                                        <div class="uk-padding-small uk-card-default">
                                                            <progress id="js-progressbar" class="uk-progress progress-green uk-margin-small-bottom" value="100" max="100" style="height: 7px;"></progress>
                                                            <img src="assets/images/courses/tags/css3.JPG" class="uk-align-left  uk-margin-small-right uk-margin-small-bottom  uk-width-1-3  uk-visible@s" alt="">
                                                            <p class="uk-text-bold uk-margin-remove">Python Introduction
                                                            </p>
                                                            <p class="uk-text-small uk-margin-remove"> by : John Doe
                                                            </p>
                                                            <div class="uk-margin-small">
                                                                <a class="Course-tags uk-margin-small-right   border-radius-6" href="#"> <i class="fas fa-play"></i> Resume</a>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class=" uk-margin-remove">
                                    <h5 class="uk-padding-small uk-margin-remove uk-text-bold uk-text-center"><a class="uk-link-heading" href="#"> See all </a> </h5>
                                </div>
                            </li>
                            <li>
                                <!-- User profile -->
                                <a href="#">
                                    <img src="assets/images/avatures/avature-2.png" alt="" class="uk-border-circle user-profile-tiny">
                                </a>
                                <div uk-dropdown="pos: top-right ;mode : click ;animation: uk-animation-slide-right" class="uk-dropdown uk-padding-small uk-dropdown-top-right  angle-top-right">
                                    <p class="uk-margin-remove-bottom uk-margin-small-top uk-text-bold"> <?php echo $name; ?>
                                    </p>
                                    <p class="uk-margin-remove-top uk-text-small uk-margin-small-bottom">
                                    </p>
                                    <ul class="uk-nav uk-dropdown-nav">
                                        <li>
                                            <a href="profile.php"> <i class="fas fa-user uk-margin-small-right"></i>
                                                Profile</a>
                                        </li>
                                        <li>
                                            <a href="#"> <i class="fas fa-cog uk-margin-small-right"></i> Setting</a>
                                        </li>
                                        <li class="uk-nav-divider"></li>
                                        <li>
                                            <a href="logout.php"> <i class="fas fa-sign-out-alt uk-margin-small-right"></i> Log out</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- Navigation for mobile -->
                    <div id="mobile-sidebar" class="mobile-sidebar" uk-offcanvas="overlay:true">
                        <div class="uk-offcanvas-bar uk-preserve-color uk-padding-remove">
                            <ul uk-accordion>
                                <li class="uk-open">
                                    <a href="#" class="uk-accordion-title uk-text-black uk-padding-small"> <i class="fas fa-play-circle uk-margin-small-right"></i> Courses </a>
                                    <div class="uk-accordion-content uk-margin-remove-top">
                                        <ul class="uk-list tm-drop-topic-list">
                                            <li>
                                                <a href="#"> All Development</a>
                                            </li>
                                            <li>
                                                <a href="#"> Web Development </a>
                                            </li>
                                            <li>
                                                <a href="#"> Mobile App </a>
                                            </li>
                                            <li>
                                                <a href="#"> Programming language </a>
                                            </li>
                                            <li>
                                                <a href="#"> Software </a>
                                            </li>
                                            <li>
                                                <a href="#"> Ecommerce </a>
                                            </li>
                                            <li>
                                                <a href="#"> Training</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- search box -->
                    <div id="modal-full" class="uk-modal-full uk-modal uk-animation-scale-down" uk-modal>
                        <div class="uk-modal-dialog uk-flex uk-flex-center" uk-height-viewport>
                            <button class="uk-modal-close-full" type="button" uk-close></button>
                            <form class="uk-search uk-margin-xlarge-top uk-search-large uk-animation-slide-bottom-medium">
                                <i class="fas fa-search uk-position-absolute uk-margin-top icon-xxlarge"></i>
                                <input class="uk-search-input uk-margin-large-left" type="search" placeholder="Search..." autofocus>
                            </form>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <main>
        <div class="course-intro uk-text-center topic4">
            <h2 class="uk-light uk-text-uppercase uk-text-bold uk-text-white uk-margin-top"> Simulink </h2>
            <p class="uk-light uk-text-bold">Simulinkの基本を習得する最も簡単な方法。</p>
            <p class="uk-light uk-text-bold uk-text-small"> <i class="fas fa-star icon-small icon-rate"></i> <i class="fas fa-star icon-small icon-rate"></i> <i class="fas fa-star icon-small icon-rate"></i> <i class="far fa-star icon-small icon-rate"></i> <i class="far fa-star icon-small icon-rate"></i> <span class="uk-margin-small-right"> 4.0 (282 ratings) </span> <br> <i class="fas fa-user icon-small uk-margin-small-right"></i> 334 people enrolled</p>
            <a class="uk-button uk-button-white" href="course-lesson jp.php" uk-tooltip="title: Start this course now ; delay: 200 ; pos: top ;animation:	uk-animation-scale-up"> 今すぐ始める</a>
            <a class="uk-button uk-button-white" href="#modal-sections" uk-toggle uk-tooltip="title: watch preview video ; delay: 200 ; pos: top ;animation:	uk-animation-scale-up"> 序文 </a>
            <!-- video demo model -->
            <div id="modal-sections" uk-modal>
                <div class="uk-modal-dialog">
                    <button class="uk-modal-close-default uk-margin-small-top  uk-margin-small-right uk-light" type="button" uk-close></button>
                    <div class="uk-modal-header topic4 none-border">
                        <b class="uk-text-white uk-text-medium"> 序文 </b>
                    </div>
                    <div class="video-responsive">
                        <iframe src="https://www.youtube.com/embed/YZUqFt9ko9U" class="uk-padding-remove" uk-video="automute: true" frameborder="0" allowfullscreen uk-responsive></iframe>
                    </div>
                    <!-- custom video 
                        <video loop muted playsinline controls uk-video="autoplay: inview"> 
                            <source src="videos/course-intro.mp4" type="video/mp4"> 
                        </video>
                        -->
                    <div class="uk-modal-body">
                        <h3>Build Responsive Websites </h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                    <div class="uk-modal-footer uk-text-right">
                        <button class="uk-button uk-button-default uk-modal-close" type="button">キャンセル</button>
                        <a href="course-dashboard.php">
                            <button class="uk-button uk-button-grey" type="button">今すぐ始める </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- navigation-->
        <ul class="uk-tab uk-flex-center  uk-margin-remove-top uk-background-default  uk-sticky" uk-sticky="animation: uk-animation-slide-top; bottom: #sticky-on-scroll-up ; media: @s;" uk-tab>
            <li aria-expanded="true" class="uk-active">
                <a href="#"> コース紹介 </a>
            </li>
            <li>
                <a href="#"> 指導者 </a>
            </li>
            <li>
                <a href="#"> レビュー </a>
            </li>
            <li>
                <a href="#"> 学生 </a>
            </li>
        </ul>
        <ul class="uk-switcher uk-margin uk-padding-small uk-container-small uk-margin-auto  uk-margin-top">
            <!-- tab 1 About the course -->
            <li class="uk-active">
                <h3> このコースについて</h3>
                
                <p> Simulinkは、マルチドメインシミュレーションとモデルベース設計のためのブロックダイアグラム環境です。システムレベルの設計、シミュレーション、自動コード生成、組み込みシステムの継続的なテストと検証をサポートします。Simulinkは、動的なシステムをモデリングおよびシミュレートするためのグラフィカルなエディタ、カスタマイズ可能なブロックライブラリ、およびソルバを提供します。MATLAB®と統合されているため、MATLABアルゴリズムをモデルに組み込み、さらに解析するためにシミュレーション結果をMATLABにエクスポートできます。 </p>
               
                <h4> 主な機能</h4>
                <ul class="uk-list uk-list-bullet">
                    <li>階層ブロック図を作成および管理するためのグラフィカルエディタ。</li>
                    <li>連続時間および離散時間システムをモデリングするための定義済みブロックのライブラリ。</li>
                    <li>固定段および可変段のODEソルバーを備えたシミュレーションエンジン </li>
                    <li>シミュレーション結果を表示するためのスコープとデータ表示•モデルファイルとデータを管理するためのプロジェクトおよびデータ管理ツール</li>
                    <li>モデルアーキテクチャの改良とシミュレーション速度の向上のためのモデル解析ツール </li>
                    <li>MATLABモデルにMATLABアルゴリズムをインポートするためのMATLAB関数ブロック </li>
                    <li>C/C++コードをモデルにインポートするためのレガシーコードツール</li>
                </ul>
                <h3>Simulinkを使用したモデルベースの設計  </h3>
                <p> モデリングは、現実世界のシステムの仮想表現を作成する方法です。この仮想表現をさまざまな条件下でシミュレートして、その動作を確認できます。モデリングとシミュレーションは、ハードウェアプロトタイプだけでは再現が困難な条件をテストする場合に役立ちます。これは、ハードウェアがまだ利用できない設計プロセスの初期段階で特に当てはまります。モデリングとシミュレーションの間の反復は、設計プロセスの後の方で発見されるエラーの数を減らすことによって、システム設計の品質を早期に改善することができる。モデルからコードを自動的に生成し、ソフトウェアとハードウェアの実装要件が含まれている場合は、システム検証用のテストベンチを作成できます。コード生成は時間を節約し、手動でコード化されたエラーの発生を防ぎます。モデルベース設計では、システムモデルがワークフローの中心になります。モデル・ベースの設計により、制御システム、信号処理システム、通信システムなどの動的システムを迅速かつコスト・パフォーマンスの高い方法で開発できます</p>
                  <h4> モデルベースの設計により、次のことが可能になります</h4>
                  <ul class="uk-list uk-list-bullet">
                    <li>プロジェクトチーム間で共通の設計環境を使用します。</li>
                    <li>設計を要件に直接リンクします。</li>
                    <li>テストと設計の統合による継続的なエラーの特定と修正 </li>
                    <li>マルチドメイン・シミュレーションによるアルゴリズムの改良•組み込みソフトウェア・コードおよびドキュメントの自動生成  </li>
                    <li>テストスイートの開発と再利用 </li>
                    
                  </ul>
            </li>

            
        </ul>

    </main>
    <!-- footer -->
    <div class="uk-section-small uk-margin-medium-top">
        <hr class="uk-margin-remove">
        <div class="uk-container uk-align-center uk-margin-remove-bottom uk-position-relative">
            <div uk-grid>
                <div class="uk-width-1-3@m uk-width-1-2@s uk-first-column">
                    <a href="pages-about.php" class="uk-link-heading uk-text-lead uk-text-bold"> <i class="fas fa-graduation-cap"></i> <?php echo $platform_name; ?></a>
                    <div class="uk-width-xlarge tm-footer-description">最高品質の製品とサービスを顧客に提供します。 企業活動の目標は、顧客の競争力を高めることです。</div>
                </div>
                <div class="uk-width-expand@m uk-width-1-2@s">
                <ul class="uk-list  tm-footer-list">
                        <li>
                            <a href="#"> コースを閲覧する </a>
                        </li>
                        <li>
                            <a href="#"> 記事 </a>
                        </li>
                        <li>
                            <a href="#"> 議論</a>
                        </li>
                    </ul>
                </div>
                <div class="uk-width-expand@m uk-width-1-2@s">
                <ul class="uk-list tm-footer-list">
                        <li>
                            <a href="pages-about.php"><?php echo $text_about_us; ?></a>
                        </li>
                    <li>
                            <a href="pages-contact.php">お問い合わせ</a>
                    </li>
                        <li>
                            <a href="pages-contact.php"> 利用規約 </a>
                        </li>
                        <li>
                            <a href="#"> よくある質問 </a>
                        </li>
                    </ul>
                </div>
                <div class="uk-width-expand@m uk-width-1-2@s">
                    <ul class="uk-list  tm-footer-list">
                        <li>
                            <a href="#">MATLAB </a>
                        </li>
                        <li>
                            <a href="#">サイバーセキュリティ </a>
                        </li>
                        <li>
                            <a href="#"> CAD/CAM </a>
                        </li>
                    </ul>
                </div>
            </div>
            <hr>
            <p class="uk-postion-absoult uk-margin-remove uk-position-bottom-right" style="bottom: 8px;right: 60px;" uk-tooltip="title: Visit Our Site; pos: top-center"> Powered By <a href="#" target="_blank" class="uk-text-bold uk-link-reset"> <?php echo $platform_name; ?></a></p>
            <div class="uk-margin-small" uk-grid>
                <div class="uk-width-1-2@m uk-width-1-2@s uk-first-column">
                    <p class="uk-text-small"><i class="fas fa-copyright"></i> 2019 <span class="uk-text-bold"><?php echo $platform_name; ?></span> . All rights reserved.</p>
                </div>
                <div class="uk-width-1-3@m uk-width-1-2@s">
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Youtube Chanal; pos: top-center"><i class="fab fa-youtube" style=" color: #fb7575  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Facebook; pos: top-center"><i class="fab fa-Facebook" style=" color: #9160ec  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Instagram; pos: top-center"><i class="fab fa-Instagram" style=" color: #dc2d2d  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our linkedin; pos: top-center"><i class="fab fa-linkedin " style=" color: #6949a5  !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our google-plus; pos: top-center"><i class="fab fa-google-plus" style=" color: #f77070 !important;"></i></a>
                    <a href="#" class="uk-icon-button uk-link-reset" uk-tooltip="title: Our Twitter; pos: top-center"><i class="fab fa-twitter" style=" color: #6f23ff !important;"></i></a>
                </div>
            </div>
        </div>
    </div>
    <!-- footer  end -->
    <!-- app end -->
    <!-- button scrollTop -->
    <button id="scrollTop" class="uk-animation-slide-bottom-medium">
        <a href="#" class="uk-text-white" uk-totop uk-scroll></a>
    </button>
    <!--  Night mood -->
    <script>
        (function(window, document, undefined) {
            'use strict';
            if (!('localStorage' in window)) return;
            var nightMode = localStorage.getItem('gmtNightMode');
            if (nightMode) {
                document.documentElement.className += ' night-mode';
            }
        })(window, document);


        (function(window, document, undefined) {

            'use strict';

            // Feature test
            if (!('localStorage' in window)) return;

            // Get our newly insert toggle
            var nightMode = document.querySelector('#night-mode');
            if (!nightMode) return;

            // When clicked, toggle night mode on or off
            nightMode.addEventListener('click', function(event) {
                event.preventDefault();
                document.documentElement.classList.toggle('night-mode');
                if (document.documentElement.classList.contains('night-mode')) {
                    localStorage.setItem('gmtNightMode', true);
                    return;
                }
                localStorage.removeItem('gmtNightMode');
            }, false);

        })(window, document);

        // Preloader
        var spinneroverlay = document.getElementById("spinneroverlay");
        window.addEventListener('load', function() {
            spinneroverlay.style.display = 'none';
        });

        //scrollTop
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("scrollTop").style.display = "block";
            } else {
                document.getElementById("scrollTop").style.display = "none";
            }
        }
        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
</body>

</html>