<?php
// Initialize the session
session_start();

// Check if the user is already logged in, if yes then redirect him to welcome page
if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
    header("location: course-dashboard.php");
    exit;
}

// Include config file
require_once "config.php";

// Define variables and initialize with empty values
$name = $username = $password = $confirm_password = $created_at = "";
$name_err = $username_err = $password_err = $confirm_password_err = "";

// Processing form data when form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST['signin_submit']) && $_POST['signin_submit']) {
        // Check if username is empty
        if (empty(trim($_POST["username"]))) {
            $username_err = "Please enter your email address.";
        } else {
            $username = trim($_POST["username"]);
        }

        // Check if password is empty
        if (empty(trim($_POST["password"]))) {
            $password_err = "Please enter your password.";
        } else {
            $password = trim($_POST["password"]);
        }

        // Validate credentials
        if (empty($username_err) && empty($password_err)) {
            // Prepare a select statement
            $sql = "SELECT name, username, password, DATE_FORMAT(created_at, \"%M %e, %Y\") FROM users WHERE username = ?";

            if ($stmt = $mysqli->prepare($sql)) {
                // Bind variables to the prepared statement as parameters
                $stmt->bind_param("s", $param_username);

                // Set parameters
                $param_username = $username;

                // Attempt to execute the prepared statement
                if ($stmt->execute()) {
                    // Store result
                    $stmt->store_result();

                    // Check if username exists, if yes then verify password
                    if ($stmt->num_rows == 1) {
                        // Bind result variables
                        $stmt->bind_result($name, $username, $hashed_password, $created_at);
                        if ($stmt->fetch()) {
                            if (password_verify($password, $hashed_password)) {

                                // Password is correct, so start a new session
                                //session_start();

                                // Store data in session variables
                                $_SESSION["loggedin"] = true;
                                $_SESSION["name"] = $name;
                                $_SESSION["username"] = $username;
                                $_SESSION["created_at"] = $created_at;

                                // Redirect user to welcome page
                                header("location: course-dashboard.php");
                            } else {
                                // Display an error message if password is not valid
                                $password_err = "The password you entered was not valid.";
                            }
                        }
                    } else {
                        // Display an error message if username doesn't exist
                        $username_err = "No account found with that email.";
                    }
                } else {
                    echo "Oops! Something went wrong. Please try again later.";
                }
            }
        }

        // Close statement
        $stmt->close();
    } else if (isset($_POST['signup_submit']) && $_POST['signup_submit']) {
        // Validate Name
        if (empty(trim($_POST["r_name"]))) {
            $name_err = "Please enter a valid name.";
        } else {
            $name = trim($_POST["r_name"]);
        }

        // Validate username
        if (empty(trim($_POST["r_username"]))) {
            $username_err = "Please enter a valid email address.";
        } else {
            // Prepare a select statement
            $sql = "SELECT id FROM users WHERE username = ?";

            if ($stmt = $mysqli->prepare($sql)) {
                // Bind variables to the prepared statement as parameters
                $stmt->bind_param("s", $param_username);

                // Set parameters
                $param_username = trim($_POST["r_username"]);

                // Attempt to execute the prepared statement
                if ($stmt->execute()) {
                    // store result
                    $stmt->store_result();

                    if ($stmt->num_rows == 1) {
                        $username_err = "This email is already registered with us.";
                    } else {
                        $username = trim($_POST["r_username"]);
                    }
                } else {
                    echo "Oops! Something went wrong. Please try again later.";
                }
            }

            // Close statement
            $stmt->close();
        }

        // Validate password
        if (empty(trim($_POST["r_password"]))) {
            $password_err = "Please enter a password.";
        } elseif (strlen(trim($_POST["r_password"])) < 8) {
            $password_err = "Password must have atleast 8 characters.";
        } else {
            $password = trim($_POST["r_password"]);
        }

        // Validate confirm password
        if (empty(trim($_POST["r_confirm_password"]))) {
            $confirm_password_err = "Please confirm password.";
        } else {
            $confirm_password = trim($_POST["r_confirm_password"]);
            if (empty($password_err) && ($password != $confirm_password)) {
                $confirm_password_err = "Passwords did not match.";
            }
        }

        // Check input errors before inserting in database
        if (empty($name_err) && empty($username_err) && empty($password_err) && empty($confirm_password_err)) {

            // Prepare an insert statement
            $sql = "INSERT INTO users (name, username, password) VALUES (?, ?, ?)";

            if ($stmt = $mysqli->prepare($sql)) {
                // Bind variables to the prepared statement as parameters
                $stmt->bind_param("sss", $param_name, $param_username, $param_password);

                // Set parameters
                $param_name = $name;
                $param_username = $username;
                $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash

                // Attempt to execute the prepared statement
                if ($stmt->execute()) {
                    // Once successful, stay at the same authentication page
                    // or Redirect to other page
                    //header("location: other.php");
                } else {
                    echo "Something went wrong. Please try again later.";
                }
            }

            // Close statement
            $stmt->close();
        }
    }

    // Close connection
    $mysqli->close();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="assets/images/favicon.png">
    <meta name="description" content="">
    <title> Login/Register </title>
    <!-- Favicon -->
    <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">

    <!-- Your stylesheet-->
    <link rel="stylesheet" href="assets/css/uikit.css">
    <link rel="stylesheet" href="assets/css/update.css">
    <link rel="stylesheet" href="assets/css/main.css">

    <!-- font awesome -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">

    <!--  javascript -->
    <script src="assets/js/simplebar.js"></script>
    <script src="assets/js/uikit.js"></script>
</head>

<body>
    <div uk-height-viewport="offset-top: true; offset-bottom: true" class="uk-flex uk-flex-middle">
        <div class="uk-width-2-3@m uk-width-1-2@s uk-margin-auto  border-radius-6 ">
            <div class="uk-child-width-1-2@m uk-background-grey uk-grid-collapse" uk-grid>
                <div class="uk-text-middle uk-margin-auto-vertical uk-text-center uk-padding-small uk-animation-scale-up">
                    <p> <i class="fas fa-graduation-cap uk-text-white" style="font-size:60px"></i> </p>
                    <h1 class="uk-text-white uk-margin-small"> <?php echo $platform_name; ?> </h1>
                    <h5 class="uk-margin-small uk-text-muted uk-text-bold uk-text-nowrap"> The Place You can learn Every Thing. </h5>
                </div>
                <div>
                    <div class="uk-card-default uk-padding uk-card-small">

                        <!-- Login tab -->
                        <form id="loginForm" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                            <div id="login" class="tabcontent tab-default-open animation: uk-animation-slide-right-medium">
                                <h2 class="uk-text-bold"> Log in </h2>
                                <p class="uk-text-muted uk-margin-remove-top uk-margin-small-bottom"> Please provide the required details</p>
                                <div class="uk-form-label"> Email address </div>
                                <div class="uk-inline">
                                    <span class="uk-form-icon"><i class="far fa-envelope icon-medium"></i></span>
                                    <input class="uk-input uk-form-width-large" name="username" placeholder="name@example.com" type="email" required>
                                </div>
                                <div class="uk-form-error"><?php echo $username_err; ?></div>
                                <div class="uk-flex-middle" uk-grid>
                                    <div class="uk-width-expand@m uk-margin-small-bottom">
                                        <div class="uk-form-label">Password</div>
                                    </div>
                                </div>
                                <div class="uk-inline uk-margin-small-bottom">
                                    <span class="uk-form-icon"><i class="fas fa-key icon-medium"></i></span>
                                    <input class="uk-input uk-form-width-large" name="password" id="password" placeholder="Password" type="password" required>
                                </div>
                                <div class="uk-form-error"><?php echo $password_err; ?></div>
                                <div class="uk-flex-middle" uk-grid>
                                    <div class="uk-width-expand@m uk-margin-small-bottom">
                                        <input class="uk-checkbox" type="checkbox" data-show-pw="#password">
                                        <span class="checkmark uk-text-small"> Show Password </span>
                                    </div>

                                    <div class="uk-width-auto@m">
                                        <a href="#" class="tablinks uk-text-small " onclick="openTabs(event, 'forget')"> Forgot password? </a>
                                    </div>

                                </div>
                                <div class="uk-margin uk-text-small">
                                    Not registered?
                                    <a href="#" class="tablinks uk-text-bold" onclick="openTabs(event, 'register')"> Create an account </a>
                                </div>
                                <div class="uk-flex-middle" uk-grid>
                                    <div class="uk-width-expand@m">
                                        <input class="uk-button uk-button-success" name="signin_submit" type="submit" class="button" value="Sign in">
                                    </div>
                                </div>
                            </div>
                        </form>

                        <!-- register tab -->
                        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                            <div id="register" class="tabcontent animation: uk-animation-slide-left-medium">
                                <h2 class="uk-text-bold"> Register </h2>
                                <p class="uk-text-muted uk-margin-remove-top uk-margin-small-bottom"> Create your free account</p>
                                <div class="uk-form-label">Full name</div>
                                <div class="uk-inline">
                                    <span class="uk-form-icon"><i class="far fa-user icon-medium"></i></span>
                                    <input class="uk-input uk-form-width-large" name="r_name" placeholder="Full name" type="text" required>
                                </div>
                                <div class="uk-form-label">Email address</div>
                                <div class="uk-inline">
                                    <span class="uk-form-icon"><i class="far fa-envelope icon-medium"></i></span>
                                    <input class="uk-input uk-form-width-large" name="r_username" placeholder="name@example.com" type="email" required>
                                </div>
                                <form class="uk-child-width-1-2@m uk-grid-small" uk-grid>
                                    <div>
                                        <div class="uk-form-label">Password </div>
                                        <input class="uk-input" name="r_password" placeholder="Password" type="Password" id="password-1" required>
                                    </div>
                                    <div>
                                        <div class="uk-form-label">Confirm password</div>
                                        <input class="uk-input" name="r_confirm_password" placeholder="Password" type="Password" id="password-2" required>
                                    </div>
                                </form>
                                <div>
                                    <label>
                                        <input class="uk-checkbox" type="checkbox" data-show-pw="#password-1 ,#password-2">
                                        <span class="checkmark uk-text-small"> Show passwords </span>
                                    </label>
                                </div>
                                <div class="uk-margin">
                                    <label>
                                        <input class="uk-checkbox" type="checkbox" checked onchange="activateSignupButton(this)">
                                        <span class="checkmark uk-text-small"> I agree to the </span>
                                        <a href="#modal-overflow" class="uk-text-bold uk-text-small" uk-toggle> Terms and Conditions </a>
                                    </label>
                                </div>
                                <div class=" uk-flex-middle" uk-grid>
                                    <div class="uk-width-expand@m">
                                        <input class="uk-button uk-button-success" id="signup_submit" name="signup_submit" type="submit" value="Create">
                                    </div>
                                    <div class="uk-width-auto@m">
                                        <span class="uk-text-small"> Already registered? <a href="#" class="tablinks uk-text-bold" onclick="openTabs(event, 'login')"> Sign in </a> </span>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <!-- forget tab -->
                        <div id="forget" class="tabcontent animation: uk-animation-slide-bottom-medium">
                            <h2 class="uk-text-bold"> Forget password </h2>
                            <p class="uk-text-muted uk-margin-remove-top uk-margin-small-bottom">to reset your password fill inter your email</p>
                            <div>
                                <div class="uk-form-label">Email address</div>
                                <div class="uk-inline">
                                    <span class="uk-form-icon"><i class="fas fa-lock icon-medium"></i></span>
                                    <input class="uk-input uk-form-width-large" placeholder="name@example.com" type="text">
                                </div>
                            </div>
                            <div class="uk-margin">
                                <input class="uk-button uk-button-success" type="submit" class="button" value="Reset password">
                            </div>
                            <span class="uk-text-small">
                                Not registered?
                                <a href="#" class="tablinks" onclick="openTabs(event, 'register')"> Create account </a>
                                Or
                                <a href="#" class="tablinks" onclick="openTabs(event, 'login')"> Sign in </a>
                            </span>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Terms model-->
    <div id="modal-overflow" uk-modal>
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-default uk-margin-small-top uk-margin-small-right" type="button" uk-close></button>
            <div class="uk-modal-header">
                <b class="uk-text-medium"> Terms </b>
            </div>
            <div class="uk-modal-body" uk-overflow-auto>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                <button class="uk-button uk-button-primary" type="button">Save</button>
            </div>
        </div>
    </div>
    <script>
        // Listen for clicks in the document
        document.addEventListener('click', function(event) {

            // Check if a password selector was clicked
            var selector = event.target.getAttribute('data-show-pw');
            if (!selector) return;

            // Get the passwords
            var passwords = document.querySelectorAll(selector);

            // Toggle visibility
            Array.from(passwords).forEach(function(password) {
                if (event.target.checked === true) {
                    password.type = 'text';
                } else {
                    password.type = 'password';
                }
            });
        }, false);

        function activateSignupButton(element) {

            if (element.checked) {
                document.getElementById("signup_submit").className = "uk-button uk-button-success";
                document.getElementById("signup_submit").disabled = false;
            } else {
                document.getElementById("signup_submit").className = "uk-button uk-button-default";
                document.getElementById("signup_submit").disabled = true;
            }

        }
    </script>
</body>

</html>